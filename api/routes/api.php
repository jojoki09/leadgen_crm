<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



    /**
     * Testing Area
     */

// Import imeis from xlsx
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImeisImport;

Route::group(["middleware" => []], function() {

    Route::post("/auth/register", "AuthController@register");

    Route::get("/test", function() {
        return App\User::all();
    });



    Route::post("/sendemail", "PhpMailerController@sendEmail");

    Route::get('open', 'DataController@open');
    Route::post('/auth/login', 'AuthController@authenticate');
    Route::post('/verify_email', 'AuthController@verify_email');

    Route::get('closed', 'DataController@closed');


    // download document boitier_gps_imeis.xlsx
    Route::get("/storage/docs/boitier_gps_imeis", "StorageController@boitier_gps_imeis");

    Route::post("/stock/imeis/import", function() {
        Excel::import(new ImeisImport, request()->file('boitier_gps_imeis')->getRealPath());
    });
    Route::get('getRelaunch/','VenteDevisDetailController@getProcepectRelaunch');

    Route::post('ProjectBycommercial','DashboardController@getProjectBycommercial');

    // download file request
    Route::get('/download', 'TestController@download');

    Route::get("/generatePDF/{intervention_id}", "InterventionController@generatePDF");

});
/*
      project
      */




Route::group(["middleware" => ["jwt"], 'namespace' => '\App\Http\Controllers'], function() {





    Route::get('cities','VilleController@index');
    Route::get('cities/{id}','VilleController@getCityById');

    Route::get('countries','VilleController@getCountry');
    Route::resource('sources','ListesController');

    Route::resource('groups', 'GroupController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('files', 'FileController');
    Route::resource('file-groups', 'FileGroupController');

    Route::resource('categorie', 'CategorieController');

    Route::resource('projects', 'ProjectController');
    Route::resource('calendars', 'CalendarController');

    Route::resource('company', 'SocieteController');

    Route::resource('partners', 'PartenaireController');
    Route::get('getDocument/{id}','VenteDevisDetailController@getDocument');

    Route::resource('quotes', 'VenteDevisController');
    Route::resource('detailProspect', 'VenteDevisDetailController');
    Route::resource('ListesStatus', 'ListesController');

    Route::get('ProcepectByPartId/{id}','ListesProcepectController@getProcepectById');


    Route::resource('Procepect', 'ListesProcepectController');

    /**
     * Auth
     */
//    Route::post("/auth/register", "AuthController@register");
    Route::get('/auth/authenticated_user', 'AuthController@getAuthenticatedUser');
    Route::post('/auth/logout', 'AuthController@logout');


    /**
     * Users
     */
    Route::get("/users/{user_id}/departement", "UserController@departement");
    Route::get("/users/{user_id}/role", "UserController@role");
    Route::get("/users/{user_id}/rights/{rights_depending}", "UserController@rights")->where("rights_depending", "^(0|1|2){1}$");
    Route::get("/users/{user_id}/rights", "UserController@getUserRights");
    Route::get("/users/{user_id}/group/rights", "UserController@getUserDeptRights");
    Route::get("/users/{user_id}/role/rights", "UserController@getUserRoleRights");
    Route::get("/users/{user_id}/avatar", "UserController@getAvatar");
    Route::delete("/users/{user_id}/avatar", "UserController@deleteAvatar");
    Route::post("/users/offer_rights", "UserController@offer_rights");
    Route::get("/users", "UserController@index");
    Route::get("/users/{user_id}", "UserController@get");
    Route::post("/users/update", "UserController@update");
    Route::delete("/users/{user_id}/delete", "UserController@destroy");
    Route::get("/users/{user_id}/job", "UserController@getJob");
    Route::post("/users/edit_password", "UserController@editPassword");

    Route::get("/getUserChildrens/{user_id}/{job_title}", "UserController@getUserChildrens");

    /**
     * Departements
     */
    Route::get("/departements", "DepartementController@index");
    Route::post("/departements", "DepartementController@store");
    Route::put("/departements", "DepartementController@update");
    Route::delete("/departements/{departement_id}/delete", "DepartementController@destroy");
    Route::get("/departements/{departement_id}/rights", "DepartementController@getDepartementRights");
    Route::post("/departements/offer_rights", "DepartementController@offer_rights");

    /**
     * Roles
     */
    Route::get("/roles", "RoleController@index");
    Route::post("/roles", "RoleController@store");
    Route::put("/roles", "RoleController@update");
    Route::delete("/roles/{role_id}/delete", "RoleController@destroy");
    Route::get("/roles/{role_id}/rights", "RoleController@getRoleRights");
    Route::post("/roles/offer_rights", "RoleController@offer_rights");

    /**
     * Rights
     */
    Route::get("/rights", "RightController@index");
    Route::post("/rights", "RightController@store");
    Route::put("/rights", "RightController@update");
    Route::delete("/rights/{right_id}/delete", "RightController@destroy");





    /**
     * Modules
     */
    Route::get("/modules/{module_id}", "ModuleController@get");
    Route::get("/modules", "ModuleController@index");
    Route::post("/modules", "ModuleController@store");
    Route::put("/modules", "ModuleController@update");
    Route::delete("/modules/{module_id}/delete", "ModuleController@destroy");
    Route::get("/modules/{module_id}/rights", "ModuleController@rights");

    /**
     * Jobs
     */
    Route::get("/jobs", "UserJobController@index");
    Route::post("/jobs", "UserJobController@store");
    Route::put("/jobs", "UserJobController@update");
    Route::delete("/jobs/{job_id}/delete", "UserJobController@destroy");
    Route::get("/jobs/{job_id}/users", "UserJobController@getUsers");




});



