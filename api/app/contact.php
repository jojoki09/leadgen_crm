<?php

namespace App;

use App\Components\partners\Models\partenaire;
use Illuminate\Database\Eloquent\Model;
use App\Components\company\models\societe;
use App\Components\User\models\user;

class contact extends Model
{
    protected $fillable = ['part_id','societe_id','titre','nom','prenom','mobile','phone','fax','email','function','typ','etat','user_id'];
    public function societe(){
        $this->belongsTo(societe::class);
    }
    public function user(){
        $this->belongsTo(user::class);
    }
    public function partenaire()
    {
        return $this->belongsTo(partenaire::class);
    }
}
