<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 31 Oct 2019 14:25:08 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property string $country_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_on
 *
 * @package App\Models
 */
class City extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'country_id' => 'int'
	];

	protected $dates = [
		'updated_on'
	];

	protected $fillable = [
		'nom',
		'country_id',
		'country_code',
		'updated_on'
	];
}
