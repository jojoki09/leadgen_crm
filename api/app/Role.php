<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected  $table = "user_roles";

    protected $dates = ['deleted_at'];

    protected $fillable = [
        "role_name",
        "description",
    ];

    public function rights()
    {
        return $this->belongsToMany('App\Right', "role_rights", "role_id", "right_id");
    }
}
