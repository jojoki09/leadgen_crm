<?php

namespace App;

use App\Components\partners\Models\partenaire;
use Illuminate\Database\Eloquent\Model;
use App\Components\projects\models\project;
use App\Components\company\models\societe;

class adresse extends Model
{
    protected $fillable = ['adr','ville_id','pays_id','project_id'];

    public function projects()
    {
        return $this->belongsTo(project::class);
    }
    public function societe(){
        $this->belongsTo(societe::class);
    }
    public function partenaire()
    {
        $this->belongsTo(partenaire::class);
    }
}
