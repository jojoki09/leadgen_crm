<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 14 Oct 2019 15:21:16 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Calendar
 *
 * @property int $id
 * @property string $object
 * @property \Carbon\Carbon $date_planif
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $part_id
 * @property int $project_id
 *
 * @package App\Models
 */
class Calendar extends Eloquent
{
	protected $casts = [
		'part_id' => 'int',
		'project_id' => 'int'
	];

	protected $dates = [
		'date_planif'
	];

	protected $fillable = [
		'object',
		'date_planif',
		'part_id',
		'project_id',
        'color'
	];
}
