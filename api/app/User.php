<?php

namespace App;


use Log;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends  Authenticatable implements JWTSubject

{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $hidden = [
    ];

    protected  $table = "users";

    protected $fillable = [
        "rights_depending",
        "departement_id",
        "superior_id",
        "role_id",
        "job_id",
        "gender",
        "login",
        "email",
        "password",
        "first_name",
        "last_name",
        "birth_day",
        "phone_number",
        "cin",
        "address",
        "photo",
        "signature",
        "function",
        "disabled",
        "is_verified",
        "last_login",
    ];

    public function departement()
    {
        return $this->hasOne('App\Departement', "id", "departement_id");
    }

    public function rights()
    {
        return $this->belongsToMany('App\Right', "user_rights", "user_id", "right_id");
    }

    public function role()
    {
        return $this->hasOne('App\Role', "id", "role_id");
    }




    # ----------------------------------------






    public function costumers()
    {
        return $this->hasMany(\App\Models\Costumer::class, 'created_by');
    }

    public function installerproducts()
    {
        return $this->hasMany(\App\Models\Installerproduct::class, 'created_by');
    }

    public function interventiondetails()
    {
        return $this->hasMany(\App\Models\Interventiondetail::class, 'created_by');
    }

    public function interventiondetailsproducts()
    {
        return $this->hasMany(\App\Models\Interventiondetailsproduct::class, 'created_by');
    }

    public function interventions()
    {
        return $this->hasMany(\App\Models\Intervention::class, 'created_by');
    }

    public function orders()
    {
        return $this->hasMany(\App\Models\Order::class, 'created_by');
    }

    public function products()
    {
        return $this->hasMany(\App\Models\Product::class, 'created_by');
    }

    public function vehicles()
    {
        return $this->hasMany(\App\Models\Vehicle::class, 'created_by');
    }

    

    /**
     * Get the work of user
     */
    public function job()
    {
        return $this->belongsTo('App\UserJob', "job_id", "id");
    }

    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */

    public function getJWTCustomClaims()
    {
        return [
            "user_id" => $this->id,
        ];
    }

}


