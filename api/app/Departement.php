<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departement extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected  $table = "user_departement";

    protected $fillable = [
        "departement_name",
        "description",
    ];

    public function rights()
    {
        return $this->belongsToMany('App\Right', "departement_rights", "departement_id", "right_id");
    }
}
