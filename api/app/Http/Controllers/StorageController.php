<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class StorageController extends Controller
{
    public function boitier_gps_imeis() {
        
        $doc_name = "boitier_gps_imeis.xlsx";

        return $this->getDoc($doc_name);
    
    }

    private function getDoc($doc_name) {
        $path = storage_path("app\documents\\" . $doc_name);

        $headers = [
            'Content-Type' => mime_content_type($path),
        ];

        return response()->download($path, $doc_name, $headers);
    }


    
}

