<?php

namespace App\Http\Controllers;

use App\inventory_personnel;
use App\product_stock;
use App\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use setasign\Fpdi\Fpdi;



class StocksController extends Controller
{
    public function getStocksBoitier()
    {
        $stockBoitier = DB::table('product_stock')
            ->leftJoin('inventory_personnel','product_stock.id','=','inventory_personnel.product_stock_id')
            ->leftJoin('tiers as t1', 'inventory_personnel.client_id','=','t1.id')
            ->where('inventory_personnel.deleted_at','=',NULL)
            ->leftjoin('orders','orders.id','=','product_stock.order_id')
            ->join('products','products.id','=','product_stock.product_id')
            ->join('tiers as t2','t2.id','=','products.provider_id')
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','like', '%' .'boitier'.'%')
            ->where('product_stock.deleted_at','=',NULL)
            ->select('product_stock.*','products.label','t2.nom','t2.id as provider_id','orders.reference','orders.ship_date','inventory_personnel.bl_document','t1.nom as client')
            ->orderBy('product_stock.id','desc')
            ->distinct()
            ->get();
            
        return response()->json($stockBoitier,201);
    }

    public function getStockSim()
    {
        $stockSim = DB::table('product_stock')
            ->leftjoin('orders','orders.id','=','product_stock.order_id')
            ->join('products','products.id','=','product_stock.product_id')
            ->join('tiers as t2','t2.id','=','products.provider_id')
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','like', '%' .'carte sim'.'%')
            ->where('product_stock.deleted_at','=',NULL)
            ->leftJoin('inventory_personnel','product_stock.id','=','inventory_personnel.product_stock_id')
            ->where('inventory_personnel.deleted_at','=',NULL)
            ->leftJoin('tiers as t1', 'inventory_personnel.client_id','=','t1.id')
            ->select('product_stock.*','products.label','t2.nom','t2.id as provider_id','orders.reference','orders.ship_date','inventory_personnel.bl_document','t1.nom as client')
            ->orderBy('product_stock.id','desc')
            ->distinct()
            ->get();

        return response()->json($stockSim,201);
    }

    public function refOrderBoitier()
    {
        $refOrderB = DB::table('orders')
            ->where('orders.deleted_at', '=', NULL)
            ->join('products', 'products.id', '=', 'orders.product_id')
            ->join('product_categories', 'product_categories.id', '=', 'products.category_id')
            ->where('product_categories.category_name', 'like', '%' . 'boitier' . '%')
            ->where('orders.deleted_at', '=', NULL)
            ->select('orders.*')
            ->get();
        return response()->json($refOrderB, 201);
    }

    public function refOrderSim()
    {
        $refOrderS = DB::table('orders')
            ->where('orders.deleted_at', '=', NULL)
            ->join('products', 'products.id', '=', 'orders.product_id')
            ->join('product_categories', 'product_categories.id', '=', 'products.category_id')
            ->where('product_categories.category_name', 'like', '%' . 'carte sim' . '%')
            ->where('orders.deleted_at', '=', NULL)
            ->select('orders.*')
            ->get();
        return response()->json($refOrderS, 201);
    }

    public function getProduitsBoitiers()
    {
        $product = DB::table('products')
            ->join('product_categories', 'product_categories.id', '=', 'products.category_id')
            ->where('product_categories.category_name', 'like', '%' . 'boitier' . '%')
            ->where('products.deleted_at', '=', NULL)
            ->select('products.label', 'products.id')
            ->get();
        return response()->json($product, 201);
    }

    public function getProduitsSim()
    {
        $product = DB::table('products')
            ->join('product_categories', 'product_categories.id', '=', 'products.category_id')
            ->where('product_categories.category_name', 'like', '%' . 'carte sim' . '%')
            ->where('products.deleted_at', '=', NULL)
            ->select('products.label', 'products.id')
            ->get();
        return response()->json($product, 201);
    }

    public function getInsta()
    {
        $insta = DB::table('product_stock')
             -> join('inventory_personnel','inventory_personnel.product_stock_id','=','product_stock.id')
             ->leftJoin('users','users.id','=','inventory_personnel.receiver_id')
             ->where('inventory_personnel.deleted_at','=',NULL)
             ->select('inventory_personnel.receiver_id','users.first_name','users.last_name')
             ->get();
            return response()->json($insta,201);
    }

    public function store(Request $request)
    {
        $imei = $request->get('imei');
        $product_id = $request->get('product_id');

        if($request->has('gsm'))
        {
            $gsm = $request->get('gsm');
            $storeProductStock = DB::table('product_stock')
               ->insert([
                   'product_id' => $product_id,
                   'imei' => $imei,
                   'gsm' => $gsm,
                   'custom_reference'=>'AJOUT_MANUEL',
               ]);
            return response()->json($storeProductStock, 201);
        }
        else
        {
            $storeProductStock = DB::table('product_stock')
                ->insert([
                    'product_id' => $product_id,
                    'imei' => $imei,
                    'custom_reference'=>'AJOUT_MANUEL',
                ]);
            return response()->json($storeProductStock, 201);

        }

    }

    public function bloque(Request $request)
    {
        $idData = ($request['selectedBoitiers']);

        foreach ($idData as $key => $value) {
            DB::table('product_stock')
                ->where('id', '=', $value)
                ->update([
                    'status' => '3'
                ]);
        }
        return ["ok" => 1];
    }

    public function activer(Request $request)
    {
        $date = $request->get('date');
        $idData = $request->get("dataIds");

        foreach ($idData as $key => $value) {
            DB::table('product_stock')
                ->where('id', '=', $value)
                ->update([
                    'disabled' => '0',
                    'activation_date' => $date
                ]);
        }
        return ["ok" => 1];
    }

    public function debloque(Request $request)
    {
        $idData = ($request['selectedBoitiers']);

        foreach ($idData as $id) {
            DB::table('product_stock')
                ->where('id', '=', $id)
                ->update([
                    'status' => '0'
                ]);
        }
        return ["ok" => 1];
    }

    public function getInstallateurs()
    {
        $installateurs = DB::table('users')
            ->join('user_jobs', 'user_jobs.id', '=', 'users.job_id')
            ->where('user_jobs.job_title', 'like', '%' . 'installateur' . '%')
            ->where('users.deleted_at', '=', NULL)
            ->select('users.*')
            ->get();
        return response()->json($installateurs, 201);
    }

    public function affecter(Request $request)
    {
        $idData = $request->get("dataIds");
        $user_id = $request->get('user_id');
        $date = Carbon::now();
        $month = Option :: where('option_name','=','month')->first();
        $currentMonth =  $date->format('m');

        if($currentMonth > $month->option_value)
        {
            $updateOption =  DB::table('options')->where('option_name','=','BL')->update(['option_value' =>0 ]);
            $updateOptions =  DB::table('options')->where('option_name','=','month')->update(['option_value' =>$currentMonth ]);
            $nbrBL = Option :: where('option_name','=','BL')->first();
            $referenceBL ='BL'.($date->format('ymd')).'-'.$nbrBL->option_value+=1;
        }
        else
        {
            if($currentMonth < $month->option_value)
            {
                $updateOption =  DB::table('options')->where('option_name','=','BL')->update(['option_value' =>0 ]);
                $updateOptions =  DB::table('options')->where('option_name','=','month')->update(['option_value' =>$currentMonth ]);
                $nbrBL = Option :: where('option_name','=','BL')->first();
                $referenceBL ='BL'.($date->format('ymd')).'-'.$nbrBL->option_value+=1;
            }
            else
            {
                $nbrBL = Option :: where('option_name','=','BL')->first();
                $numberBL = $nbrBL->option_value+=1;
                $referenceBL ='BL'.($date->format('ymd')).'-'.$numberBL;
                $updateOption =  DB::table('options')->where('option_name','=','BL')->update(['option_value'=>$numberBL]);
            }

        }

        if($request->has('received_id'))
        {
            $received_id = $request->get('received_id');
            foreach ($idData as $id)
            {

                $updateStatus = DB::table('product_stock')
                    ->where('id','=',$id)
                    ->where('status','=','0')
                    ->update([
                        'status' =>'1'
                    ]);

                if($updateStatus)
                {
                    DB::table('inventory_personnel')
                        ->insert([
                            'product_stock_id' => $id,
                            'distributor_id' => $user_id,
                            'receiver_id'=>$received_id,
                            'accepted'=> '1',
                            'assignment_date'=>$date,
                        ]);
                }

            }

            return ["ok" => 0];
        }
        else
        {
            $response = ["ok" => 0, "reference" => NULL];

            if($request->has('ssid'))
            {
                $ssidData = $request->get('ssid');
                
                foreach ($idData as $id)
                {
                    $updateStatus = DB::table('product_stock')
                        ->where('id','=',$id)
                        ->where('status','=','0')
                        ->update([
                            'status' =>'2'
                        ]);

                    if($updateStatus)
                    {
                        DB::table('inventory_personnel')
                            ->insert([
                                'product_stock_id' => $id,
                                'distributor_id' => $user_id,
                                'accepted'=> '1',
                                'assignment_date'=>$date,
                                'client_id'=>$request->get('client'),
                                'bl_document'=>$referenceBL.'.pdf',
                            ]);
                    }
                }

                foreach ($ssidData as $ssid)
                {
                    $productID = DB::table('product_stock')
                                ->where('imei','=',$ssid)
                                ->select('id')
                                ->first();

                    DB::table('product_stock')
                        ->where('id','=',$productID->id)
                        ->update([
                            'status' =>'2'
                        ]);

                        DB::table('inventory_personnel')
                        ->insert([
                            'product_stock_id' => $productID->id,
                            'distributor_id' => $user_id,
                            'accepted'=> '1',
                            'assignment_date'=>$date,
                            'client_id'=>$request->get('client'),
                            'bl_document'=>$referenceBL.'.pdf',
                        ]);

                        $response["ok"] = 1;
                        $response["reference"] = $referenceBL;
                }

                return $response;
            }
            else
            {
                foreach ($idData as $id)
                {
                    $updateStatus = DB::table('product_stock')
                        ->where('id','=',$id)
                        ->where('status','=','0')
                        ->update([
                            'status' =>'2'
                        ]);

                    if($updateStatus) {
                        DB::table('inventory_personnel')
                            ->insert([
                                'product_stock_id' => $id,
                                'distributor_id' => $user_id,
                                'accepted' => '1',
                                'assignment_date' => $date,
                                'client_id' => $request->get('client'),
                                'bl_document' => $referenceBL . '.pdf',
                            ]);

                        $response["ok"] = 1;
                        $response["reference"] = $referenceBL;
                    }
                }
                return $response;
            }
        }
             
    }

    public function generateBL(Request $request)
    {
        $idClient= $request->get('client');
        $idData = $request->get("dataIds");
        $referenceBL = $request->get("reference");

        Log::info( $referenceBL);
        $infoClient = DB::table('tiers')
            ->where('id','=',$idClient)
            ->select('tiers.*')
            ->get();

        header('Access-Control-Allow-Origin: *');
        $pdf = new Fpdi();
        $pdf->setSourceFile('resources/BL.pdf');
        $tplIdx = $pdf->importPage(1); 
        $pdf->AddPage();
        $pdf->useTemplate($tplIdx, 10, 10, 200);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica','', 10);

        //Date création
        $date=Carbon::now();
        $pdf->SetXY(169,43.2);
        $pdf->Write(0,$date->toDateString());

        // Reference
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(169, 38.5);
        $pdf->Write(0,$referenceBL);

        // Name Costumer
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica', '', 10);
        $name=$infoClient[0]->nom;
        $pdf->SetXY(120.9, 65.8);
        $pdf->Write(0,utf8_decode($name));

        //phone_number

        $phone=$infoClient[0]->phone;
        $pdf->SetXY(120.9,74.5);
        $pdf->Write(0,$phone);

        //mail
        $mail = $infoClient[0]->email;
        $pdf->SetXY(120.9,82.6);
        $pdf->Write(0,$mail); 

        //address
        $address = $infoClient[0]->address;
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(124,90.4);
        $pdf->Write(0,utf8_decode($address));


        if($request->has('ssid'))
        {
            $SSIData = $request->get("ssid");
            $y= 124;
            foreach ($SSIData as $imei){

                $gsm = DB::table('product_stock')
                     ->where('imei','=',$imei)
                     ->select('gsm')
                     ->get();

                $pdf->SetFont('Helvetica', '', 10);
                $pdf->SetXY(89,$y+=5);
                $pdf->Write(0,$gsm[0]->gsm);    
            }
        }

        $y= 124;
        foreach ($idData as $id){
            $imei = DB::table('product_stock')
                    ->where('id','=',$id)
                    ->select('imei')
                    ->get();

            $pdf->SetFont('Helvetica', '', 10);
            $pdf->SetXY(33,$y+=5);
            $pdf->Write(0,$imei[0]->imei);    
        }

        $pdf->output($referenceBL.'.pdf','D');

    }

    public function generateBLSIM(Request $request)
    {
        $idClient= $request->get('client');
        $idData = $request->get("dataIds");
        $referenceBL = $request->get("reference");

        Log::info( $referenceBL);
        $infoClient = DB::table('tiers')
            ->where('id','=',$idClient)
            ->select('tiers.*')
            ->get();

        header('Access-Control-Allow-Origin: *');
        $pdf = new Fpdi();
        $pdf->setSourceFile('resources/BL.pdf');
        $tplIdx = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tplIdx, 10, 10, 200);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica','', 10);

        //Date création
        $date=Carbon::now();
        $pdf->SetXY(169,43.2);
        $pdf->Write(0,$date->toDateString());

        // Reference
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(169, 38.5);
        $pdf->Write(0,$referenceBL);

        // Name Costumer
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica', '', 10);
        $name=$infoClient[0]->nom;
        $pdf->SetXY(120.9, 65.8);
        $pdf->Write(0,utf8_decode($name));

        //phone_number

        $phone=$infoClient[0]->phone;
        $pdf->SetXY(120.9,74.5);
        $pdf->Write(0,$phone);

        //mail
        $mail = $infoClient[0]->email;
        $pdf->SetXY(120.9,82.6);
        $pdf->Write(0,$mail);

        //address
        $address = $infoClient[0]->address;
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(124,90.4);
        $pdf->Write(0,utf8_decode($address));

        $y= 124;
        foreach ($idData as $id){
            $gsm = DB::table('product_stock')
                ->where('id','=',$id)
                ->select('gsm')
                ->get();

            $pdf->SetFont('Helvetica', '', 10);
            $pdf->SetXY(89,$y+=5);
            $pdf->Write(0,$gsm[0]->gsm);
        }

        $pdf->output($referenceBL.'.pdf','D');

    }

    public function generateBLPV(Request $request)
    {
        $idClient = $request->get('client');
        $referenceBL = $request->get("reference");
        $product_id = $request->get("productId");
        $output = $request->get('output');


        $infoClient = DB::table('tiers')
            ->where('id','=',$idClient)
            ->select('tiers.*')
            ->get();

        Log::info($infoClient);
        $designation_product = DB::table('products')
            ->where('id','=',$product_id)
            ->select('products.*')
            ->get();
        Log::info($designation_product);
        header('Access-Control-Allow-Origin: *');
        $pdf = new Fpdi();
        $pdf->setSourceFile('resources/BLPEV.pdf');
        $tplIdx = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tplIdx, 10, 10, 200);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica','', 10);

        //Date création
        $date=Carbon::now();
        $pdf->SetXY(169,43.2);
        $pdf->Write(0,$date->toDateString());

        // Reference
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(169, 38.5);
        $pdf->Write(0,$referenceBL);

        // Name Costumer
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Helvetica', '', 10);
        $name=$infoClient[0]->nom;
        $pdf->SetXY(124, 65.8);
        $pdf->Write(0,utf8_decode($name));

        //phone_number

        $phone=$infoClient[0]->phone;
        $pdf->SetXY(120.9,74.5);
        $pdf->Write(0,$phone);

        //mail
        $mail = $infoClient[0]->email;
        $pdf->SetXY(121.5,82.1);
        $pdf->Write(0,$mail);

        //address
        $address = $infoClient[0]->address;
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(128,90);
        $pdf->Write(0,utf8_decode($address));

        $designation = $designation_product[0]->label;
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(85,120);
        $pdf->Write(0,utf8_decode($designation));

        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(180,120);
        $pdf->Write(0,$output);



        $pdf->output($referenceBL.'.pdf','D');

    }

    public function getFile($doc_name)
    {
        $path = storage_path("app/BL/$doc_name");

        $headers = [
            'Content-Type' => mime_content_type($path),
        ];
        return response()->download($path, $doc_name, $headers);
    }

    public function liberer(Request $request)
    {
        $idData = ($request['selectedBoitiers']);

        foreach ($idData as $id)
        {
            $updateStatus = DB::table('product_stock')
                ->where('id','=',$id)
                ->where('status','=',1)
                ->update([
                    'status' => '0'
                ]);

            if($updateStatus)
                inventory_personnel::where('product_stock_id', $id)->delete();
        }
        return ["ok" => 1];
    }

    public function retourner(Request $request)
    {
        $idData = ($request['selectedBoitiers']);

        foreach ($idData as $id) {
            DB::table('product_stock')
                ->where('id', '=', $id)
                ->update([
                    'status'=>'0',
                    'retourner' =>'1'
                ]);

            inventory_personnel::where('product_stock_id', $id)->delete();
        }
        return ["ok" => 1];
    }

    public function transferer(Request $request)
    {
        $received_id = $request->get('received_id');
        $idData = $request->get("dataIds");
        $user_id = $request->get('user_id');
        $date = Carbon::now();
        foreach ($idData as $id)
        {
            $delete = inventory_personnel::where('product_stock_id', $id)->delete();

            if($delete)
            {
                DB::table('inventory_personnel')
                    ->insert([
                        'product_stock_id' => $id,
                        'distributor_id' => $user_id,
                        'receiver_id'=>$received_id['receiver_id'],
                        'accepted'=> '1',
                        'assignment_date'=>$date,
                    ]);
            }

        }
        return ["ok" => 1];
    }

    public function getClients()
    {
        $clients = DB::table('tiers')
            ->where('deleted_at', '=', Null)
            ->where('client', '=', '1')
            ->orWhere(function ($query) {
                $query->where('client', '=', '1')
                    ->where('fournisseur', '=', '1')
                    ->where('deleted_at', '=', Null);
            })
            ->select('tiers.id', 'tiers.nom')
            ->get();
        return response()->json($clients, 201);
    }

    public function getSsid()
    {
        $SSID = DB::table('product_stock')
            ->where('gsm', '!=', Null)
            ->whereIn('status', ['0', '4'])
            ->select('imei')
            ->get();
        return response()->json($SSID, 201);
    }


    public static function changeProductsStatus($product_id, $status)
    {
        $available_status = [0, 1, 2, 3];

        if (!array_has($available_status, $status))
            return;

        return \DB::table('product_stock')
            ->where('id', '=', $product_id)
            ->update(["status" => intval($status) + 1]);
    }

    public static function cps($product_id, $status)
    {
        $s = is_numeric($status) ? (string)$status : $status;
        return product_stock::Where('id', '=', $product_id)
            ->update(["status" => $s]);
    }

    public static function isProductStatus($product_id, $status)
    {
        $available_status = [0, 1, 2, 3];

        if (!array_has($available_status, $status))
            return;

        return \DB::table('product_stock')->where('id', '=', $product_id)->where('status', '=', $status)->count() == 1 ? true : false;
    }


    function getProductsByCategory($products, $category = "both")
    {
        if (count($products) < 1) return [];
        $boxes = [];
        $cards = [];

        foreach ($products as $obj => $p)
            if ($p->gsm == Null) array_push($boxes, $p);


        foreach ($products as $obj => $p)
            if ($p->gsm != Null) array_push($cards, $p);

        switch ($category) {
            case "boxes";
                return $boxes;
                break;
            case "cards":
                return $cards;
                break;
            default:
                return ["boxes" => $boxes, "cards" => $cards];
                break;
        }

    }

    function getProductInstalledByVehiculeId($vehicule_id)
    {


        /*

         * -- OPENTECH

            # installed products in intervention de type "i"

            select product_stock.imei from vehicules
            join intervention_details on  intervention_details.vehicule_id = vehicules.id
            join product_stock on (intervention_details.box_imie = product_stock.id or intervention_details.card_ssid = product_stock.id)
            where vehicules.id = 27  and intervention_details.type = "i" and intervention_details.deleted_at is null and product_stock.deleted_at is null

                    # uninstalled products in intervention de type "cs" and "cb"

                    select product_stock.imei from vehicules
                    join intervention_details on  intervention_details.vehicule_id = vehicules.id
                    join product_stock on (intervention_details.old_box_imie = product_stock.id or intervention_details.old_card_ssid = product_stock.id)
                    where vehicules.id = 27  and intervention_details.type in ("cb", "cs") and intervention_details.deleted_at is null and product_stock.deleted_at is null

                    # uninstalled products in intervention de type "d"

                    select product_stock.imei from vehicules
                    join intervention_details on  intervention_details.vehicule_id = vehicules.id
                    join product_stock on (intervention_details.box_imie = product_stock.id or intervention_details.card_ssid = product_stock.id)
                    where vehicules.id = 27  and intervention_details.type = "d" and intervention_details.deleted_at is null and product_stock.deleted_at is null





            * -- CLIENT
                # installed client boxes and cards


                select intervention_details.box_client, intervention_details.card_client from intervention_details
                join vehicules on  intervention_details.vehicule_id = vehicules.id
                where vehicules.id = 27  and intervention_details.type = "i" and (intervention_details.box_client is not null or intervention_details.card_client is not null)
                and intervention_details.deleted_at is null

        */



        $box = "";
        $isClientBox = false;
        $card = "";
        $isClientCard = false;

        /* stock [B,S]*/

        $opentech_products = \DB::table('product_stock')
            ->where('product_stock.deleted_at', '=', Null)
            ->where('intervention_details.vehicule_id', '=', $vehicule_id)
            ->where(function($query) {
                $query->where('intervention_details.type', '=', 'i')
                    ->orWhere('intervention_details.type', '=', 'cb')
                    ->orWhere('intervention_details.type', '=', 'cs');
            })
            ->join('intervention_details', function ($join) {
                $join->on('intervention_details.box_imie', '=', 'product_stock.id');
                $join->orOn('intervention_details.card_ssid', '=', 'product_stock.id');
            })
            ->select('product_stock.*')
            ->get();

        if(count($opentech_products) > 0) {
            foreach ($opentech_products as $obj => $p) {

                if($p->gsm == Null) { // box
                    $box = $p->imei;
                    $isClientBox = false;
                }
                if($p->gsm != Null) { // card
                    $card = $p->imei;
                    $isClientCard = false;
                }
            }
        }

        /* stock [CUSTOM --> Client Equipements ] */

        $client_products = \DB::table('intervention_details')
            ->where('intervention_details.deleted_at', '=', Null)
            ->where('intervention_details.vehicule_id', '=', $vehicule_id)
            ->where('intervention_details.box_imie', '=', Null)
            ->orWhere('intervention_details.card_ssid', '=', Null)
            ->select('intervention_details.*')
            ->get();


        if(count($client_products) > 0) {
            foreach ($client_products as $obj => $p) {
                if($p->box_client != Null) { // boxe
                    $box = $p->box_client;
                    $isClientBox = true;
                }
                if($p->card_client != Null) { // card
                    $card = $p->card_client;
                    $isClientCard = true;
                }
            }
        }


        return [
            "box" => $box,
            "isClientBox" => $isClientBox,
            "card" => $card,
            "isClientCard" => $isClientCard,
        ];
    }




    function getInstalledProductsByClientId($client_id)
    {

        /*
            select product_stock.* from product_stock
            join intervention_details on product_stock.id = intervention_details.box_imie
            join interventions on intervention_details.intervention_id = interventions.id
            where intervention_details.type in ("i") and intervention_details.deleted_at is null and product_stock.deleted_at is null and interventions.client_id = 9

            ==============================================================================================================================================


            select product_stock.* from product_stock
            join intervention_details on product_stock.id = intervention_details.box_imie
            join interventions on intervention_details.intervention_id = interventions.id
            and intervention_details.deleted_at is null
            and product_stock.deleted_at is null
            where intervention_details.type in ("i", "cb", "cs")
            and interventions.client_id = 12
            and product_stock.retourner = "1"

            ==============================================================================================================================================


            select product_stock.* from product_stock join intervention_details on product_stock.id = intervention_details.box_imie or product_stock.id = intervention_details.card_ssid join interventions on intervention_details.intervention_id = interventions.id where intervention_details.deleted_at is null and product_stock.deleted_at is null and intervention_details.type in ("i", "cb", "cs") and product_stock.id and interventions.client_id = 12  and product_stock.id NOT in (select intervention_details.old_box_imie from product_stock join intervention_details on product_stock.id = intervention_details.box_imie  join interventions on intervention_details.intervention_id = interventions.id where intervention_details.deleted_at is null and product_stock.deleted_at is null and intervention_details.type in ("cb", "cs") and interventions.client_id = 12 union select intervention_details.old_box_imie from product_stock join intervention_details on product_stock.id = intervention_details.card_ssid  join interventions on intervention_details.intervention_id = interventions.id where intervention_details.deleted_at is null and product_stock.deleted_at is null and intervention_details.type in ("cb", "cs") and interventions.client_id = 12)


            ==============================================================================================================================================

            SELECT product_stock.*
            FROM   product_stock
                   JOIN intervention_details
                     ON product_stock.id = intervention_details.box_imie
                         OR product_stock.id = intervention_details.card_ssid
                   JOIN interventions
                     ON intervention_details.intervention_id = interventions.id
            WHERE  intervention_details.deleted_at IS NULL
                   AND product_stock.deleted_at IS NULL
                   AND intervention_details.type IN ( "i", "cb", "cs" )
                   AND product_stock.id
                   AND interventions.client_id = 9
                   AND product_stock.id not in (

            SELECT intervention_details.old_box_imie
            FROM   product_stock
                   JOIN intervention_details ON product_stock.id = intervention_details.box_imie
                   JOIN interventions ON intervention_details.intervention_id = interventions.id
            WHERE  intervention_details.deleted_at IS NULL
                   AND product_stock.deleted_at IS NULL
                   AND intervention_details.type = "cb"
                   AND interventions.client_id = 9
            UNION
            SELECT intervention_details.old_card_ssid
            FROM   product_stock
                   JOIN intervention_details ON product_stock.id = intervention_details.card_ssid
                   JOIN interventions ON intervention_details.intervention_id = interventions.id
            WHERE  intervention_details.deleted_at IS NULL
                   AND product_stock.deleted_at IS NULL
                   AND intervention_details.type = "cs"
                   AND interventions.client_id = 9

            )

        */

        $client_id = intval(strip_tags(stripslashes($client_id)));

        /*$sql = "
            SELECT product_stock.*
            FROM   product_stock
                   JOIN intervention_details
                     ON product_stock.id = intervention_details.box_imie
                         OR product_stock.id = intervention_details.card_ssid
                   JOIN interventions
                     ON intervention_details.intervention_id = interventions.id
            WHERE  intervention_details.deleted_at IS NULL
                   AND product_stock.deleted_at IS NULL
                   AND intervention_details.type IN ( \"i\", \"cb\", \"cs\" )
                   AND product_stock.id
                   AND interventions.client_id = $client_id
                   AND product_stock.id not in (

            SELECT intervention_details.old_box_imie
            FROM   product_stock
                   JOIN intervention_details ON product_stock.id = intervention_details.box_imie
                   JOIN interventions ON intervention_details.intervention_id = interventions.id
            WHERE  intervention_details.deleted_at IS NULL
                   AND product_stock.deleted_at IS NULL
                   AND intervention_details.type = \"cb\"
                   AND interventions.client_id = $client_id
            UNION
            SELECT intervention_details.old_card_ssid
            FROM   product_stock
                   JOIN intervention_details ON product_stock.id = intervention_details.card_ssid
                   JOIN interventions ON intervention_details.intervention_id = interventions.id
            WHERE  intervention_details.deleted_at IS NULL
                   AND product_stock.deleted_at IS NULL
                   AND intervention_details.type = \"cs\"
                   AND interventions.client_id = $client_id

            )
        ";

        $products = \DB::select(\DB::raw($sql));

        return $this->getProductsByCategory($products);*/

        $return = [
            "opentech" => [
                "boxes" => [],
                "cards" => [],
                "total" => [
                    "boxes" => 0,
                    "cards" => 0
                ]
            ],
            "client" => [
                "boxes" => [],
                "cards" => [],
                "total" => [
                    "boxes" => 0,
                    "cards" => 0
                ]
            ]
        ];



        $vehicules = VehiculeController::getVehiculeByClientId($client_id);

        if($vehicules && !count($vehicules))
            return ["ok" => 0, "error" => "this client dosn't have any vehicule"];

        foreach ($vehicules as $vehicule => $v) {
            $vehicule_id = $v->id;

            $history = VehiculeController::getVehiculeHistory($vehicule_id)["output"];

            foreach ($history["opentech"]["boxes"] as $opentech => $box)
                $return["opentech"]["boxes"][] = $box;

            foreach ($history["opentech"]["cards"] as $opentech => $card)
                $return["opentech"]["cards"][] = $card;

            foreach ($history["client"]["boxes"] as $client => $box)
                $return["client"]["boxes"][] = $box;

            foreach ($history["client"]["cards"] as $client => $card)
                $return["client"]["cards"][] = $card;
        }


        $return["opentech"]["total"]["boxes"] = count($return["opentech"]["boxes"]);
        $return["opentech"]["total"]["cards"] = count($return["opentech"]["cards"]);
        $return["client"]["total"]["boxes"] = count($return["client"]["boxes"]);
        $return["client"]["total"]["cards"] = count($return["client"]["cards"]);


        return $return;
    }


    public static  function getProductById($product_id) {
        return \DB::table("product_stock")->where("id", "=", $product_id)->where("deleted_at", "=", null)->first();
    }

    public static  function getClientProduct($product, $category = "box") {

        if($category == "box") {
            $product = \DB::table("intervention_details")->where("box_client", "=", $product)->where("deleted_at", "=", null)->first();
            if($product) {
                return [
                    "box_client" => $product->box_client,
                    "box_client_brand" => $product->box_client_brand,
                ];
            }
        }

        if($category == "card") {
            $product = \DB::table("intervention_details")->where("card_client", "=", $product)->where("deleted_at", "=", null)->first();
            if($product) {
                return [
                    "card_client" => $product->card_client,
                    "card_client_operator" => $product->card_client_operator,
                ];
            }
        }

        return;
    }


    public static  function getProductProvider($product_id) {
        /*
            select * from tiers
            join products on products.provider_id = tiers.id
            join product_stock on product_stock.product_id = products.id
            where tiers.deleted_at is null
            and product_stock.id = 2;
        */

        return \DB::table('tiers')
            ->where('tiers.deleted_at','=',Null)
            ->where('product_stock.id','=',$product_id)
            ->join('products','products.provider_id','=','tiers.id')
            ->join('product_stock','product_stock.product_id','=','products.id')
            ->first();
    }



        

    public function getOtherProducts()
    {
        $otherProducts = DB::table('products')
            ->where('products.deleted_at','=',NULL)
            ->join('orders','orders.product_id','=','products.id')
            -> join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','not like', '%' .'carte sim'.'%')
            ->where('product_categories.category_name','not like', '%' .'boitier'.'%')
            ->select('products.label','products.id as idProduct')
            ->distinct()
            ->get();
            return response()->json($otherProducts,201);
    }

    public function getBulkProducts($id)
    {
        $bulkProducts = DB::table('bulk_products')
            ->leftJoin('users','bulk_products.installateur','=','users.id')
            ->leftJoin('tiers','bulk_products.client_id','=','tiers.id')
            ->join('orders','bulk_products.order_id','=','orders.id')
            ->leftJoin('tiers as t2','orders.provider_id','=','t2.id')
            ->join('products','orders.product_id','=','products.id')
            ->where('products.id','=',$id)
            ->select('bulk_products.*','t2.nom','orders.reference','products.label','users.first_name','users.last_name','tiers.nom as client','bulk_products.bl_document')
            ->orderBy('bulk_products.operation_date','asc')
            ->orderBy('bulk_products.created_at','asc')
            ->get();
            return response()->json($bulkProducts,201);
    }

    public function getiD($id)
    {
        $id= DB::table('bulk_products')
            ->join('orders','bulk_products.order_id','=','orders.id')
            ->join('products','orders.product_id','=','products.id')
            ->where('products.id','=',$id)
            ->select('bulk_products.*')
            ->orderBy('bulk_products.id','desc')
            ->first();
        return response()->json($id,201);
    }

    public function affecterProduct(Request $request)
    {
        $output = $request->get('output');
        $order_id = $request->get('order_id');

        $alert = DB::table('orders')
             ->where('orders.id',$order_id)
             ->join('products','orders.product_id','=','products.id')
             ->select('products.seuil_stock_alert')
             ->first();

        $seuil_stock_alert = $alert->seuil_stock_alert;
        $date = Carbon::now();
        $month = Option :: where('option_name','=','month')->first();
        $currentMonth =  $date->format('m');

        if($currentMonth > $month->option_value)
        {
            $updateOption =  DB::table('options')->where('option_name','=','BL')->update(['option_value' =>0 ]);
            $updateOptions =  DB::table('options')->where('option_name','=','month')->update(['option_value' =>$currentMonth ]);
            $nbrBL = Option :: where('option_name','=','BL')->first();
            $referenceBL ='BL'.($date->format('ymd')).'-'.$nbrBL->option_value+=1;

        }
        else
        {
            if($currentMonth < $month->option_value)
            {
                $updateOption =  DB::table('options')->where('option_name','=','BL')->update(['option_value' =>0 ]);
                $updateOptions =  DB::table('options')->where('option_name','=','month')->update(['option_value' =>$currentMonth ]);
                $nbrBL = Option :: where('option_name','=','BL')->first();
                $referenceBL ='BL'.($date->format('ymd')).'-'.$nbrBL->option_value+=1;
            }
            else
            {
                $nbrBL = Option :: where('option_name','=','BL')->first();
                $numberBL = $nbrBL->option_value+=1;
                $referenceBL ='BL'.($date->format('ymd')).'-'.$numberBL;
                $updateOption =  DB::table('options')->where('option_name','=','BL')->update(['option_value'=>$numberBL]);
            }
        }


        $data = $request->get('data');
        $reste = $request->get('reste');


        if($request->has('installateur'))
        {
            if(!empty($data))
            {
                $remaining = $data['remaining'];
                if(!empty($reste))
                {
                    if($remaining - $output < $seuil_stock_alert)
                        return ["ok" => 0, "error" => "Vous avez dépasser la limite de stock ".$seuil_stock_alert];
                    $stock_final = $data['remaining'] - $output ;
                    foreach ($reste as $r)
                    {
                        if(!empty($r['entered']))
                        {
                            $stock_final = $stock_final + $r['entered'];
                            Log::info('entree  '.$stock_final);
                            if($stock_final< $seuil_stock_alert)
                                return ["ok" => 0, "error" => "Vous avez dépasser la limite de stock ".$seuil_stock_alert];
                        }
                        if(!empty($r['output']))
                        {
                            $stock_final = $stock_final - $r['output'];
                            Log::info('sortie  '.$stock_final);

                            if($stock_final < $seuil_stock_alert)
                                return ["ok" => 0, "error" => "Vous avez dépasser la limite de stock ".$seuil_stock_alert];
                        }
                    }

                    $insertBulkProduct = DB::table('bulk_products')
                        ->insert([
                            'order_id' => $data['order_id'],
                            'entered' =>'0',
                            'output'=>$output,
                            'remaining'=> $data['remaining'] - $output,
                            'operation_date'=>$request->get('operation_date'),
                            'installateur'=> $request->get('installateur'),
                        ]);

                    $stock_final = $data['remaining'] - $output ;

                    foreach ($reste as $r)
                    {
                        if(!empty($r['entered']))
                        {
                            DB::table('bulk_products')
                                ->where('id','=',$r['id'])
                                ->update([
                                    'remaining' =>$stock_final + $r['entered']
                                ]);

                            $stock_final = $stock_final + $r['entered'];
                        }
                        else
                        {
                            DB::table('bulk_products')
                                ->where('id','=',$r['id'])
                                ->update([
                                    'remaining' =>$stock_final - $r['output']
                                ]);
                            $stock_final = $stock_final - $r['output'];
                        }
                    }
                    return ["ok" => 1, "message" => "Opération effectuer avec succès"];

                }
                else
                {
                    Log::info('je suis la');
                    $insertBulkProduct = DB::table('bulk_products')
                        ->insert([
                            'order_id' => $data['order_id'],
                            'entered' =>'0',
                            'output'=>$output,
                            'remaining'=> $remaining - $output,
                            'operation_date'=>$request->get('operation_date'),
                            'installateur'=> $request->get('installateur'),
                        ]);
                    return ["ok" => 1, "message" => "Opération effectuer avec succès"];
                }
            }
            else
                return ["ok" => 0, "error" => "Impossible d'affecter, veuillez vérfier votre stock"];
        }
        else
        {
            if(!empty($data))
            {
                $remaining = $data['remaining'];
                if(!empty($reste))
                {
                    if($remaining - $output < $seuil_stock_alert)
                        return ["ok" => 0, "error" => "Vous avez dépasser la limite de stock ".$seuil_stock_alert];
                    $stock_final = $data['remaining'] - $output ;
                    Log::info('stock au debut   '.$stock_final);
                    foreach ($reste as $r)
                    {
                        if(!empty($r['entered']))
                        {
                            $stock_final = $stock_final + $r['entered'];
                            Log::info('entree  '.$stock_final);
                            if($stock_final< $seuil_stock_alert)
                                return ["ok" => 0, "error" => "Vous avez dépasser la limite de stock ".$seuil_stock_alert];
                        }
                        if(!empty($r['output']))
                        {
                            $stock_final = $stock_final - $r['output'];
                            Log::info('sortie  '.$stock_final);

                            if($stock_final < $seuil_stock_alert)
                                return ["ok" => 0, "error" => "Vous avez dépasser la limite de stock ".$seuil_stock_alert];
                        }
                    }

                    $insertBulkProduct = DB::table('bulk_products')
                        ->insert([
                            'order_id' =>$data['order_id'],
                            'entered' =>'0',
                            'output'=>$output,
                            'remaining'=> $data['remaining'] - $output,
                            'operation_date'=>$request->get('operation_date'),
                            'client_id'=> $request->get('client'),
                            'bl_document' =>$referenceBL.'.pdf'
                        ]);

                    $stock_final = $data['remaining'] - $output ;

                    foreach ($reste as $r)
                    {
                        if(!empty($r['entered']))
                        {
                            DB::table('bulk_products')
                                ->where('id','=',$r['id'])
                                ->update([
                                    'remaining' =>$stock_final + $r['entered']
                                ]);

                            $stock_final = $stock_final + $r['entered'];
                        }
                        else
                        {
                            DB::table('bulk_products')
                                ->where('id','=',$r['id'])
                                ->update([
                                    'remaining' =>$stock_final - $r['output']
                                ]);
                            $stock_final = $stock_final - $r['output'];
                        }
                    }

                    return ["ok" => 2, "message" => "Affectation et BL génerer avec succès",'reference'=>$referenceBL];

                }
                else
                {
                    Log::info('je suis la');
                    $insertBulkProduct = DB::table('bulk_products')
                        ->insert([
                            'order_id' => $data['order_id'],
                            'entered' =>'0',
                            'output'=>$output,
                            'remaining'=> $remaining - $output,
                            'operation_date'=>$request->get('operation_date'),
                            'client_id'=> $request->get('client'),
                            'bl_document' =>$referenceBL.'.pdf'
                        ]);


                    return ["ok" => 2, "message" => "Affectation et BL génerer avec succès",'reference'=>$referenceBL];
                }
            }
            else
                return ["ok" => 0, "error" => "Impossible d'affecter, veuillez vérfier votre stock"];
        }
        
    }

    public function testGenerate($nomClient)
    {
        $infoClient = DB::table('tiers')
        ->where('nom','like', '%'.$nomClient.'%')
        ->select('tiers.*')
        ->get();
        return response()->json($infoClient,201);
    }

    public function storeBL(Request $request)
    {
        $fake_name = $request->reference.'.pdf';
        $path = $request->file('fichier')->storeAs(
            'BL', $fake_name
        );
    }

    public function listeAffectation($category_id)
    {
        $listeAffectation = DB::table('inventory_personnel')
                              ->where('inventory_personnel.deleted_at','=',NULL)
                              ->leftJoin('product_stock','inventory_personnel.product_stock_id','=','product_stock.id')
                              ->leftJoin('products','products.id','=','product_stock.product_id')
                              ->join('product_categories','product_categories.id','=','products.category_id')
                              ->where('product_categories.id','=', $category_id)
                              ->leftJoin('users','inventory_personnel.receiver_id','=','users.id')
                              ->leftJoin('tiers','inventory_personnel.client_id','=','tiers.id')
                              ->select('inventory_personnel.*','product_stock.imei','product_stock.gsm','products.label','product_stock.status','inventory_personnel.assignment_date','users.last_name','users.first_name','tiers.nom')
                              ->orderBy('inventory_personnel.accepted','asc')
                              ->orderBy('product_stock.status','asc')

            ->get();
            return response()->json($listeAffectation,201);
    }

    public function affectedToFitter(Request $request)
    {
        $idData = $request->get("dataIds");
        $distributor_id = $request->get('distributor_id');
        $receiver_id = $request->get('receiver_id');
        $date = Carbon::now();
        $confirm = true;
        foreach ($idData as $id)
        {
            $product_stock_id = DB::table('inventory_personnel')
                ->where('id',$id)
                ->select('product_stock_id')
                ->first();

            $insertInventory = DB::table('inventory_personnel')
                ->insert([
                    'product_stock_id' => $product_stock_id->product_stock_id,
                    'distributor_id' => $distributor_id,
                    'receiver_id' => $receiver_id,
                    'accepted'=> '0',
                    'assignment_date'=>$date,
                ]);
            $confirm = false;
        }
        if($confirm)
            return ["ok" => 0,"error"=>'Error lors d\'affectation' ];
        else
            return ["ok" => 1,"message"=>'Votre affectation est en attente d\'acceptation' ];

    }

    public function accepter(Request $request)
    {
        $id = $request->get('id');
        $product_stock_id = $request->get('product_stock_id');
        inventory_personnel::where('product_stock_id', $product_stock_id)->where('accepted','1')->delete();
        if(DB::table('inventory_personnel')
            ->where('id','=',$id)
            ->update([
                'accepted' => '1'
            ]))
        {
            return ["ok" => 1,"message"=>'Acceptation effectuer' ];
        }
        else
            return ["ok" => 0,"error"=>'Erreur Lors de l\'acceptation '];
    }

    public function refuser($id)
    {
        if(inventory_personnel::where('id', $id)->delete())
            return ["ok" => 1,"message"=>'Demande refuser' ];
        else
            return ["ok" => 0,"error"=>'Erreur '];

    }


}
