<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    function download() {

        $path = storage_path("app\documents\interventions\FI\FI1904-2-18_2_1BC7001.pdf");

        $headers = [
            'Content-Type' => mime_content_type($path),
        ];

        return response()->download($path, "FI1904-2-18_2_1BC7001.pdf", $headers);

    }
}
