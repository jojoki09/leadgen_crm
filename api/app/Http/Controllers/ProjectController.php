<?php

namespace App\Http\Controllers;

use App\Components\projects\models;
use App\Components\projects\Models\project;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\projects\Repositories\ProjectsRepository;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    /**
     * @var ProjectsRepository
     */
    private $ProjectsRepository;

    /**
     * GroupController constructor.
     * @param ProjectsRepository $ProjectsRepository
     */
    public function __construct(ProjectsRepository $ProjectsRepository)
    {
        $this->ProjectsRepository = $ProjectsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // Log::info(request()->all());


       $projectData = DB::table('projects')
            ->Join('categories','categories.id','=','projects.categorie_id')
            ->Join('adresses','adresses.project_id','=','projects.id')
            ->Join('villes','villes.id','=','adresses.ville_id')
            ->select('projects.*','categories.nom as categories','villes.nom as villes' )
            ->orderBy('projects.id','desc')
            ->get();


       log::info($projectData[0]->photo);

        foreach($projectData as $key => $oneData){

            $photo = $oneData->photo;

            if($photo) {
                $slices = explode('.', $photo);
                $ext = end($slices);
                $content = Storage::get('images/projets/' . $photo);
                $encoded_content = base64_encode($content);
                $imageData = 'data:' . 'image/' . $ext  . ';base64,' . $encoded_content;
                $projectData[$key]->photo= $imageData;
            } else {
                $imageData = false;
            }

        }


//        $data = $this->ProjectsRepository->index(request()->all());



        return response()->json($projectData,200);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Log::info( $request->all());

        $project =  $request->all();


        $validate = validator($request->all(),[
            'nom' => 'required',
            'photo' => 'mimes:jpeg,bmp,png,jpeg,gif|max:20481',
            'categorie_id' => 'required',
            'ville_id' => 'required',
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        if($request->hasFile('photo')) {
            $file = $request->file('photo');
            $fake_name = md5($project["nom"]) . '_' . $file->getClientOriginalName();

            $path = Storage::putFileAs(
                'images/projets', $request->file('photo'), $fake_name
            );

            $project["photo"] = $fake_name;
        }



        /** @var project $project */
        $project = $this->ProjectsRepository->create($project);

        if(!$project) return $this->sendResponseBadRequest("Failed create.");

        // attach to Categorie



         $project->categories()->attach($request->get('categorie_id',[]));



        $project->adresse()->create($request->all());

        return response()->json($project,200);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = $this->ProjectsRepository->find($id,['categories','adresse']);

        if(!$project) return $this->sendResponseNotFound();

        return $this->sendResponseOk($project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = validator($request->all(),[
            'nom' => 'required',
            'adr' => 'required',
            'categorie' => 'array',
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        $updated = $this->ProjectsRepository->update($id,$request->all());

        if(!$updated) return $this->sendResponseBadRequest("Failed update");

        // re-sync Categorie

        /** @var Projet $project */
        $project = $this->ProjectsRepository->find($id);

        $categoryIds = [];

        if($category = $request->get('categorie',[]))
        {
            foreach ($category as $categoryId => $shouldAttach)
            {
                if($shouldAttach) $categoryIds[] = $categoryId;
            }
        }

        $project->categories()->sync($categoryIds);

        //$project->adresse()->touch($request->all());
        return $this->sendResponseUpdated();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->ProjectsRepository->delete($id);
        } catch (\Exception $e) {
            return $this->sendResponseBadRequest("Failed to delete");
        }

        return $this->sendResponseDeleted();
    }
}
