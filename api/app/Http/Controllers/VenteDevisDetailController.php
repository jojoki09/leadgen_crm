<?php

namespace App\Http\Controllers;

use App\Components\Devis\Models\vente_devis_detail;
use App\Components\Devis\Repositories\VenteDevisDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PhpParser\JsonDecoder;


class VenteDevisDetailController extends Controller
{
    /**
     * @var VenteDevisDetailRepository
     */
    private $VenteDevisDetailRepository;

    /**
     * UserController constructor.
     * @param VenteDevisDetailRepository $VenteDevisDetailRepository
     */
    public function __construct(VenteDevisDetailRepository $VenteDevisDetailRepository)
    {
        $this->VenteDevisDetailRepository = $VenteDevisDetailRepository;
    }

    public function getDocument($id)
    {

        $documents = DB::table('vente_devis_details')
        ->where('vente_devis_id','=',"{$id}")
        ->select('vente_devis_details.*')
        ->get();


        foreach($documents as $key => $oneData){

            $photo = $oneData->doc;

            if($photo) {
                $slices = explode('.', $photo);
                $ext = end($slices);
                $content = Storage::get('images/document/' . $photo);
                $encoded_content = base64_encode($content);
                $imageData = 'data:' . 'image/' . $ext  . ';base64,' . $encoded_content;
                $documents[$key]->doc= $imageData;
                $documents[$key]->ext= 'download.' .$ext;

            }

        }

        Log::info($documents);


        return response()->json($documents,200);


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->VenteDevisDetailRepository->index(request()->all());



//        foreach($data as $key => $oneData){
//
//            $photo = $oneData->photo;
//
//            if($photo) {
//                $slices = explode('.', $photo);
//                $ext = end($slices);
//                $content = Storage::get('images/document/' . $photo);
//                $encoded_content = base64_encode($content);
//                $imageData = 'data:' . 'image/' . $ext  . ';base64,' . $encoded_content;
//                $data[$key]->photo= $imageData;
//            }
//
//        }

//        $devis_id = request()->get("devis_id") ? request()->get("devis_id") : "";
//        if(empty($devis_id))
//          $projectData = DB::table('vente_devis_details')
//          ->select('vente_devis_details.*' )
//          ->orderBy('vente_devis_details.vente_devis_id','desc')
//          ->get();
//
//         else
//         {
//             $projectData = DB::table('vente_devis_details')
//                 ->where('vente_devis_id','=',"{$devis_id}")
//                 ->join('type_action','type_action.type','=','vente_devis_details.')
//                 ->select('vente_devis_details.*' )
//                 ->orderBy('vente_devis_details.vente_devis_id','desc')
//                 ->get();
//             return $this->belongsTo(type_action::class,'type');
//
//         }
//
//         log::info($projectData);
//
//
//        foreach($projectData as $key => $oneData){
//            if (is_null ( $oneData['action_type']))
//            {
//                $action = $oneData['action_type']->code;
//                Log::info($action);
//            }
//
//
////            if(!$action) {
////                $projectData[$key]->action_type['code']= [];
////
////            }
//
//        }


//        if($data->action_type['code'] == null) {
//       $data->action_type['code'] = [];
//        }
        return response()->json($data,200);
    }
    public function getProcepectRelaunch()
    {

        Log::info("nice job");

        // TODO type == 2 order by created_at ( vente_devis_details) les gens qui ont effectué de visite apres les 48h
        $sql2 = "select `calendars`.`object`, `vente_devis_details`.`statut` , `contacts`.`nom` , `contacts`.`prenom` from `calendars` 
            inner join `partenaires` on `calendars`.`part_id` = `partenaires`.`part_id`
            inner join `vente_devis_details` on `calendars`.`part_id` = `vente_devis_details`.`vente_devis_id`
            inner join `contacts` on `contacts`.`partenaire_id` = `calendars`.`part_id`
            where `vente_devis_details`.`type` = 2 &&
            `vente_devis_details`.`article_id` IS NOT NULL &&
            DATE(`vente_devis_details`.`created_at`) >= CURDATE() - 2 AND DATE(`vente_devis_details`.`created_at`) <= CURDATE() &&
            DATE(`calendars`.`date_planif`) <= CURDATE() - 2 
        ";

         // le probléme c'est que la requet me raméne des doublons c'est ce qu'on a pas prévu

//        foreach ($sql2 as $key => $value)
//        {
//            // todo check every row is verified tehj condition or not ===> if vente_devisdetails type == 2 and article_id is not null in date betweeen curdate - 2 et aujourd'huit
//        if($value ==   )
//        {
//
//
//        }
//
//        }


        Log::info('-----------------------------------------');
        $data = DB::select(DB::raw($sql2));
        Log::info('-----------------------------------------');

        if ( empty ( $data ) ) {
            $sql2 = "select `calendars`.*,`contacts`.`prenom`,`partenaires`.*  from `calendars` 
            inner join `partenaires` on `calendars`.`part_id` = `partenaires`.`part_id`
            inner join `contacts` on `contacts`.`partenaire_id` = `calendars`.`part_id`
            where DATE(`calendars`.`date_planif`) <= CURDATE() - 2 
        ";

            $data = DB::select(DB::raw($sql2));

        }
        return response()->json($data,200);




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partner =  $request->all();


        $validate = validator($request->all(),[
            'vente_devis_id' => 'required',
            'photo' => 'mimes:jpeg,bmp,png,jpeg,gif,|max:20481',
            'type' => 'required'
        ]);


        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        if($request->hasFile('photo')) {
            $file = $request->file('photo');
            $fake_name = md5($partner["vente_devis_id"]) . '_' . $file->getClientOriginalName();

            $path = Storage::putFileAs(
                'images/document', $request->file('photo'), $fake_name
            );

            $partner["doc"] = $fake_name;
        }
        $vente_devis_detial = $this->VenteDevisDetailRepository->create($partner);

        return response()->json($vente_devis_detial,201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vente_devis_detail  $vente_devis_detail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $listesPrespectDetail = $this->VenteDevisDetailRepository->find($id);
        Log::info($listesPrespectDetail);

        if(!$listesPrespectDetail) return $this->sendResponseNotFound();
        return response()->json($listesPrespectDetail,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vente_devis_detail  $vente_devis_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(vente_devis_detail $vente_devis_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vente_devis_detail  $vente_devis_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vente_devis_detail $vente_devis_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vente_devis_detail  $vente_devis_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(vente_devis_detail $vente_devis_detail)
    {
        //
    }
}
