<?php

namespace App\Http\Controllers;

use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{
    public function index(Request $request)
    {
        $serviceData = DB::table('services')
            ->where('deleted_at','=',Null)
            ->orderBy('id','desc')
            ->get();
        return response()->json($serviceData,201);
    }

    public function storeService(Request $request)
    {
        if($service = services::create($request->all()))
        {
            return response()->json($service, 201);
        }
        else
            return response()->json(['data'=>'error in the insert'], 500);
    }

    public function updateService(Request $request)
    {
        $serviceUpdate = services::findOrFail($request->input('id'));
        if($service = $serviceUpdate->update($request->all()))
        {
            return response()->json($service, 201);
        }
        else
            return response()->json(['data'=>'error in the update'], 500);
    }

    public function deleteService($id)
    {
        if($service = services::find($id))
        {
            $service->delete();
            return response()->json(null, 201);
        }else
            return response()->json(['data'=>'error in the delete'], 500);
    }

    public function getServiceById($id)
    {
        $service = DB::table('services')
            ->where('services.id','=',$id)
            ->get()
            ->first();
        return response()->json($service,201);

    }

    public function getIdService()
    {
        if($id = DB::table('services')->orderBy('id','desc')->select('id')->pluck('id')->first())
            return response()->json($id, 201);
        else
            return response()->json(['data'=>'error in the get'], 500);
    }

}
