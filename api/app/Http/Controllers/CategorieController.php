<?php

namespace App\Http\Controllers;

use App\Components\projects\models;
use Illuminate\Http\Request;
use App\Components\projects\Repositories\CategorieRepository;

class CategorieController extends Controller
{
    /**
     * @var CategorieRepository
     */
    private $CategorieRepository;

    /**
     * GroupController constructor.
     * @param CategorieRepository $CategorieRepository
     */
    public function __construct(CategorieRepository $CategorieRepository)
    {
        $this->CategorieRepository = $CategorieRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->CategorieRepository->index(request()->all());

        return response()->json($data,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = validator($request->all(),[
            'nom' => 'required'
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        $group = $this->CategorieRepository->create($request->all());

        return $this->sendResponseOk($group,"Created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->CategorieRepository->find($id);

        if(!$category) return $this->sendResponseNotFound();

        return $this->sendResponseOk($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validate = validator($request->all(),[
            'nom' => 'required'
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        $updated = $this->CategorieRepository->update($id,$request->all());

        if(!$updated) return $this->sendResponseBadRequest("Failed update.");

        return $this->sendResponseUpdated();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->CategorieRepository->delete($id);
        } catch (\Exception $e) {
            return $this->sendResponseBadRequest("Failed to delete");
        }

        return $this->sendResponseDeleted();
    }
}
