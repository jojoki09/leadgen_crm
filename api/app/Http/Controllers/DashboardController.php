<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function getProjectBycommercial(Request $request)
    {

//        select projects.nom, count(*) as 'NvPropsect', concat(contacts.prenom, " ", contacts.nom) as 'commercial' from partenaires
//            join contacts on contacts.partenaire_id = partenaires.part_id
//            join vente_devis on vente_devis.partenaire_id = partenaires.part_id
//            join projects on projects.id = vente_devis.projet_id
//            where YEAR(partenaires.created_at)=2019
//            group by partenaires.user_id

//        sum(i.instalation) as instalation,sum(i.desinstallation) as desinstallation,sum(i.reconfiguration) as reconfiguration,sum(i.verification) as verification,i.first_name
        $date_year = $request->get('year');
        $date_month = $request->get('month');
        $projet = $request->get('project');

        Log::info($date_year .' '.$date_month);
        if(!empty($date_year)  && !empty($date_month)){
            $sql = "select projects.nom, partenaires.user_id, COUNT(*) as 'NbPropsect', concat(users.first_name, \" \", users.last_name) as 'commercial' from partenaires
           join contacts on contacts.partenaire_id = partenaires.part_id
            join vente_devis on vente_devis.partenaire_id = partenaires.part_id
            join users on users.id = partenaires.user_id
            join projects on projects.id = vente_devis.projet_id
            where YEAR(partenaires.created_at)=".$date_year." && MONTH(partenaires.created_at)=".$date_month." && projects.id =".$projet."
            group by partenaires.user_id ,projects.id
        ";
        }


        return \DB::select(\DB::raw($sql));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
