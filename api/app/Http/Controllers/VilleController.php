<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\ville;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VilleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(){

    $chunks= ville::all();

    return response()->json($chunks,200);
}
    public function getCityById($id)
    {

        $chunks= City::where('country_id','=',$id)->get();

        Log::info("it's the city seklected bvy countries ".$chunks );

        return response()->json($chunks,200);
    }

    public function getCountry()
    {
            $data= Country::all();

            Log::info($data);

        return response()->json($data,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $ville
     * @return \Illuminate\Http\Response
     */
    public function show(City $ville)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $ville
     * @return \Illuminate\Http\Response
     */
    public function edit(City $ville)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $ville
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $ville)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $ville
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $ville)
    {
        //
    }
}
