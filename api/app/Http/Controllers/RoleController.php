<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class RoleController extends Controller
{
    public function index()
    {

        $sql = "
            select ur.*, count(u.id) as 'total_users' from user_roles ur left join users u on  ur.id = u.role_id 
            where ur.deleted_at is null and u.deleted_at is null
            group by ur.id
            order by ur.created_at desc
        ";

        return \DB::select(\DB::raw($sql));
    }


    public function store(Request $request)
    {

        $rules = [
            'role_name' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first() ]);
        }

        $role = Role::create($request->all());

        

        return response()->json(["ok"=> 1, "feedback"=> "we generate a new resource for you", "role_id" => $role->id]);

    }


    public function  offer_rights(Request $request) {
        $role_id = $request->get("id");
        $role =  Role::find($role_id);
        if ( !$role ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        // remove the old rights
        $this->removeRoleRights($role_id);
        
        $offered_rights = [];

        if($request->has('rights')) { 
            $rights = $request->get('rights');
            for ($i=0; $i < count($rights) ; $i++) { 
                array_push($offered_rights, ["role_id" => $role_id, "right_id" => $rights[$i]]);
            }
        }

        DB::table('role_rights')->insert($offered_rights);
        
        return ["ok" => 1, "feedback" => "rights has been updated"];
    }

    public function removeRoleRights($role_id) {
        DB::table('role_rights')->where('role_id', '=', $role_id)->delete();
    }

    public function getRoleRights($role_id) {
        $rights = DB::table('rights')
            ->join('role_rights', 'rights.id', '=', 'role_rights.right_id')
            ->where('role_rights.role_id', '=', $role_id)
            ->select('rights.*')
            ->get();

        return $rights;
    }



    public function update(Request $request)
    {
        Role::where("id", $request->get("id"))
            ->update($request->except(["id"]));

        return response()->json(["ok"=> 1, "feedback"=> "go to main page to see changes"]);
    }


    public function destroy($role_id)
    {
        $role = Role::find($role_id);
        if(!$role) return;

        if($role->role_name == "super admin" || $role->role_name == "root" || $role->id == 1)
            return ["ok" => 0, "error" => "this role can't be deleted"];

        $role->delete();
        return ["ok" => 1, "feedback" => "the resource softly deleted, check the trash"];
    }
}
