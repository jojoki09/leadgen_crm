<?php

namespace App\Http\Controllers;

use App\Components\partners\Models\partenaire;
use App\Components\partners\Repositories\PartenaireRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PartenaireController extends Controller
{
    /**
     * @var
     */
    private $PartenaireRepository;
    /**
     * PartenaireRepository constructor.
     * @param PartenaireRepository  $PartenaireRepository
     */
    public function __construct(PartenaireRepository $PartenaireRepository)
    {
        $this->PartenaireRepository = $PartenaireRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->PartenaireRepository->index(request()->all());



        return response()->json($data,200);

//        return $this->sendResponseOk($data,"list  ok.");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = validator($request->all(),[
            'type' => 'required',
            'nom' => 'required',
            'prenom' => 'required',
            'mobile' => 'required',
            'projet_id' => 'required',
            'source' => 'required',
        ]);
        Log::info($request->all());

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        /** @var Partenaire $partner */
        $partner = $this->PartenaireRepository->create($request->all());

        if(!$partner) return $this->sendResponseBadRequest("Failed create.");
        $partner->contact()->create($request->all());

        $partner->adresse()->create($request->all());
        $vente_devis = $partner->vente_devis()->create($request->all());
        $requestData = $request->all();
        $requestData['type'] = 0;
        $vente_devis->vente_devis_detail()->create($requestData);

        return response()->json(["ok"=> 1,"data" =>$vente_devis],201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $part = $this->PartenaireRepository->find($id,['vente_devis','vente_devis_detail','contact','adresse','projcets']);
        Log::info("the id ".$id);

        Log::info($part);

        if(!$part) return response()->json($part,404);;

        return response()->json($part,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function edit(partenaire $partenaire)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Log::info($request->all());
        Log::info($id);

        $updated = $this->PartenaireRepository->update($id,$request->all());


        DB::table('contacts')->where("partenaire_id", $id)
        ->update($request->all());

        if(!$updated) return $this->sendResponseBadRequest("Failed update");


        return response()->json($updated,201);



        /*$user = $this->userRepository->find($id);

        $groupIds = [];

        if($groups = $request->get('groups',[]))
        {
            foreach ($groups as $groupId => $shouldAttach)
            {
                if($shouldAttach) $groupIds[] = $groupId;
            }
        }

        $user->groups()->sync($groupIds);

        return $this->sendResponseUpdated();*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(partenaire $partenaire)
    {
        //
    }
}
