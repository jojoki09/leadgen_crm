<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserJob;
use Illuminate\Support\Facades\Log;
use Validator;

class UserJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user_jobs = UserJob::all();
        return $user_jobs;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'job_title' => 'required|string',
        ];

        $validator = Validator::make($request->only("job_title"), $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first() ]);
        }

        UserJob::create($request->only("job_title"));

        return response()->json(["ok"=> 1, "feedback"=> "we generate a new resource for you" ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($job_id)
    {
        UserJob::where('id', '=', $job_id)->delete();
        
        return ["ok" => 1, "feedback" => "the resource permanently deleted"];
    }

    public function update(Request $request)
    {
        UserJob::where("id", $request->get("id"))
            ->update($request->except(["id"]));

        return response()->json(["ok"=> 1, "feedback"=> "go to main page to see changes"]);
    }

    

    public function getUsers($job_id) {
        $job =  UserJob::find($job_id);
        if ( !$job ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        return $job->users()->get();
        
    }

    public static  function getJob($job_title) {
        return UserJob::Where("job_title", "=", $job_title)->where("deleted_at", "=", null)->first();
    }

}
