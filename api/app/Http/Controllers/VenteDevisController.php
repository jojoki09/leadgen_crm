<?php

namespace App\Http\Controllers;

use App\Components\Devis\Models\vente_devis;
use App\Components\Devis\Repositories\VenteDevisRepository;
use Illuminate\Http\Request;

class VenteDevisController
{

    /**
     * @var VenteDevisRepository
     */
    private $VenteDevisRepository;

    /**
     * UserController constructor.
     * @param VenteDevisRepository $VenteDevisRepository
     */
    public function __construct(VenteDevisRepository $VenteDevisRepository)
    {
        $this->VenteDevisRepository = $VenteDevisRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->VenteDevisRepository->index(request()->all());

        return $this->sendResponseOk($data,"list ok.");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = validator($request->all(),[
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required',
            'projet_id' => 'required',
            'statut' => 'required',
            'source' => 'required',
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        /** @var User $user */
        $partner = $this->userRepository->create($request->all());

        if(!$user) return $this->sendResponseBadRequest("Failed create.");

        // attach to group
        if($groups = $request->get('groups',[]))
        {
            foreach ($groups as $groupId => $shouldAttach)
            {
                if($shouldAttach) $user->groups()->attach($groupId);
            }
        }

        return $this->sendResponseCreated($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vente_devis  $vente_devis
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info($id);
        $devis = $this->VenteDevisRepository->find($id,['vente_devis_detail','partenaire','projet','adresse','contact','source']);
        //$devis->orderBy($params['order_by'] ?? 'oppppid', $params['order_sort'] ?? 'desc');
        if(!$devis) return $this->sendResponseNotFound();
        return $this->sendResponseOk($devis);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vente_devis  $vente_devis
     * @return \Illuminate\Http\Response
     */
    public function edit(vente_devis $vente_devis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vente_devis  $vente_devis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vente_devis $vente_devis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vente_devis  $vente_devis
     * @return \Illuminate\Http\Response
     */
    public function destroy(vente_devis $vente_devis)
    {
        //
    }
}
