<?php

namespace App\Http\Controllers;

use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ModuleController extends Controller
{

    public function rights($module_id)
    {   
        return Module::findOrFail($module_id)->rights()->get();
    }


    public function get($module_id)
    {
        return Module::findOrFail($module_id);
    }

    public function index()
    {
        return Module::all();
    }


    public function store(Request $request)
    {

        $rules = [
            'module_name' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first() ]);
        }

        Module::create($request->all());

        return response()->json(["ok"=> 1, "feedback"=> "we generate a new resource for you" ]);

    }



    public function update(Request $request)
    {
        Module::where("id", $request->get("id"))
            ->update($request->except(["id"]));

        return response()->json(["ok"=> 1, "feedback"=> "go to main page to see changes"]);
    }


    public function destroy($module_id)
    {
        Module::findOrFail($module_id)->delete();
        return ["ok" => 1, "feedback" => "the resource softly deleted, check the trash"];
    }
}
