<?php

namespace App\Http\Controllers;

use App\Tiers;
use App\Type_interne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TiersController extends Controller
{
    /////////////////////// Clients //////////////////////////
    public function getClients()
    {
        $clients = DB::table('tiers')
            ->where('client','=','1')
            ->where('tiers.deleted_at','=',Null)
            ->leftJoin('users','users.id','=','tiers.user_id')
            ->select('tiers.*','users.first_name','users.last_name')
            ->orderBy('tiers.id','desc')
            ->get();
        return response()->json($clients,201);
    }

    public function getTypesGenerale()
    {
        $type_generale = DB::table('type_generale')
            ->get();
        return response()->json($type_generale,201);
    }

    public function getTypesInterne()
    {
        $type_interne = DB::table('type_interne')
            ->where('deleted_at','=',Null)
            ->get();
        return response()->json($type_interne,201);
    }

    public function storeTypesInterne(Request $request)
    {
       if($type_interne = Type_interne::create($request->all()))
       {
           return response()->json($type_interne, 201);
       }
       else
           return response()->json(['data'=>'error in the insert'], 500);
    }

    public function deleteTypesInterne($id)
    {
        if($typeInterne = Type_interne::find($id))
        {
            $typeInterne->delete();
            return response()->json($typeInterne, 201);
        }else
            return response()->json(['data'=>'error in the delete'], 500);
    }

    public function getUsers()
    {
        $users = DB::table('users')
            ->select('id','first_name','last_name')
            ->get();
        return response()->json($users,201);
    }

  
    /////////////////////// Fournisseurs //////////////////////////

    public function getFournisseurs()
    {
        $fournisseurs = DB::table('tiers')
        ->where('fournisseur','=','1')
        ->where('tiers.deleted_at','=',Null)
        ->leftJoin('users','users.id','=','tiers.user_id')
        ->select('tiers.*','users.first_name','users.last_name')
        ->orderBy('tiers.id','desc')
        ->get();
        return response()->json($fournisseurs,201);
    }

    public function getTierById($id)
    {
        $tier = DB::table('tiers')
            ->where('id','=',$id)
            ->select('id','nom')
            ->first();
            return response()->json($tier,201);
    }

    public function getClientFournisseur()
    {
        $cf = DB::table('tiers')
            ->where('client','=','1')
            ->where('fournisseur','=','1')
            ->where('tiers.deleted_at','=',Null)
            ->leftJoin('users','users.id','=','tiers.user_id')
            ->select('tiers.*','users.first_name','users.last_name')
            ->orderBy('tiers.id','desc')
            ->get();
        return response()->json($cf,201);
    }

    public function getTiersByID($id)
    {
        $CF = DB::table('tiers')
            ->where('tiers.id','=',$id)
            ->leftJoin('users','users.id','=','tiers.user_id')
            ->select('tiers.*','users.first_name','users.last_name')
            ->first();
        return response()->json($CF,201);
    }

    public function store(Request $request)
    {
        if($tier = tiers::create($request->all()))
        {
            return response()->json($tier, 201);
        }
        else
            return response()->json(['data'=>'error in the insert'], 500);
    }

    public function update(Request $request)
    {
        $tierUpdate = tiers::findOrFail($request->input('id'));
        if($tier = $tierUpdate->update($request->all()))
        {
            return response()->json($tier, 201);
        }
        else
            return response()->json(['data'=>'error in the update'], 500);
    }

    public function delete($id)
    {
        if($tier = tiers::find($id))
        {
            $tier->delete();
            return response()->json($tier, 201);
        }else
            return response()->json(['data'=>'error in the delete'], 500);
    }

     

    public static  function getClientById($user_id) {
        return \DB::table("tiers")->where("id", "=", $user_id)->where("deleted_at", "=", null)->first();
    }
    
    public function getIdTiers()
    {
        if($id = DB::table('tiers')->orderBy('id','desc')->select('id')->pluck('id')->first())
            return response()->json($id, 201);
        else
            return response()->json(['data'=>'error in the get'], 500);
    }



    public function getProvider()
    {
        $fournisseurs = DB::table('tiers')
            ->where('fournisseur','=','1')
            ->where('tiers.deleted_at','=',Null)
            ->select('tiers.id','tiers.nom')
            ->get();

        return response()->json($fournisseurs ,201);


    }

}


