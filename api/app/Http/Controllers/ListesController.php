<?php

namespace App\Http\Controllers;

use App\Components\Listes\Models\listes;
use Illuminate\Http\Request;
use App\Components\Listes\Repositories\ListesRepository;
use Illuminate\Support\Facades\Log;

class ListesController extends Controller
{
    /**
     * @var ListesRepository
     */
    private $listesRepository;

    /**
     * ListesController constructor.
     * @param ListesRepository $listesRepository
     */
    public function __construct(ListesRepository $listesRepository)
    {
        $this->ListesRepository = $listesRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->ListesRepository->index(request()->all());
        Log::info($data);

        return response()->json($data,200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\listes  $listes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listes = $this->ListesRepository->find($id);
        if(!$listes) return $this->sendResponseNotFound();
        return response()->json($listes,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\listes  $listes
     * @return \Illuminate\Http\Response
     */
    public function edit(listes $listes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\listes  $listes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, listes $listes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\listes  $listes
     * @return \Illuminate\Http\Response
     */
    public function destroy(listes $listes)
    {
        //
    }
}