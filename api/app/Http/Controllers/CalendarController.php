<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $calendarsData = DB::table('calendars')
            ->Join('projects','projects.id','=','calendars.project_id')
            ->Join('partenaires','partenaires.part_id','=','calendars.part_id')
            ->select('calendars.*','projects.nom as projectname','partenaires.nom as partname' )
            ->orderBy('calendars.date_planif','desc')
            ->get();
        return response()->json($calendarsData,200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($calendar = Calendar::create($request->all()))
        {
            return response()->json($calendar, 201);
        }else
            return response()->json(['data'=>'error in the insert'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function show(Calendar $calendar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function edit(Calendar $calendar)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info("ici ici");
        Log::info($request->all());
        Log::info($id);
        Log::info("here here");


        DB::table('calendars')->where("id", $id)
            ->update($request->all());


        return response()->json(['data'=> 'updated'],201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id)
    {
        Log::info($event_id);
        if($calendar = Calendar::find($event_id))
        {
            $calendar->delete();
            return response()->json($calendar, 201);
        }else
            return response()->json(['data'=>'error in the delete'], 500);
    }
}
