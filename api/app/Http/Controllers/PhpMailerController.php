<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Validator;
use DB;
use Log;

class PhpMailerController extends Controller
{
    public $table = "**";

    const ALLOWED_EXTENTIONS = ["jpg", "jpeg", "gif", "png", "pptx", "xlsx", "docx", "doc", "pdf", "mp3", "mp4"];
    const MAX_SIZE = 875000;
    const DEFAULT_SUBJECT = "No subject given";
    const DEFAULT_BODY = "i\'m the body nice to be formated html";

    private $host;
    private $username;
    private $password;
    private $port;
    private $encryption;

    public function sendEmail(Request $request) {

        $envelope = $request->only("from", "to", "cc", "reply_to", "subject", "body");

        if($request->hasFile("attachements")) {


            $response = $this->readAttachements($request->file("attachements"));

            if(!$response["ok"]) {
                return ["ok" => 0, "error" => $response["errors"]];
            }

            $envelope["attachements"] = $response["attachements"];
        }

        // return $envelope;

        $response = $this->send($envelope);

        if($response["ok"]) {
            return $response;
        } else {
            return ["ok" => 0, $response["error"]];
        }


    }

    public function readAttachements($files) {

        // $content = file_get_contents($file);
        // $encoded_content = base64_encode($content);
        // $well_formated_content = chunk_split($encoded_content);

        $attachements = [];
        $errors = [];

        foreach ($files  as $file) {

            $originalName = $file->getClientOriginalName();
            $size = $file->getClientSize();

            $allowed_extension = $this->allowed_extention($originalName);
            $allowed_size = $this->allowed_size($size);

            if(!$file->isValid())
                $errors[] = "the file is present is not valid";

            if(!$allowed_extension)
                $errors[] =  "[" . $originalName . "]" . " l'extension de fichier donnée n'est pas autorisée";
            if(!$allowed_size)
                $errors[] =  "[" . $originalName . "]" . " la taille du fichier est trop grande";

            if(count($errors) > 0)
                break;

            $attachements[] = [
                "realPath" => $file->getRealPath(),
                "clientOriginalName" => $file->getClientOriginalName()
            ];

        }

        if(count($errors) > 0)
            return  ["ok" => 0, "errors" => $errors];
        else
            return ["ok" => 1, "attachements" => $attachements];


    }

    public function accepted_envelope($envelope) {
        return

        isset($envelope["from"]["email"]) && !is_null($envelope["from"]["email"]) && !empty($envelope["from"]["email"]) &&

        isset($envelope["to"]["email"]) && !is_null($envelope["to"]["email"]) && !empty($envelope["to"]["email"]) &&

        isset($envelope["subject"]) && !is_null($envelope["subject"]) && !empty($envelope["subject"]) &&
        isset($envelope["body"]) && !is_null($envelope["body"]) && !empty($envelope["body"]);
    }

    public function send($envelope) {

        if($this->accepted_envelope($envelope)) {

            $mail = new PHPMailer(true);                                // Passing `true` enables exceptions

            try {
                $mail->SMTPDebug = 0;                                   // Enable verbose debug output
                $mail->isSMTP();                                        // Set mailer to u-se SMTP
                $mail->Host = $this->host; 				                // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                                 // Enable SMTP authentication
                $mail->Username = $this->username;                      // SMTP username
                $mail->Password = $this->password;                      // SMTP password
                $mail->SMTPSecure = "tls";                              // Enable TLS encryption, `ssl` also accepted
                $mail->Port = $this->port;                              // TCP port to connect to

                $mail->isHTML(true); 							        // Set email format to HTML


                if(isset($envelope["from"]["name"])) {
                    $mail->setFrom($envelope["from"]["email"], $envelope["from"]["name"]);
                } else
                    $mail->setFrom($envelope["from"]["email"]);

                if(isset($envelope["to"]["name"])) {
                    $mail->addAddress($envelope["to"]["email"], $envelope["to"]["name"]);
                } else
                    $mail->addAddress($envelope["to"]["email"]);


                if(isset($envelope["cc"]))
                    $mail->addCc($envelope["cc"]);

                if(isset($envelope["reply_to"]))
                    $mail->addReplyTo($envelope["reply_to"]);

                $mail->Subject = substr(strip_tags($envelope["subject"]), 0, 255);
                $mail->Body = $envelope["body"];


                if(isset($envelope["attachements"]) && count($envelope["attachements"]) > 0) {

                    foreach ($envelope["attachements"] as $attachement) {
                        if(isset($attachement["clientOriginalName"]))
                            $mail->addAttachment(
                                $attachement["realPath"],
                                $attachement["clientOriginalName"]
                            );      // Optional name
                        else
                            $mail->addAttachment($attachement["realPath"]);
                    }


                }


                $mail->send();

            return ["ok" => 1];
            } catch (Exception $e) {
                return ["ok" => "0", "error" => $e->getMessage()];
            }
        } else {
            return ["ok" => "0", "error" => "insuffisant requirements"];
        }
    }






    public function __construct() {

        $params = $this->mail_server_params();
        $this->host = $params["host"];
        $this->username = $params["username"];
        $this->password = $params["password"];
        $this->port = $params["port"];
    }


    private function mail_server_params() {
        $oc = new OptionController();
        
        return [
            "host" => $oc->get("mailserver_url"),
            "username" => $oc->get("mailserver_login"),
            "password" => $oc->get("mailserver_pass"),
            "port" => $oc->get("mailserver_port"),
        ];
    }


    private function allowed_extention($path) {
        $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
        return in_array($ext, self::ALLOWED_EXTENTIONS);
    }

    private function allowed_size($size) {
        return (double)$size <=  (float)self::MAX_SIZE;
    }
}
