<?php

namespace App\Http\Controllers;

use App\Right;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class RightController extends Controller
{
    public function index()
    {
        return DB::table("rights")->get();
    }


    public function store(Request $request)
    {

        $rules = [
            'right_name' => 'required',
            'module_id' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first() ]);
        }

        Right::create($request->all());

        return response()->json(["ok"=> 1, "feedback"=> "we generate a new resource for you" ]);

    }



    public function update(Request $request)
    {
        Right::where("id", $request->get("id"))
            ->update($request->except(["id"]));

        return response()->json(["ok"=> 1, "feedback"=> "go to main page to see changes"]);
    }


    public function destroy($right_id)
    {
        DB::table('rights')->where('id', '=', $right_id)->delete();
        return ["ok" => 1, "feedback" => "the resource softly deleted, check the trash"];
    }
}
