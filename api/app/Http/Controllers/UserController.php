<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use DB;
use Symfony\Component\HttpFoundation\RequestStack;




class UserController extends Controller
{

    public function get($user_id)
    {
        return User::findOrFail($user_id);
    }

    public function index()
    {
        $sql = "
            select u.*, uj.job_title as 'job', concat(if(isnull(u.first_name), \"\", u.first_name), \" \", if(isnull(u.last_name), \"\", u.last_name)) as 'full_name' from users u left join user_jobs uj on u.job_id = uj.id
            where u.deleted_at is null
            order by u.created_at desc
        ";

        return \DB::select(\DB::raw($sql));
    }

    public function  offer_rights(Request $request) {
        $user_id = $request->get("id");
        $user =  User::find($user_id);
        if ( !$user ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        // remove the old rights
        $this->removeUserDirectRights($user_id);
        
        $offered_rights = [];

        if($request->has('rights')) { 
            $rights = $request->get('rights');
            for ($i=0; $i < count($rights) ; $i++) { 
                array_push($offered_rights, ["user_id" => $user_id, "right_id" => $rights[$i]]);
            }
        }

        DB::table('user_rights')->insert($offered_rights);
        
        return ["ok" => 1, "feedback" => "rights has been updated"];
    }

    public function removeUserDirectRights($user_id) {
        DB::table('user_rights')->where('user_id', '=', $user_id)->delete();
    }

    public function getUserRights($user_id) {

        /* select r.* from rights r 
        join user_rights ur on r.id = ur.right_id
        where ur.user_id = 1; */

        $rights = DB::table('rights')
            ->join('user_rights', function ($join) {
                $join->on('rights.id', '=', 'user_rights.right_id');
            })
            ->where('user_rights.user_id', '=', $user_id)
            ->select('rights.*')
            ->get();

        return $rights;

    }
    public function getUserDeptRights($user_id) {
        /* select r.* from rights r 
        join departement_rights dr on r.id = dr.right_id
        join user_departement ud on ud.id = dr.departement_id
        join users u on u.departement_id = ud.id
        where u.id = 1; */

        $rights = DB::table('rights')
            ->join('departement_rights', 'rights.id', '=', 'departement_rights.right_id')
            ->join('user_departement', 'user_departement.id', '=', 'departement_rights.departement_id')
            ->join('users', 'users.departement_id', '=', 'user_departement.id')
            ->where('users.id', '=', $user_id)
            ->select('rights.*')
            ->get();

        return $rights;

    }
    public function getUserRoleRights($user_id) {
        /* select r.* from rights r 
        join role_rights rr on r.id = rr.right_id
        join user_roles ur on ur.id = rr.role_id
        join users u on u.role_id = ur.id
        where u.id = 1 */

        $rights = DB::table('rights')
            ->join('role_rights', 'rights.id', '=', 'role_rights.right_id')
            ->join('user_roles', 'user_roles.id', '=', 'role_rights.role_id')
            ->join('users', 'users.role_id', '=', 'user_roles.id')
            ->where('users.id', '=', $user_id)
            ->select('rights.*')
            ->get();

        return $rights;
    }


    public function update(Request $request)
    {

        $old_user =  User::find($request->get("id"));
        $new_user = $request->except("id");

        if ( !$old_user ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        $rules = [
            'email' => 'email',
            'password' => 'min:6',
            'login' => 'string|max:50',
            'photo' => 'mimes:jpeg,bmp,png,jpeg,gif|max:20481',
            'rights_depending' => Rule::in(['0', '1', '2']),
        ];

        $validator = Validator::make($new_user, $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first()]);
        }


        #-----------------


        $old_pwd = $old_user["password"];

        if($request->has("password")) {
            $new_pwd = $request->get("password");
            if(!is_null($new_pwd) && !empty($new_pwd)) {
                $new_user["password"] = Hash::make($new_pwd);
            }
        }


        #-----------------

        $old_photo = $old_user["photo"];

        if($request->hasFile("photo")) {


            Storage::delete('images/avatars/' . $old_photo);

            $file = $request->file('photo');

            #-------------------

            if(isset($new_user["login"]))
                $fake_name = md5($new_user["login"]) . '_' . $file->getClientOriginalName();
            else
                $fake_name = md5($old_user["login"]) . '_' . $file->getClientOriginalName();

            #-------------------


            if($request->hasFile('photo')) {
                $path = Storage::putFileAs(
                    'images/avatars', $request->file('photo'), $fake_name
                );
            }

            $new_user["photo"] = $fake_name;
        }


        #-----------------




        User::where("id", $request->get("id"))
            ->update($new_user);

        return response()->json(["ok"=> 1, "feedback"=> "go to main page to see changes"]);

    }

    public static function getAvatar($user_id)
    {


        $user = User::find($user_id);
        $photo = $user->photo;

        if($user->photo == "null" or $user->photo == "")
            return ["ok" => 0, 
                    "error" => "no avatar"];

        
        if($photo) {
            $slices = explode('.', $photo);
            $ext = end($slices);
            $content = Storage::get('images/avatars/' . $photo);
            $encoded_content = base64_encode($content);
            $imageData = 'data:' . 'image/' . $ext  . ';base64,' . $encoded_content;
            return ["ok"=> 1, "imageData" => $imageData];
        } else {
            return ["ok"=> 0, "error" => "not found"];
        }
        
    }

    public static function  deleteAvatar($user_id) {

        $user =  User::find($user_id);

        if ( !$user ) return response()->json(["ok"=> 0, "error" => "not found"], 404);        

        $photo = $user->photo;
        if($photo) {

            // set null to photo in database
            User::where("id", $user_id)
            ->update(["photo" => "null"]);

            // remove the photo from the local storage
            Storage::delete('images/avatars/' . $photo);

            // return success feedback
            return ["ok"=> 1, "feedback"=> "the avatar remove from storage"];
        } else {
            return ["ok"=> 0, "feedback"=> "the user dosn't have profile image"];
        }

    }

    public function destroy($user_id)
    {

        $user = User::find($user_id);
        if(!$user) return;

        if($user->login == "root" || $user->id == 1)
            return ["ok" => 0, "error" => "this user can't be deleted"];

        $user->delete();
        return ["ok" => 1, "feedback" => "the resource softly deleted, check the trash"];

    }

    public function getJob($user_id) {
        $user =  User::find($user_id);
        if ( !$user ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        return $user->job()->get();
        
    }

    
    public function editPassword(Request $request) {
        /* user_id, current_password, new_password */

        $validator = Validator::make($request->only(["current_password", "new_password"]), [
            'user_id' => 'number',
            'current_password' => 'string|min:6',
            'new_password' => 'string|min:6',
        ]);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first()]);
        }


        $user_id = $request->get("user_id");
        $user =  User::find($user_id);
        if (!$user) return response()->json(["ok"=> 0, "error" => "not found"], 404);


        $old_password = $request->get("current_password");
        $new_password = $request->get("new_password");

        /* check the password if the same */

        /* return ["old_password" => $old_password, "user->password" => $user->password]; */
        
        $response = password_verify($old_password, $user->password);

        if(!$response)
            return ["ok"=> 0, "error" => "les informations d'identification invalides"];

        /* update the password  */

        $hashed_password = Hash::make($new_password);

        User::where("id", $user_id)
            ->update(["password" => $hashed_password]);

        return response()->json(["ok"=> 1, "feedback"=> "garder le nouveau mot de passe à l'esprit"]);

    }


    public static  function getUserById($user_id) {
        return \DB::table("users")->where("id", "=", $user_id)->where("deleted_at", "=", null)->first();
    }


    public static function isMySuperior($child_id, $parent_id) {
        if(!$child_id || !$parent_id) return;

        $child = self::getUserById($child_id);
        $parent = self::getUserById($parent_id);

        if(!$child || !$parent) return;

        return $child->superior_id == $parent->id;
    }


    public static function getUserRoleById($user_id) {
        if(!$user_id) return;

        $user = User::find($user_id);

        if(!$user) return;

        return $user->role()->first();
    }

    public static function current() {
        return \JWTAuth::parseToken()->authenticate();
    }


    public static function root() {
        return User::Where("id", "=", 1)->where("role_id", "=", 1)->first();
    }


    public static function getUserChildrens($user_id, $job_title = "all") {
        $root = self::root();
        if(!$root) return;

        if($root->id == $user_id) {
            $childrens = User::all();
        } else {
            $childrens = User::Where("superior_id", "=", $user_id)->where("deleted_at", "=", null)->get();
        }



        $filtredChildrens = [];

        switch ($job_title) {
            case "installateur":
                $filtredChildrens = self::filterUsersByJob($childrens, "installateur");
                return $filtredChildrens != null && count($filtredChildrens) > 0 ? $filtredChildrens : [];
            default:
                return $childrens;
        }
    }

    public static function getUserSuperior($user_id) {
        $user = User::find($user_id);
        if(!$user) return;

        $superior_id = $user->superior_id;
        return User::Where("id", "=", $superior_id)->where("deleted_at", "=", null)->get();
    }

    public static function filterUsersByJob($users, $job_title) {
        $job = UserJobController::getJob($job_title);
        if(!$job) return;

        $installers = [];

        foreach ($users as $obj => $user) {
            if($user->job_id == $job->id) {
                array_push($installers, $user);
            }
        }

        return $installers;
    }



    
}
