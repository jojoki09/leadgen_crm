<?php

namespace App\Http\Controllers;

use App\societe_contact;
use Illuminate\Http\Request;

class SocieteContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\societe_contact  $societe_contact
     * @return \Illuminate\Http\Response
     */
    public function show(societe_contact $societe_contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\societe_contact  $societe_contact
     * @return \Illuminate\Http\Response
     */
    public function edit(societe_contact $societe_contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\societe_contact  $societe_contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, societe_contact $societe_contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\societe_contact  $societe_contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(societe_contact $societe_contact)
    {
        //
    }
}
