<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\PhpMailerController;
use Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Hash;
use DB;
use Mockery\Undefined;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        $user =  $request->all();

        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'login' => 'required|string|max:50|unique:users',
            'photo' => 'mimes:jpeg,bmp,png,jpeg,gif|max:20481',
            'rights_depending' => Rule::in(['0', '1', '2']),
        ];

        $validator = Validator::make($user, $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first()]);
        }




        if($request->hasFile('photo')) {
            $file = $request->file('photo');
            $fake_name = md5($user["login"]) . '_' . $file->getClientOriginalName();

            $path = Storage::putFileAs(
                'images/avatars', $request->file('photo'), $fake_name
            );

            $user["photo"] = $fake_name;
        }

        $user["password"] = Hash::make($user["password"]);

        $user = User::create($user);
        $user_id = $user->id;

        /**
         * EMAIL VERIFICATION
         */

        $response = $this->sendVerificationEmail($user_id);

        /**
         * generate a token for the user is going to register
         */

        $token = JWTAuth::fromUser($user);

        /**
         * define rights based on static defination
         */

        $offered_rights = [];

        if($request->has('rights')) {
            $rights = json_decode($request->get('rights'));
            for ($i=0; $i < count($rights) ; $i++) {
                array_push($offered_rights, ["user_id" => $user_id, "right_id" => $rights[$i]]);
            }
        }

        DB::table('user_rights')->insert($offered_rights);


        return response()->json(["ok"=> 1, "token" => $token, "feedback"=> "we generate a new resource for you" ]);
    }


    public function sendVerificationEmail($user_id)
    {
        $user = User::find($user_id);
        if(!$user) {
            return ["ok" => 0, "error" => "no found"];
        }

        /* check if the user is verified */
        if($this->isVerified($user->id)) {
            return ["ok" => 0, "error" => "verified account!!"];
        }

        /* don't repeat yourself */


        /* generate token */
        $token = str_random(70);

        DB::table('email_verfications')->insert([
            'email' => $user->email,
            'token' => $token,
            /* 'created_at' => date("Y-m-d h:i:s") */
        ]);


        $option = new OptionController();
        $from = $option->get("verification_emails_sender");
        /* $webui_origin = $option->get("webui_origin"); */

        $webui_origin = env("CORS_ACTIVE_ORIGIN",  $option->get("webui_origin"));


        $phpMailer = new PhpMailerController();

        $body_template = "<div ><p>Veuillez vérifier votre adresse e-mail en cliquant sur le lien suivant:</p><p><a href=\"%ORIGIN%/auth/request/verify-email?ticket=%TOKEN%\" target=\"_blank\">Confirmer mon compte</a></p><p>Si vous rencontrez des problèmes avec votre compte, n'hésitez pas à nous contacter en répondant à ce courrier.</p><br>Merci <br><strong>Opentech</strong> <br><br><hr style=\"border:2px solid #eaeef3;border-bottom:0;margin:20px 0\"> <p style=\"text-align:center;color:#a9b3bc\"> If you did not make this request, please contact us by replying to this mail. </p></div>";

        $body = str_replace("%ORIGIN%", $webui_origin, str_replace("%TOKEN%", $token, $body_template));


        $envelope = [
            "from" => [
                "email" => $from,
            ],
            "to" => [
                "email" => $user["email"],
            ],
            "subject" => "Confirmez votre e-mail",
            "body" => $body
        ];

        return $phpMailer->send($envelope);

    }

    public function verify_email(Request $request) {
        if(!$request->has("ticket"))
            return ["ok" => 0,
            "error" => "no provided date"];

        $ticket = $request->get("ticket");

        $response = DB::table("email_verfications")->where("token", "=", $ticket)->first();

        if(!$response)
            return ["ok" => 0,
                    "error" => "bad request"];

        $verifcation_date = $response->verifcation_date;
        if($verifcation_date)
        return ["ok" => 0,
                "error" => "verified email at " . $verifcation_date];

        $response->token = $ticket;
        $response->verifcation_date = date("Y-m-d h:i:s");

        $response_array = json_decode(json_encode($response), true);

        DB::table("email_verfications")->where("token", "=", $ticket)
            ->update($response_array);

        User::where("email", $response->email)->update(["is_verified" => 1]);

        return ["ok" => 1, "Done"];
    }

    public function sendPasswordResetToken(Request $request)
    {
        $user = User::where ('email', $request->email)->first();
        if ( !$user ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        $token = str_random(60);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => date("Y-m-d h:i:s")
        ]);



        /**
         * Send email to the email above with a link to your password reset
         * something like url('password-reset/' . $token)
         * Sending email varies according to your Laravel version. Very easy to implement
         */
    }

    public function authenticate(Request $request)
    {
        if(!$request->has("email") || !$request->has("email"))
            return ["ok"=> 0, "error" => "Informations insuffisant"];

        $credentials = $request->only('email', 'password');

        $user = User::where("email", "=", $credentials["email"])->orWhere("login", "=", $credentials["email"])->first();

        if(!$user)
            return ["ok"=> 0, "error" => "les informations d'identification invalides"];

        $response = Hash::check($credentials["password"], $user->password);

        if(!$response)
            return ["ok"=> 0, "error" => "les informations d'identification invalides"];


        $user_id = $user->id;

        if(!$this->isVerified($user_id))
            return ["ok"=> 0, "error" => "vérifier votre boîte de réception pour confirmer la propriété de l'email"];

        if($this->disabled($user_id))
            return ["ok"=> 0, "error" => "le compte est désactivé maintenant"];

        try {
            /* if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['ok' => 0, 'error' => "les informations d'identification invalides"]);
            } */
            Log::info("hello");
            $token = JWTAuth::fromUser($user);
            Log::info($token);
        } catch (JWTException $e) {
            return response()->json(['ok' => 0, 'error' => "Impossible de générer Impossible de créer un clé d'authentification"]);
        }

        /* UPDATE LAST LOGIN [last_login] */
        User::where("id", $user_id)
            ->update(["last_login" => now()]);


        return ["ok" => 1, "token" => $token, "user_id" => $user_id ];
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function reset_password(Request $request) {

        $rules = [
            'uid' => 'required|email',
            'token' => 'required|string',
        ];

        $validator = Validator::make($request->only(['uid', 'token']), $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first()]);
        }

        $token = $request->get("token");
        $uid = $request->get("uid");

        /**
         * retrieve uid in reset_password table ERR_MSG
         * if the token matching the registred one
         * update is_verified status and send success message
         * if the user status verified return token expired message
         */


        $response = DB::table("password_resets")->where("email", "=", $uid)->get();

        if(!count($response))
            return ["ok" => 0, "error" => "not found"];

        if($response["0"]->token !== $token) {
            return ["ok" => 0, "error" => "invalid token"];
        }

        $response = DB::table('users')->where('email', '=', $uid)
        ->update(array('is_verified' => '1'));

        if(!$response) {
            return ["ok" => 0, "error" => "no changes affected"];
        }

        return ["ok" => 1, "message" => $uid . " become a verified email"];
    }

    public function logout(Request $request) {
        /*auth()->logout();*/
        JWTAuth::invalidate(JWTAuth::getToken());
        return ["ok" => 1, "message" => "Successfully logged out"];
    }

    // Refresh a token, which invalidates the current one
    public function refresh(Request $request) {
        $newToken = auth()->refresh();
        return ["ok" => 1, "token" => $newToken];
    }

    public function isVerified($user_id) {
        $user = User::find($user_id);
        if(!$user)
            return null;

        return $user["is_verified"] == 1 ? true : false;
    }

    public function disabled($user_id) {
        $user = User::find($user_id);
        if(!$user)
            return null;

        return $user["disabled"] == 1 ? true : false;
    }
}
