<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Option;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class OptionController extends Controller
{
    public function get($option_name)
    {
        $option = Option::where('option_name', '=', $option_name)->first();
        if($option) {
            return $option->option_value;
        }
    }

    public function set($option_name, $option_value)
    {

        $params = ["option_name" => $option_name, "option_value" => $option_value];
        $rules = [
            'option_name' => 'required',
            'option_value' => 'required',
        ];

        $validator = Validator::make($params, $rules);

        if($validator->fails()) {
            return ["error"=> $validator->errors()->first()];
        }

        $option = Option::create($params);

        
        return ["option_id" => $option->id];
    }

    public function remove($option_name)
    {
        return Option::where('option_name', '=', $option_name)->delete();
    }

    public function update($option_name, $option_value)
    {
        
        
        $params = ["option_name" => $option_name, "option_value" => $option_value];
        $rules = [
            'option_name' => 'required',
            'option_value' => 'required',
        ];

        $validator = Validator::make($params, $rules);

        return DB::table('options')->where('option_name', '=', $option_name)
        ->update(array('option_value' => $option_value));
    }
}