<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactsController extends Controller
{
    public function index(Request $request)
    {
        $contactsData = DB::table('contacts')
            ->where('contacts.deleted_at','=',Null)
            ->join('tiers','tiers.id','=','contacts.tier_id')
            ->select('contacts.*','tiers.nom')
            ->get();
        return response()->json($contactsData,201);
    }

    public function getCurrentContact($id)
    {
        $currentContact = DB::table('contacts')
            ->where('contacts.id','=',$id)
            ->join('tiers','tiers.id','=','contacts.tier_id')
            ->select('contacts.*','tiers.nom')
            ->first();
        return response()->json($currentContact,201);
    }

    public function storeContacts(Request $request)
    {
        if($contact = contacts::create($request->all()))
        {
            return response()->json($contact, 201);
        }
        else
            return response()->json(['data'=>'error in the insert'], 500);
    }

    public function updateContacts(Request $request)
    {
        $contactUpdate = contacts::findOrFail($request->input('id'));
        if($contact = $contactUpdate->update($request->all()))
        {
            return response()->json($contact, 201);
        }
        else
            return response()->json(['data'=>'error in the update'], 500);
    }

    public function deleteContact($id)
    {
        if($contact = contacts::find($id))
        {
            $contact->delete();
            return response()->json($contact, 201);
        }else
            return response()->json(['data'=>'error in the delete'], 500);
    }

    public function getTiers()
    {
        $tiersData= DB::table('tiers')
            ->select('tiers.id','tiers.nom')
            ->get();
        return response()->json($tiersData,201);
    }
}
