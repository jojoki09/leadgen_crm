<?php

namespace App\Http\Controllers;

use App\societe_adresse;
use Illuminate\Http\Request;

class SocieteAdresseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\societe_adresse  $societe_adresse
     * @return \Illuminate\Http\Response
     */
    public function show(societe_adresse $societe_adresse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\societe_adresse  $societe_adresse
     * @return \Illuminate\Http\Response
     */
    public function edit(societe_adresse $societe_adresse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\societe_adresse  $societe_adresse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, societe_adresse $societe_adresse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\societe_adresse  $societe_adresse
     * @return \Illuminate\Http\Response
     */
    public function destroy(societe_adresse $societe_adresse)
    {
        //
    }
}
