<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehiculeModel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;
use Validator;
use Illuminate\Support\Str;

class VehiculeModelController extends Controller
{
    public function index() {
        return VehiculeModel::all();
    }
    
    public function get($model_id) {
        return VehiculeModel::findOrFail($model_id);
    }

    public function store(Request $request) {
        $resource =  $request->only(["model_name","brand_id","schema_url","photo","year"]);
        
        $rules = [
            'model_name' => 'required|string',
            "brand_id" => "required|numeric|exists:vehicule_brands,id",
            "schema_url" => 'required|string',
            'photo' => 'mimes:jpeg,bmp,png,jpeg,gif|max:20481',
            "year" => "numeric",
        ];

        $validator = Validator::make($resource, $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first()]);
        }

        if($request->hasFile('photo')) {
            $file = $request->file('photo');
            $fake_name = Str::limit(Str::slug(Hash::make($resource["model_name"])), 25) . '_' . $file->getClientOriginalName();

            $path = Storage::putFileAs(
                'images/vehicules/models', $file, $fake_name
            );

            $resource["photo"] = $fake_name;
        }
    
        $resource_id = VehiculeModel::create($resource)->id;

        return response()->json(["ok"=> 1, "feedback"=> "we generate a new resource for you", "model_id" => $resource_id]);
    }

    public function update(Request $request) {
        
        if(!$request->has("id"))
            return ["ok"=> 0, "error" => "specify the resource id required"];
        
        $id = $request->get("id");
        $old_resource =  VehiculeModel::find($id);

        $new_resource = $request->except(["id","removePhoto"]);
        if(!count($new_resource)) 
            return ["ok"=> 0, "error" => "no given info"];

        if ( !$old_resource ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        $rules = [
            'model_name' => 'string|max:50',
            'photo' => 'mimes:jpeg,bmp,png,jpeg,gif|max:20481',
        ];

        $validator = Validator::make($new_resource, $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first()]);
        }


        $old_photo = $old_resource["photo"];

        if($request->hasFile("photo")) {
            Storage::delete('images/vehicules/models/' . $old_photo);

            $file = $request->file('photo');

            if(isset($new_resource["model_name"]))
                $fake_name = Str::limit(Str::slug(Hash::make($new_resource["model_name"])), 25) . '_' . $file->getClientOriginalName();
            else
                $fake_name = Str::limit(Str::slug(Hash::make($old_resource["model_name"])), 25) . '_' . $file->getClientOriginalName();

            if($request->hasFile('photo')) {
                $path = Storage::putFileAs(
                    'images/vehicules/models/', $request->file('photo'), $fake_name
                );
            }

            $new_resource["photo"] = $fake_name;
        }

        if($request->has("removePhoto")) {
            Storage::delete('images/vehicules/models/' . $old_photo);
            $new_resource["photo"] = null;
        }
        

        VehiculeModel::where("id", $id)
            ->update($new_resource);

        return response()->json(["ok"=> 1, "feedback"=> "go to main page to see changes"]);
    }

    public function destroy($model_id) {
        VehiculeModel::findOrFail($model_id)->delete();
        return ["ok" => 1, "feedback" => "the resource softly deleted, check the trash"];
    }

    public function getBrand($model_id) {
        $model =  VehiculeModel::find($model_id);
        if (!$model) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        return $model->brand()->get();
    }


    public function getPhoto($model_id) {

        $model =  Vehiculemodel::find($model_id);
        if (!$model) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        $photo = $model->photo;
        if(!$photo) 
            return ["ok"=> 0, "error" => "no photo given for " . $model->model_name];

        if($photo) {
            $slices = explode('.', $photo);
            $ext = end($slices);
            $content = Storage::get('images/vehicules/models/' . $photo);
            $encoded_content = base64_encode($content);
            $imageData = 'data:' . 'image/' . $ext  . ';base64,' . $encoded_content;
            return ["ok"=> 1, "imageData" => $imageData];
        }
    }

    public static function  deletePhoto($model_id) {

        $model =  Vehiculemodel::find($model_id);
        if (!$model) return response()->json(["ok"=> 0, "error" => "not found"], 404);       

        $photo = $model->photo;
        if(!$photo) 
            return ["ok"=> 0, "error" => "no photo given for " . $model->model_name];

        // set null to photo in database
        Vehiculemodel::where("id", $model_id)
        ->update(["photo" => null]);

        // remove the photo from the local storage
        Storage::delete('images/vehicules/models/' . $photo);

        // return success feedback
        return ["ok"=> 1, "feedback"=> "the model photo removeed from storage"];
        

    }

    public function getPhotos() {

        $models =  Vehiculemodel::all();

        if(!count($models))
            return ["ok"=> 0, "error" => "empty gallery"];

        $object = [];
        
        foreach ($models as $obj => $brand) {
            $logo = $brand->photo;

            if($logo) {
                $slices = explode('.', $logo);
                $ext = end($slices);

                $relative = 'images/vehicules/models/' . $logo;
                if(!Storage::exists($relative))
                    return ["ok"=> 0, "error" => "not copy in local storage [$relative]"];

                $content = Storage::get('images/vehicules/models/' . $logo);
                $encoded_content = base64_encode($content);
                $imageData = 'data:' . 'image/' . $ext  . ';base64,' . $encoded_content;
                array_push($object, ["model_id" => $brand->id, "imageData" => $imageData]);
            }

        }

        return $object;
    }



    public static function getModelById($model_id) {
        return \DB::table("vehicule_models")->where("id", "=", $model_id)->where("deleted_at", "=", null)->first();
    }
}
