<?php

namespace App\Http\Controllers;


use App\Components\ListesProc\models\ListesProcepect;
use App\Components\ListesProc\Repositories\ListesProRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ListesProcepectController extends Controller
{
    /**
     * @var
     */
    private $ListesProRepository;
    /**
     * ListesProRepository constructor.
     * @param ListesProRepository  $ListesProRepository
     */
    public function __construct(ListesProRepository $ListesProRepository)
    {
        $this->ListesProRepository = $ListesProRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('listprocepect')
            ->select('listprocepect.*')
            ->get();

        return response()->json($data,200);

    }

    public function getProcepectById($id) {

        $liste = DB::table('listprocepect')
            ->where('part_id','=',$id)
            ->select('listprocepect.*')
            ->first();

        return response()->json($liste,200);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listesPrecepect = $this->ListesProRepository->find($id);

        if(!$listesPrecepect) return $this->sendResponseNotFound();

        return response()->json($listesPrecepect,200);
    }
}
