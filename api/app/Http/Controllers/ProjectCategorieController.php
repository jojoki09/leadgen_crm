<?php

namespace App\Http\Controllers;

use App\project_categorie;
use Illuminate\Http\Request;

class ProjectCategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\project_categorie  $project_categorie
     * @return \Illuminate\Http\Response
     */
    public function show(project_categorie $project_categorie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\project_categorie  $project_categorie
     * @return \Illuminate\Http\Response
     */
    public function edit(project_categorie $project_categorie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\project_categorie  $project_categorie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, project_categorie $project_categorie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\project_categorie  $project_categorie
     * @return \Illuminate\Http\Response
     */
    public function destroy(project_categorie $project_categorie)
    {
        //
    }
}
