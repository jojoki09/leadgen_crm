<?php

namespace App\Http\Controllers;

use App\Departement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Log;

class DepartementController extends Controller
{
    public function index()
    {

        $sql = "
            select ud.*, count(u.id) as 'total_users' from user_departement ud left join users u on  ud.id = u.departement_id 
            where ud.deleted_at is null and u.deleted_at is null
            group by ud.id
            order by ud.created_at desc
        ";

        return \DB::select(\DB::raw($sql));
    }

    public function  offer_rights(Request $request) {
        

        $departement_id = $request->get("id");
        $departement =  Departement::find($departement_id);

        

        if ( !$departement ) return response()->json(["ok"=> 0, "error" => "not found"], 404);

        // remove the old rights
        $this->removeDepartementRights($departement_id);
        
        $offered_rights = [];

        if($request->has('rights')) { 
            $rights = $request->get('rights');
            for ($i=0; $i < count($rights) ; $i++) { 
                array_push($offered_rights, ["departement_id" => $departement_id, "right_id" => $rights[$i]]);
            }
        }

        DB::table('departement_rights')->insert($offered_rights);
        
        return ["ok" => 1, "feedback" => "rights has been updated"];
    }

    public function removeDepartementRights($departement_id) {
        DB::table('departement_rights')->where('departement_id', '=', $departement_id)->delete();
    }


    public function store(Request $request)
    {


        $rules = [
            'departement_name' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json(["ok"=> 0, "error"=> $validator->errors()->first() ]);
        }

        $departement = Departement::create($request->all());

        return response()->json(["ok"=> 1, "feedback"=> "we generate a new resource for you", "departement_id" => $departement->id]);

    }

    public function getDepartementRights($departement_id) {
        $rights = DB::table('rights')
            ->join('departement_rights', 'rights.id', '=', 'departement_rights.right_id')
            ->where('departement_rights.departement_id', '=', $departement_id)
            ->select('rights.*')
            ->get();

        return $rights;
    }



    public function update(Request $request)
    {
        Departement::where("id", $request->get("id"))
            ->update($request->except(["id"]));

        return response()->json(["ok"=> 1, "feedback"=> "go to main page to see changes"]);
    }


    public function destroy($departement_id)
    {
        Departement::findOrFail($departement_id)->delete();
        return ["ok" => 1, "feedback" => "the resource softly deleted, check the trash"];
    }
}
