<?php

namespace App\Http\Controllers;

use App\Orders;
use App\product_stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;


class OrdersController extends Controller
{
    public function index(Request $request)
    {
        $orders = DB::table('orders')
        -> where('orders.deleted_at','=',NULL)
        ->join('tiers','tiers.id','=','orders.provider_id')
        ->join('products','products.id','=','orders.product_id')
        ->join('product_categories','product_categories.id','=','products.category_id')
        ->select('orders.*','tiers.nom','product_categories.category_name','product_categories.id as category_id','products.label')
        ->orderBy('orders.id','desc')
        ->get();
        return response()->json($orders,201);
    }

    public function OrderBoitier()
    {
        $OrderBoitier = DB::table('orders')
            -> where('orders.deleted_at','=',NULL)
            ->join('tiers','tiers.id','=','orders.provider_id')
            ->join('products','products.id','=','orders.product_id')
            ->join('product_categories','product_categories.id','=','products.category_id')
            -> where('product_categories.category_name','like', '%' .'boitier'.'%')
            ->select('orders.*','tiers.nom','product_categories.category_name','product_categories.id as category_id','products.label', DB::raw('(orders.quantity - orders.received_quantity ) as reste'))
            ->orderBy('orders.id','desc')
            ->get();
            $date = Carbon::now();
            $Date = $date->toDateString();
           
            foreach($OrderBoitier as $order)
            {
                if($order->order_status == '0' || $order->order_status == '2')
                {
                    $ship_date = $order->ship_date;
                   
                    if($ship_date < $Date)
                    {
                        $dureeSejour = strtotime($Date) - strtotime($ship_date);  
                        $delay = $dureeSejour/86400;
                        DB::table('orders')
                        ->where('id','=',$order->id)
                        ->update([
                            'delay' =>$delay
                        ]);
                    }
                }
            }
            
        return response()->json($OrderBoitier,201);
    }

    public function OrderSim()
    {
        $OrderSim = DB::table('orders')
            -> where('orders.deleted_at','=',NULL)
            ->join('tiers','tiers.id','=','orders.provider_id')
            ->join('products','products.id','=','orders.product_id')
            ->join('product_categories','product_categories.id','=','products.category_id')
            -> where('product_categories.category_name','like', '%' .'carte sim'.'%')
            ->select('orders.*','tiers.nom','product_categories.category_name','product_categories.id as category_id','products.label',DB::raw('(orders.quantity - orders.received_quantity) as reste'))
            ->orderBy('orders.id','desc')
            ->get();

            $date = Carbon::now();
            $Date = $date->toDateString();
           
            foreach($OrderSim as $order)
            {
                if($order->order_status == '0' || $order->order_status == '2' )
                {
                    $ship_date = $order->ship_date;
                    
                    if($ship_date < $Date)
                    {
                        $dureeSejour = strtotime($Date) - strtotime($ship_date);  
                        $delay = $dureeSejour/86400;
                        DB::table('orders')
                        ->where('id','=',$order->id)
                        ->update([
                            'delay' =>$delay
                        ]);
                    }
                }
            }
            
        return response()->json($OrderSim,201);
    }

    public function OrderOthers()
    {
        $autresOrders = DB::table('orders')
            -> where('orders.deleted_at','=',NULL)
            ->join('tiers','tiers.id','=','orders.provider_id')
            ->join('products','products.id','=','orders.product_id')
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','not like', '%' .'carte sim'.'%')
            ->where('product_categories.category_name','not like', '%' .'boitier'.'%')
            ->select('orders.*','tiers.nom','product_categories.category_name','product_categories.id as category_id','products.label')
            ->orderBy('orders.id','desc')
            ->get();

            $date = Carbon::now();
            $Date = $date->toDateString();
           
            foreach($autresOrders as $order)
            {
                if($order->order_status == '0' || $order->order_status == '2')
                {
                    $ship_date = $order->ship_date;
                   
                    if($ship_date < $Date)
                    {
                        $dureeSejour = strtotime($Date) - strtotime($ship_date);  
                        $delay = $dureeSejour/86400;
                        DB::table('orders')
                        ->where('id','=',$order->id)
                        ->update([
                            'delay' =>$delay
                        ]);
                    }
                }
            }
        return response()->json($autresOrders,201);
    }

    public function getOrderById($id)
    {
        $orders = DB::table('orders')
            ->where('orders.id','=',$id)
            ->join('tiers','tiers.id','=','orders.provider_id')
            ->join('products','products.id','=','orders.product_id')
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->select('orders.*','tiers.nom','product_categories.category_name','product_categories.id as category_id','products.label')
            ->first();
        return response()->json($orders,201);
    }

    public function getProductBoxs()
    {
        $product= DB::table('products')
            -> where('products.deleted_at','=',NULL)
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','like', '%' .'boitier'.'%')
            ->join('tiers','tiers.id','=','products.provider_id')
            ->select('products.label','products.id','product_categories.category_name','tiers.nom','tiers.id as tier_id')
            ->distinct('tiers.nom')
            ->get();
        return response()->json($product,201);
    }

    public function getProvidersBoxs()
    {
        $providerBox= DB::table('products')
            -> where('products.deleted_at','=',NULL)
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','like', '%' .'boitier'.'%')
            ->join('tiers','tiers.id','=','products.provider_id')
            ->select('tiers.nom','tiers.id')
            ->distinct()
            ->get();
        return response()->json($providerBox,201);
    }

    public function getProductSim()
    {
        $product= DB::table('products')
            -> where('products.deleted_at','=',NULL)
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','like', '%' .'carte sim'.'%')
            ->join('tiers','tiers.id','=','products.provider_id')
            ->select('products.label','products.id','product_categories.category_name','tiers.nom','tiers.id as tier_id')
            ->get();
        return response()->json($product,201);
    }

    public function getProvidersSim()
    {
        $providerSim= DB::table('products')
            -> where('products.deleted_at','=',NULL)
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->where('product_categories.category_name','like', '%' .'carte sim'.'%')
            ->join('tiers','tiers.id','=','products.provider_id')
            ->select('tiers.nom','tiers.id')
            ->distinct()
            ->get();
        return response()->json($providerSim,201);
    }

    public function getProductByCategorie($id)
    {
        $product= DB::table('products')
            ->where('products.deleted_at','=',NULL)
            ->where('category_id','=',$id)
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->select('products.label','products.id','product_categories.category_name')
            ->get();
        return response()->json($product,201);
    }

    public function getProvidersByProduct($id)
    {
        $providers= DB::table('products')
            ->where('products.deleted_at','=',NULL)
            ->where('category_id','=',$id)
            ->join('product_categories','product_categories.id','=','products.category_id')
            ->join('tiers','tiers.id','=','products.provider_id')
            ->select('tiers.nom','tiers.id')
            ->distinct()
            ->get();
        return response()->json($providers,201);
    }

    public function storeOrder(Request $request)
    {
        if($order = orders::create($request->all()))
        {
            return response()->json($order, 201);
        }
        else
            return response()->json(['data'=>'error in the insert'], 500);
    }

    public function updateOrder(Request $request)
    {
        $orderUpdate = orders::findOrFail($request->input('id'));
        if($order = $orderUpdate->update($request->all()))
        {
            return response()->json($order, 201);
        }
        else
            return response()->json(['data'=>'error in the update'], 500);
    }

    public function updateBulkCommandes(Request $request)
    {
        if($request->get('received_quantity'))
        {
            if($request->get('quantity') == $request->get('received_quantity'))
            {
                $orderUpdate = orders::findOrFail($request->input('id'));
                $order = $orderUpdate->update($request->all());

                $bulkProducts = DB::table('bulk_products')
                    ->leftJoin('users','bulk_products.installateur','=','users.id')
                    ->leftJoin('tiers','bulk_products.client_id','=','tiers.id')
                    ->join('orders','bulk_products.order_id','=','orders.id')
                    ->join('products','orders.product_id','=','products.id')
                    ->where('products.id','=',$request->get('product_id'))
                    ->select('bulk_products.*','orders.reference','products.label','users.first_name','users.last_name','tiers.nom as client','bulk_products.bl_document')
                    ->orderBy('bulk_products.operation_date','asc')
                    ->orderBy('bulk_products.created_at','asc')
                    ->get();

                $data = [];
                $reste = [];
                $lastData =[];

                foreach($bulkProducts as $p)
                {
                    if($p->operation_date <= $request->get('ship_date'))
                    {
                        Log::info($p->operation_date);
                        array_push($data,array($p));
                    }

                    if($p->operation_date > $request->get('ship_date'))
                    {
                        Log::info($p->operation_date);
                        array_push($reste,$p);
                    }
                }

                array_push($lastData, end($data));

                if(!empty($data))
                {
                    if(!empty($lastData))
                    {
                        foreach ($lastData as $obj)
                        {
                            $remaining = $obj[0]->remaining;
                            Log::info($remaining);
                        }

                        $store_bulk_stock =  DB::table('bulk_products')->insert([
                            [
                                'order_id' => $request->get('id'),
                                'entered' => $request->get('quantity'),
                                'output'=>'0',
                                'remaining' => $request->get('quantity') + $remaining,
                                'operation_date'=> $request->get('ship_date'),
                            ]]);

                        $stock_final = $request->get('quantity') + $remaining;

                        foreach ($reste as $r)
                        {
                            if(!empty($r->entered))
                            {
                                DB::table('bulk_products')
                                    ->where('id','=',$r->id)
                                    ->update([
                                        'remaining' =>$stock_final + $r->entered
                                    ]);

                                $stock_final = $stock_final + $r->entered;
                            }
                            else
                            {
                                DB::table('bulk_products')
                                    ->where('id','=',$r->id)
                                    ->update([
                                        'remaining' =>$stock_final - $r->output
                                    ]);
                                $stock_final = $stock_final - $r->output;
                            }
                        }

                        return ["ok" => 1, "message" => "Commande modifier et produits ajouter au stock "];
                    }
                }
                else
                {
                    $store_bulk_stock =  DB::table('bulk_products')->insert([
                        [
                            'order_id' => $request->get('id'),
                            'entered' => $request->get('quantity'),
                            'output'=>'0',
                            'remaining' => $request->get('quantity'),
                            'operation_date'=> $request->get('ship_date'),
                        ]
                    ]);
                    return ["ok" => 1, "message" => "Commande modifier et produits ajouter au stock "];

                }
            }


            if($request->get('quantity') > $request->get('received_quantity'))
            {
                $orderUpdate = orders::findOrFail($request->input('id'));
                $order = $orderUpdate->update($request->all());
                return ["ok" => 0, "error" => "Quantité commandé " . $request->get('quantity') . " supérieur à la quantité reçu " . $request->get('received_quantity')];
            }

            if($request->get('quantity') < $request->get('received_quantity'))
            {
                $orderUpdate = orders::findOrFail($request->input('id'));
                $order = $orderUpdate->update($request->all());
                return ["ok" => 0, "error" => "Quantité commandé " . $request->get('quantity') . " inférieur à la quantité reçu " . $request->get('received_quantity')];
            }
        }
        else
        {
            $orderUpdate = orders::findOrFail($request->input('id'));
            if($order = $orderUpdate->update($request->all()))
            {
                return ["ok" => 1, "message" => "Commande modifier avec succée"];
            }
            else
             return ["ok" => 0, "error" => "Erreur lors de modification de commande"];

        }
    } 

    public function deleteOrder($id)
    {
        if($order = orders::find($id))
        {
            $order->delete();
            return response()->json($order, 201);
        }else
            return response()->json(['data'=>'error in the delete'], 500);
    }

    public function getIdOrder()
    {
        if($id = DB::table('orders')->orderBy('id','desc')->select('id')->pluck('id')->first())
        {
            return response()->json($id, 201);
        }
        else
        {
            return response()->json(['data'=>'error in the get'], 500);
        }
    }

    public function store(Request $request)
    {
        $Qte_Recu= $request->get("quantite_recu")-1;
        $data_order = $request->get("order");
        $Qte_Cmd = $data_order['quantity'];
        $product_id = $data_order['product_id'];

        if($Qte_Cmd == $Qte_Recu)
        {
            $rows = DB::table('product_stock')
                ->select('imei','gsm')
                ->get();

            if($request->has("carte_sim")) {
                $carte_sim = $request->get("carte_sim");

                if(!count($carte_sim))
                    return ["ok" => 0, "error" => "empty array"];
                foreach ($carte_sim as $key => $value)
                {
                    $ssid = $value["ssid"];
                    $gsm = $value["gsm"];
                    foreach ($rows as $row => $product)
                    {
                        if($ssid == $product->imei)
                        {
                            return ["ok" => 0, "error" => $ssid . ", [SSID] déja existe, merci de vérifier le fichier importé "];
                        }
                        if($gsm == $product->gsm)
                        {
                            return ["ok" => 0, "error" => $gsm . ", [GSM] déja existe, merci de vérifier le fichier importé"];
                        }
                        if($ssid == $product->imei && $gsm == $product->gsm)
                        {
                            return ["ok" => 0, "error" => "[GSM]: " . $gsm . ", [SSID]: " . $ssid . "existent déja, merci de vérifier le fichier importé"];
                        }
                    }
                }

                # ---------- verified

                $date = Carbon::now();
                $Date = $date->toDateString();

                $data_order['order_status'] = '1';
                $order = orders::create($data_order);
                $order_Id= $order->id;
                $ship_date =$order->ship_date;
                if($ship_date < $Date)
                {
                    $dureeSejour = strtotime($Date) - strtotime($ship_date);
                    $delay = $dureeSejour/86400;
                    DB::table('orders')
                        ->where('id','=',$order_Id)
                        ->update([
                            'delay' =>$delay
                        ]);
                }
                foreach ($carte_sim as $key => $value)
                {
                    $store_ProductStock =  DB::table('product_stock')->insert([
                        [
                            'product_id' =>  $product_id,
                            'order_id' => $order_Id,
                            'imei'=>$value["ssid"],
                            'gsm' => $value["gsm"],
                            'status'=> '0',
                            'disabled'=> '1',
                        ]
                    ]);
                }
                return ["ok" => 1, "message" => "Commande ajouter à votre systeme"];
            }

            if($request->has("gps_box")) {
                $gps_box= $request->get("gps_box");

                if(!count($gps_box))
                    return ["ok" => 0, "error" => "empty array"];

                foreach ($gps_box as $key => $value)
                {
                    $ssid = $value["ssid"];
                    foreach ($rows as $row => $product)
                    {
                        if($ssid == $product->imei)
                            return ["ok" => 0, "error" => $ssid . ", [SSID] déja existe"];
                    }
                }
                $data_order['order_status'] = '1';
                $order = orders::create($data_order);
                $order_Id= $order->id;
                foreach ($gps_box as $key => $value)
                {
                    try {
                        $store_ProductStock =  DB::table('product_stock')->insert([
                            [
                                'product_id' =>  $product_id,
                                'order_id' => $order_Id,
                                'imei'=>$value["ssid"],
                                'status'=> '0',
                                'disabled'=> '1',
                            ]
                        ]);
                    } catch(Exception $e) {
                        return ["ok" => 0, "error" => $e->getMessage()];
                    }
                }
                return ["ok" => 1, "message" => "Commande ajouter et fichier importé avec succès"];
            }

        }

        if($Qte_Cmd > $Qte_Recu)
        {
            $data_order['order_status'] = '2';
            orders::create($data_order);
            return ["ok" => 2, "error" => "Fichier non importé , La quantité reçue ".$Qte_Recu." est inférieure à la commande ".$Qte_Cmd ];

        }
        if($Qte_Cmd < $Qte_Recu)
        {
            $data_order['order_status'] = '2';
             orders::create($data_order);
             return ["ok" => 2, "error" => "Fichier non importé , La quantité reçue ".$Qte_Recu." est supérieur à la commande ".$Qte_Cmd ];

        }
    }

    public function update(Request $request)
    {
        $Qte_Recu= $request->get("quantite_recu")-1;
        $data_order = $request->get("order");
        $Qte_Cmd = $data_order['quantity'];
        $product_id = $data_order['product_id'];
        if($Qte_Cmd == $Qte_Recu)
        {
            $rows = DB::table('product_stock')
                ->select('imei','gsm')
                ->get();

            if($request->has("carte_sim")) {
                $carte_sim = $request->get("carte_sim");

                if(!count($carte_sim))
                    return ["ok" => 0, "error" => "empty array"];
                foreach ($carte_sim as $key => $value)
                {
                    $ssid = $value["ssid"];
                    $gsm = $value["gsm"];
                    foreach ($rows as $row => $product)
                    {
                        if($ssid == $product->imei)
                        {
                            return ["ok" => 0, "error" => $ssid . ", [SSID] déja existe, merci de vérifier le fichier importé"];
                        }
                        if($gsm == $product->gsm)
                        {
                            return ["ok" => 0, "error" => $gsm . ", [GSM] déja existe, merci de vérifier le fichier importé"];
                        }
                        if($ssid == $product->imei && $gsm == $product->gsm)
                        {
                            return ["ok" => 0, "error" => "[GSM]: " . $gsm . ", [SSID]: " . $ssid . "existent déja, merci de vérifier le fichier importé"];
                        }
                    }
                }

                # ---------- verified
                $data_order['order_status'] = '1';
                
                $orderUpdate = orders::findOrFail($data_order['id']);
                $update = $orderUpdate->update($data_order);

                foreach ($carte_sim as $key => $value)
                {
                    $store_ProductStock =  DB::table('product_stock')->insert([
                        [
                            'product_id' =>  $product_id,
                            'order_id' => $data_order['id'],
                            'imei'=>$value["ssid"],
                            'gsm' => $value["gsm"],
                            'status'=> '0',
                            'disabled'=> '1',
                        ]
                    ]);
                }
                return ["ok" => 1, "message" => "Commande modifier et fichier importé avec succès"];
            }

            if($request->has("gps_box")) {
                $gps_box= $request->get("gps_box");

                if(!count($gps_box))
                    return ["ok" => 0, "error" => "empty array"];

                foreach ($gps_box as $key => $value)
                {
                    $ssid = $value["ssid"];
                    foreach ($rows as $row => $product)
                    {
                        if($ssid == $product->imei)
                            return ["ok" => 0, "error" => $ssid . ", [SSID] déja existe"];
                    }
                }
                $data_order['order_status'] = '1';
                
                $orderUpdate = orders::findOrFail($data_order['id']);
                $update = $orderUpdate->update($data_order);
                foreach ($gps_box as $key => $value)
                {
                    $store_ProductStock =  DB::table('product_stock')->insert([
                        [
                            'product_id' =>  $product_id,
                            'order_id' => $data_order['id'],
                            'imei'=>$value["ssid"],
                            'status'=> '0',
                            'disabled'=> '1',
                        ]
                    ]);
                }
                return ["ok" => 1, "message" => "Commande modifier et fichier importé avec succès"];
            }
        }

        if($Qte_Cmd > $Qte_Recu)
        {
            $data_order['order_status'] = '2';
            $orderUpdate = orders::findOrFail($data_order['id']);
            $update = $orderUpdate->update($data_order);
            return ["ok" => 2, "error" => "Fichier non importé , La quantité reçue ".$Qte_Recu." est inférieure à la commande ".$Qte_Cmd ];
        }

        if($Qte_Cmd < $Qte_Recu)
        {
            $data_order['order_status'] = '2';
            $orderUpdate = orders::findOrFail($data_order['id']);
            $update = $orderUpdate->update($data_order);
            return ["ok" => 2, "error" => "Fichier non importé , La quantité reçue ".$Qte_Recu." est supérieur à la commande ".$Qte_Cmd];
        }

    }

    public function storeFile(Request $request)
    {
        $file = $request->fichier;
        $fichier = $file->getClientOriginalName();
        $fake_name = $request->reference.'_'. $fichier;
        $path = $request->file('fichier')->storeAs(
            'xlsx', $fake_name
        );
    }

    public function getFile($doc_name)
    {
        $path = storage_path("app/xlsx/$doc_name");

        $headers = [
            'Content-Type' => mime_content_type($path),
        ];
        return response()->download($path, $doc_name, $headers);
    }

    public function getOthersCategory()
    {
        $autresCategories =  DB::table('product_categories')
            ->where('product_categories.category_name','not like', '%' .'carte sim'.'%')
            ->where('product_categories.category_name','not like', '%' .'boitier'.'%')
            ->where('product_categories.deleted_at','=',Null)
            ->select('id','category_name')
            ->get();
        return response()->json($autresCategories,201);
    }
}
