<?php

namespace App\Http\Controllers;

//use App\Components\company\models;
use App\Components\company\Models\societe;
use Illuminate\Http\Request;
use App\Components\company\Repositories\SocieteRepository;

class SocieteController extends AdminController
{
    /**
     * @var SocieteRepository
     */
    private $SocieteRepository;

    /**
     * GroupController constructor.
     * @param SocieteRepository $SocieteRepository
     */
    public function __construct(SocieteRepository $SocieteRepository)
    {
        $this->SocieteRepository = $SocieteRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->SocieteRepository->index(request()->all());

        return $this->sendResponseOk($data,"Get groups ok.");
    }

    public function destroy($id)
    {
        try {
            $this->SocieteRepository->delete($id);
        } catch (\Exception $e) {
            return $this->sendResponseBadRequest("Failed to delete");
        }

        return $this->sendResponseDeleted();
    }
}
