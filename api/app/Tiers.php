<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tiers extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable =
        [
            'id',
            'nom',
            'name_alias',
            'code',
            'address',
            'zip' ,
            'town' ,
            'pays' ,
            'phone' ,
            'email' ,
            'cin' ,
            'rc' ,
            'ape' ,
            'ice' ,
            'cnss' ,
            'patente' ,
            'client' ,
            'fournisseur' ,
            'revendeur' ,
            'type_interne_id' ,
            'type_general_id' ,
            'user_id' ,
        ];

}
