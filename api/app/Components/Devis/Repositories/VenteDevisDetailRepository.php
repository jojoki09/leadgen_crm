<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 10/6/2017
 * Time: 4:58 PM
 */

namespace App\Components\Devis\Repositories;


use App\Components\Core\BaseRepository;
use App\Components\Devis\Models\vente_devis_detail;
use App\Components\Core\Utilities\Helpers;

class VenteDevisDetailRepository extends BaseRepository
{
    public function __construct(vente_devis_detail $model)
    {
        parent::__construct($model);
    }

    /**
     * index items
     *
     * @param array $params
     * @return VenteDevisDetailRepository[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]|mixed[]
     */
    public function index($params)
    {
        return $this->get($params,["actionType","status"],function($q) use ($params)
        {
            $devis_id = array_get($params,'devis_id','');

            $q->where('vente_devis_id','=',"{$devis_id}");
            $q->orderBy($params['order_by'] ?? 'id', $params['order_sort'] ?? 'desc');

            return $q;
        });
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id)
        {
            /** @var Group $Group */
            $Group = $this->model->find($id);

            if(!$Group) return false;

            $Group->users()->detach();
            $Group->delete();
        }

        return true;
    }
}
