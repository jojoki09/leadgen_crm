<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 10/6/2017
 * Time: 4:58 PM
 */

namespace App\Components\Devis\Repositories;


use App\Components\Core\BaseRepository;
use App\Components\Devis\Models\vente_devis;
use App\Components\Core\Utilities\Helpers;

class VenteDevisRepository extends BaseRepository
{
    public function __construct(vente_devis $model)
    {
        parent::__construct($model);
    }

    /**
     * index items
     *
     * @param array $params
     * @return VenteDevisRepository[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]|mixed[]
     */
    public function index($params)
    {
        return $this->get($params,['vente_devis_detail','partenaire','projet','adresse','contact','source'],function($q) use ($params)
        {
            /*$name = array_get($params,'nom','');

            $q->where('nom','like',"%{$name}%");*/
            $q->orderBy($params['order_by'] ?? 'devis_id', $params['order_sort'] ?? 'desc');

            return $q;
        });
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id)
        {
            /** @var Group $Group */
            $Group = $this->model->find($id);

            if(!$Group) return false;

            $Group->users()->detach();
            $Group->delete();
        }

        return true;
    }
}