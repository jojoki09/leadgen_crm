<?php

namespace App\Components\Devis\Models;

use App\Components\partners\Models\partenaire;
use App\Components\projects\Models\project;
use App\Components\Devis\Models\vente_devis_detail;
use App\contact;
use App\adresse;
use App\Components\Listes\Models\listes;
use Illuminate\Database\Eloquent\Model;

class vente_devis extends Model
{
    protected $fillable = ['devis_num','devis_date','description','part_ref','partenaire_id','adresse_id','contact_id','projet_id','employe_id','depot_id','devise_id','date_planif','date_expire','fichier','type','source'];
    protected $primaryKey = 'devis_id';
    public function partenaire()
    {
        return $this->belongsTo(partenaire::class,'partenaire_id','part_id');
    }
    public function projet()
    {
        return $this->belongsTo(project::class,'projet_id','id');
    }
    public function vente_devis_detail()
    {
        return $this->hasMany(vente_devis_detail::class,'vente_devis_id','devis_id');
    }
    public function adresse()
    {
        return $this->hasManyThrough(adresse::class,partenaire::class,'part_id','partenaire_id','devis_id','part_id');
    }
    public function contact()
    {
        return $this->hasManyThrough(contact::class,partenaire::class,'part_id','partenaire_id','devis_id','part_id');
    }
    public function source()
    {
        return $this->belongsTo(listes::class,'source');
    }
}
