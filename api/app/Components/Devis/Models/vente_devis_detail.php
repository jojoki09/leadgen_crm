<?php

namespace App\Components\Devis\Models;

use App\Status;
use App\type_action;
use Illuminate\Database\Eloquent\Model;

class vente_devis_detail extends Model
{
    protected $fillable = ['vente_devis_id','article_id','variante_id','prix_typ','prix_qte','price','taux_tva','remise','unite_id','area','quality','statut','description','part_ref','date_planif','parent','doc','ligne','object','type','etat'];
    public function vente_devis()
    {
        return $this->belongsTo(vente_devis::class);
    }
    public function actionType()
    {
        return $this->belongsTo(type_action::class,'type');
    }
    public function status()
    {
        return $this->belongsTo(Status::class,'statut');
    }
}
