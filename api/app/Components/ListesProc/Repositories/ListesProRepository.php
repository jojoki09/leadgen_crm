<?php
/**
 * Created by PhpStorm.
 * User: Effectus Media
 * Date: 04/02/2019
 * Time: 15:12
 */

namespace App\Components\ListesProc\Repositories;

use App\Components\Core\BaseRepository;
use App\Components\ListesProc\Models\ListesProcepect;
use App\Components\Core\Utilities\Helpers;

class ListesProRepository extends BaseRepository
{
    public function __construct(ListesProcepect $model)
    {
        parent::__construct($model);
    }

    /**
     * index items
     *
     * @param array $params
     * @return ListesProRepository[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]|mixed[]
     */
    public function index($params)
    {
        return $this->get($params,[],function($q) use ($params)
        {
            $nom = array_get($params,'nom','');
            $projects = explode(',',array_get($params,'project_id',''));
            $status = explode(',',array_get($params,'status_id',''));
            $part_id = array_get($params,'part_id','');

            $q->where('nom','like',"%{$nom}%");

            if(count($projects) > 0 && !empty($projects[0])) $q->whereIn('projet_id',$projects);
            if(count($status) > 0 && !empty($status[0])) $q->whereIn('statut',$status);

            $q->orderBy($params['order_by'] ?? 'devis_id', $params['order_sort'] ?? 'desc');

            return $q;
        });
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id)
        {
            $category = $this->model->find($id);

            if(!$category) return false;

            $category->projet()->detach();
            $category->delete();
        }
        return true;
    }
}
