<?php

namespace App\Components\ListesProc\Models;

use Illuminate\Database\Eloquent\Model;

class ListesProcepect extends Model
{
    protected $table = 'listprocepect';
    protected $primaryKey = 'devis_id';
}
