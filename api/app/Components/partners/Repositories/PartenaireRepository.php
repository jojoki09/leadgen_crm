<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 10/6/2017
 * Time: 4:58 PM
 */

namespace App\Components\partners\Repositories;


use App\Components\Core\BaseRepository;
use App\Components\partners\Models\partenaire;
use App\Components\Core\Utilities\Helpers;

class PartenaireRepository extends BaseRepository
{
    public function __construct(partenaire $model)
    {
        parent::__construct($model);
    }

    /**
     * index items
     *
     * @param array $params
     * @return PartenaireRepository[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]|mixed[]
     */
    public function index($params)
    {
        return $this->get($params,['vente_devis','vente_devis_detail','contact','adresse','projcets','source'],function($q) use ($params)
        {
            $q->ofName($params['id'] ?? '');
            $q->ofName($params['nom'] ?? '');
            $q->OfProject(Helpers::commasToArray($params['project_id'] ?? ''));
            $q->orderBy($params['order_by'] ?? 'part_id', $params['order_sort'] ?? 'desc');
            return $q;
        });
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id)
        {
            $category = $this->model->find($id);

            if(!$category) return false;

            $category->projet()->detach();
            $category->delete();
        }
        return true;
    }
}