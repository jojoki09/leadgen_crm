<?php

namespace App\Components\partners\Models;

use App\adresse;
use App\Components\Devis\Models\vente_devis;
use App\Components\Devis\Models\vente_devis_detail;
use App\Components\projects\Models\project;
use App\contact;
use Illuminate\Database\Eloquent\Model;
use App\Components\Listes\Models\listes;
class partenaire extends Model
{
    protected $fillable = ['nom','type','city_id','user_id'];
    protected $primaryKey = 'part_id';
    public function adresse()
    {
        return $this->hasMany(adresse::class,'partenaire_id','part_id');
    }
    public function contact()
    {
        return $this->hasMany(contact::class,'partenaire_id','part_id');
    }
    public function vente_devis()
    {
        return $this->hasMany(vente_devis::class,'partenaire_id','part_id');
    }
    public function projcets()
    {
        return $this->hasManyThrough(project::class,vente_devis::class,'partenaire_id','id','part_id','projet_id');
    }
    public function vente_devis_detail()
    {
        return $this->hasManyThrough(vente_devis_detail::class,vente_devis::class,'partenaire_id','vente_devis_id','part_id','devis_id');
    }
    public function source()
    {
        return $this->hasManyThrough(listes::class,vente_devis::class,'partenaire_id','id','part_id','source');
    }
    public function scopeOfName($query, $name)
    {
        if( $name === null || $name === '' ) return false;

        return $query->where('nom','LIKE',"%{$name}%");
    }
    public function scopeOfId($query, $id)
    {
        if( $id === null || $id === '' ) return false;

        return $query->where('id','=',$id);
    }
    public function scopeOfProject($q,$v)
    {
        if($v === false || $v === '' || count($v)==0 || $v[0]=='') return $q;

        return $q->whereHas('projcets',function($q) use ($v)
        {
            return $q->whereIn('projects.id',$v);
        });
    }
}
