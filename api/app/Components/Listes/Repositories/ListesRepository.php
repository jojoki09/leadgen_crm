<?php
/**
 * Created by PhpStorm.
 * User: Effectus Media
 * Date: 05/02/2019
 * Time: 10:25
 */

namespace App\Components\Listes\Repositories;


use App\Components\Core\BaseRepository;
use App\Components\Listes\Models\listes;
use App\Components\Core\Utilities\Helpers;
class ListesRepository extends BaseRepository
{
    public function __construct(listes $model)
    {
        parent::__construct($model);
    }

    /**
     * index items
     *
     * @param array $params
     * @return ListesRepository[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]|mixed[]
     */
    public function index($params)
    {
        return $this->get($params,[],function($q) use ($params)
        {
            $type = array_get($params,'type','');

            $q->where('type','=',"{$type}");
            /*$q->orderBy($params['order_by'] ?? 'devis_id', $params['order_sort'] ?? 'desc');*/
            return $q;
        });
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id)
        {
            /** @var Group $Group */
            $Group = $this->model->find($id);

            if(!$Group) return false;

            $Group->users()->detach();
            $Group->delete();
        }

        return true;
    }
}