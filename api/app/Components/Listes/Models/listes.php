<?php

namespace App\Components\Listes\Models;

use App\Components\Devis\Models\vente_devis;
use App\Components\Devis\Models\vente_devis_detail;
use Illuminate\Database\Eloquent\Model;

class listes extends Model
{

    protected $fillable = ['group_id','code','nom','etat'];

    //protected $primaryKey = ['id','type'];

    public function vente_devis()
    {
        return $this->hasMany(vente_devis::class);
    }
    public function vente_devis_detail()
    {
        return $this->hasMany(vente_devis_detail::class);
    }
}
