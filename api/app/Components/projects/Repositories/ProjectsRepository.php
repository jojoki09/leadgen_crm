<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 10/6/2017
 * Time: 4:58 PM
 */

namespace App\Components\projects\Repositories;


use App\Components\Core\BaseRepository;
use App\Components\projects\Models\project;
use App\Components\Core\Utilities\Helpers;

class ProjectsRepository extends BaseRepository
{
    public function __construct(project $model)
    {
        parent::__construct($model);
    }

    /**
     * index items
     *
     * @param array $params
     * @return CategorieRepository[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]|mixed[]
     */
    public function index($params)
    {
        return $this->get($params,['categories','adresse','societe','villes'],function($q) use ($params)
        {
            $q->OfCategories(Helpers::commasToArray($params['categorie_id'] ?? ''));
            $q->ofName($params['nom'] ?? '');
            $q->OfEtat($params['etat'] ?? '');

            return $q;
        });
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id)
        {

            $projet = $this->model->find($id);

            if(!$projet)
            {
                return false;
            };

            $projet->categories()->detach();
            $projet->adresse()->delete();
            $projet->delete();
        }

        return true;
    }
}