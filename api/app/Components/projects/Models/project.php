<?php

namespace App\Components\projects\Models;

use App\adresse;
use App\Components\company\Models\societe;
use App\Components\vente_devis\Models\vente_devis\vente_devis;
use App\ville;
use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    //
    protected $fillable = ['nom','societe_id','etat','categorie_id','photo'];

    public function categories()
    {
        return $this->belongsToMany(categorie::class,'project_categories','projet_id');
    }

    public function adresse()
    {
        return $this->hasMany(adresse::class);
    }

    public function societe()
    {
        return $this->belongsTo(societe::class);
    }

    public function scopeOfName($query, $name)
    {
        if( $name === null || $name === '' ) return false;

        return $query->where('nom','LIKE',"%{$name}%");
    }
    public function scopeOfEtat($query, $name)
    {
        if( $name === null || $name === '' ) return false;

        return $query->where('etat','=',"{$name}");
    }
    public function scopeOfCategories($q,$v)
    {
        if($v === false || $v === '' || count($v)==0 || $v[0]=='') return $q;

        return $q->whereHas('categories',function($q) use ($v)
        {
            return $q->whereIn('categorie_id',$v);
        });
    }
    public function vente_devis()
    {
        return $this->hasMany(vente_devis::class);
    }
    public function villes(){
        return $this->hasManyThrough(ville::class,adresse::class,'project_id','id','id','ville_id');
    }
}