<?php

namespace App\Components\projects\Models;

use App\Components\projects\models\project;
use Illuminate\Database\Eloquent\Model;

class categorie extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $fillable = ['nom','code'];

    public function projet()
    {
        return $this->belongsToMany(project::class,'project_categories','categorie_id');
    }
}
