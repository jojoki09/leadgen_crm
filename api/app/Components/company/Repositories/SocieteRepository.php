<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 10/6/2017
 * Time: 4:58 PM
 */

namespace App\Components\company\Repositories;


use App\Components\Core\BaseRepository;
use App\Components\company\Models\societe;
use App\Components\Core\Utilities\Helpers;

class SocieteRepository extends BaseRepository
{
    public function __construct(societe $model)
    {
        parent::__construct($model);
    }

    /**
     * index items
     *
     * @param array $params
     * @return SocieteRepository[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]|mixed[]
     */
    public function index($params)
    {
        return $this->get($params,[],function($q) use ($params)
        {
            $nom = array_get($params,'nom','');

            $q->where('nom','like',"%{$nom}%");

            return $q;
        });
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id)
        {
            $company = $this->model->find($id);

            if(!$company) return false;

            $company->delete();
        }
        return true;
    }
}