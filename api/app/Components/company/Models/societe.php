<?php

namespace App\Components\company\Models;

use App\adresse;
use Illuminate\Database\Eloquent\Model;
use App\Componenets\projects\models\project;
use App\contact;

class societe extends Model
{
    protected $fillable = ['nom','created_by'];

    public function societe_adresse(){
        $this->hasMany(adresse::class);
    }

    public function contact(){
        $this->hasMany(contact::class);
    }
    public function project(){
        return $this->hasMany(project::class);
    }
}
