<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ville extends Model
{
    protected $fillable = ['pays_id','code','nom'];
}
