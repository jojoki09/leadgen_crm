<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 31 Oct 2019 14:16:50 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 *
 * @property int $id
 * @property string $name
 * @property string $phonecode
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Country extends Eloquent
{
	protected $fillable = [
		'name',
		'phonecode'
	];
}
