<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Right extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected  $table = "rights";

    protected $fillable = [
        "module_id",
        "right_name",
        "description",
    ];


    public function module()
    {
        return $this->hasMany('App\Module', "id", "module_id");
    }

    
    public function roles()
    {
        return $this->belongsToMany('App\Role', "role_rights", "right_id", "role_id");
    }


    public function departement()
    {
        return $this->belongsToMany('App\Departement', "departement_rights", "right_id", "departement_id");
    }

    public function users() {
        return $this->belongsToMany('App\User', "user_rights");
    }
}
