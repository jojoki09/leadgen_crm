<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class type_action extends Model
{
    protected $table = 'type_action';
    protected $primaryKey = 'code';
}
