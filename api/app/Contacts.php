<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Contacts extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable =
        [
            'id',
            'tier_id',
            'lastname',
            'firstname',
            'address',
            'zip',
            'town',
            'pays',
            'poste',
            'email',
            'cin',
            'phone',
            'phone_personal',
            'phone_mobile',
        ];
}
