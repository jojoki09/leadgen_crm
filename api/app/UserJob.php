<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserJob extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected  $table = "user_jobs";
    protected $fillable = [
        "job_title",
    ];


    /**
     * Get the users work under job
     */
    public function users()
    {
        return $this->hasMany('App\User', "job_id", "id");
    }
}
