<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable =
        [
            'id',
            'code_service',
            'reference',
            'label',
            'description',
            'description_facture',
            'price',
            'price_unit',
            'price_ttc',
            'price_min',
            'price_min_ttc',
            'tva',
            'duration',
            'unite_temps',
            'tobuy',
            'tosell',
            'renouvelable',
        ];

}
