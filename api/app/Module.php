<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected  $table = "right_module";

    protected $fillable = [
        "module_name",
        "description",
    ];


    public function rights()
    {
        return $this->hasMany('App\Right');
    }

}
