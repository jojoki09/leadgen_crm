<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Log;

class ImeisImport implements  ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    public function model(array $row)
    {

        if(strtolower($row[0]) !== "imei" && strtolower($row[0]) !== "imeis") {
            Log::info($row[0]);
        }

    }

    
}
