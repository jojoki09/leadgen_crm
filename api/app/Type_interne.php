<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type_interne extends Model
{
    protected $table = "type_interne";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable =
        [
            'id',
            'types',
        ];
}
