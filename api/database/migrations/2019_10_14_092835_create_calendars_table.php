<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->text('object')->nullable();
            $table->timestamp('date_planif')->nullable();

            $table->timestamps();
        });
        Schema::table('calendars', function(Blueprint $table) {
            $table->unsignedInteger('part_id');
            $table->unsignedInteger('project_id');
            $table->foreign('part_id')->references('part_id')->on('partenaires')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('project_id')->references('id')->on('projects')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
