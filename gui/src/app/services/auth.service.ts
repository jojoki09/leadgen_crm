import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { TokenService } from './token.service';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import {environment} from "../../environments/environment";

import { HandleError, HttpErrorHandler } from 'app/main/administration/services/http-error-handler.service';
import { User } from 'app/main/administration/users/user.model';
import { UsersService } from 'app/main/administration/users/users.service';
import { Right } from 'app/main/administration/rights/right.model';
import { NodeLogger } from '@angular/core/src/view';
import { WebDriverLogger } from 'blocking-proxy/built/lib/webdriver_logger';

const httpOptions = {
  headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'my-auth-token'
  })
};


@Injectable({
  providedIn: 'root'
})
export class AuthService {

    hasOne: boolean  = false;
    permissions: any = [];

    private loggedIn = new BehaviorSubject<boolean>(this._tokenService.loggedIn());
    authStatus = this.loggedIn.asObservable();

    private rights = new BehaviorSubject<Right[]>([]);
    onRightsChanged = this.rights.asObservable();

    apiUrl = `${environment["API_URL"]}auth/`;  // URL to web api
    private handleError: HandleError;


    private $online = new BehaviorSubject<any>({});
    authenticatedUser = this.$online.asObservable();

    onAvatarChanged = new Subject<boolean>();

    constructor(
        private router: Router,
        private http: HttpClient,
        private _usersService: UsersService,
        httpErrorHandler: HttpErrorHandler,
        private  _tokenService: TokenService,
    ) { 
        this.handleError = httpErrorHandler.createHandleError('AuthService');

    }

    changeAuthStatus(value: boolean) {
        this.loggedIn.next(value);
    }

    logout() {
        this.http.post(`${this.apiUrl}logout`, {}).subscribe(response => {
            this._tokenService.remove();
            this.changeAuthStatus(false);
            this.router.navigate(["auth", "login"]);
        });
    }

    register() {
        
    }

    sendPasswordResetToken() {

    }

    authenticate(credentials) {
        return this.http.post(`${this.apiUrl}login`, credentials)
        .pipe(
            catchError(this.handleError('authenticate', credentials))
        );
    }

    getAuthenticatedUser() {
        return this.http.get<any>(`${environment["API_URL"]}auth/authenticated_user`).pipe(
            map(_ => _ = _.user),
            tap((user: any) => {

                this.hasOne = true;
                
                let requestAvatar = false;
                /* set avatar undefind for default mode */
                
                requestAvatar = user["photo"] != "null";
                user.photo = undefined;

                /* tell the whole app we have a new authenticatedUser WITHOUT avatar may take seconds*/
                this.setUser(user);


                /* request avatar if the user has one */
                if(requestAvatar)
                    this._usersService.getUserAvatar(user.id).subscribe((data: any) => {
                        user.photo = data.imageData;
                        this.setUser(user);
                });

                /*  read the correct rights */
                const rights_depending  = parseInt(user.rights_depending);
                const user_id = parseInt(user.id) || undefined;
                /* debugger; */
                // [0] Static definition, [1] permission based on role, [3] permissions based on departement
                switch (rights_depending) {
                    case 0:
                        this._usersService.getUserRights(user_id).subscribe((rights: Right[]) => {
                            this.rights.next(rights);
                            this.permissions = rights;
                            /* debugger; */
                        })
                        break;
                    case 1:
                        this._usersService.getUserRoleRights(user_id).subscribe((rights: Right[]) => {
                            this.rights.next(rights);
                            this.permissions = rights;
                            /* debugger; */
                        })
                        break;
                    case 2:
                        this._usersService.getUserDeptRights(user_id).subscribe((rights: Right[]) => {
                            this.rights.next(rights);
                            this.permissions = rights;
                            /* debugger; */
                        })
                        break;
                }
            }
                
            ),
        );
    }

    reset_password() {

    }

    refresh() {

    }

    verify_email(ticket: string) {
        return this.http.post<any>(`${environment["API_URL"]}verify_email`, {ticket});
    }

    setUser(_: any) {
        this.$online.next(_);
    }

    removeAvatar() {
                
    }

    getAuthenticatedUserId() {
        const payload = this._tokenService.payload(this._tokenService.get());
        return parseInt(payload.user_id) || undefined;
    }

    myRights(): Right[] {
        return this.permissions || [];
    }

    hasRight(right_name: string, module_id: number) : boolean {
        return this.myRights().filter((right: Right) => right.right_name == right_name && right.module_id == module_id).length > 0;
    }

}
