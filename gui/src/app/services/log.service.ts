import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor() { }

  log(_: any) {
      console.log("------------######---------------");
      console.log(_);
      console.log("------------######---------------");
  }
}
