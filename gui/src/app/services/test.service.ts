import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(
      private http: HttpClient,
  ) { }


    download() {
        return this.http.get(`${environment["API_URL"]}download`);
    }
}
