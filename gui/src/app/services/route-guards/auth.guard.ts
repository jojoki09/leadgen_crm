import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { TokenService } from '../token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private route:Router, 
    private _authService: AuthService,
    private _tokenService: TokenService,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<boolean> | Promise<boolean> | boolean {

      /* console.log("checking before access."); */

      const loggedIn = this._tokenService.isValid();

      if(!loggedIn)
        this.route.navigate(["/auth", "login"]);
      else {
        if(!this._authService.hasOne) 
          this._authService.getAuthenticatedUser().subscribe();
      }

      return loggedIn;
  }
}
