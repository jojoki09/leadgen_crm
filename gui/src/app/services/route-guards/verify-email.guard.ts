import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { TokenService } from '../token.service';
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class VerifyEmailGuard implements CanActivate {

  constructor(
    private route:Router, 
    private _authService: AuthService,
    private logger: NGXLogger, 
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<boolean> | Promise<boolean> | boolean {
    
    const ticket = next.queryParams.ticket || undefined;
    if(ticket)
      this._authService.verify_email(ticket).subscribe((_) => { this.route.navigate(["auth", "login"]); });
      

    return false;
  }
}
