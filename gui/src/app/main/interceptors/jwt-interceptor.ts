import { Injectable } from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from 'app/services/auth.service';
import { TokenService } from 'app/services/token.service';
import { map } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { Route } from '@angular/compiler/src/core';

@Injectable({
    providedIn: 'root',
})
export class JwtInterceptor implements HttpInterceptor {



    constructor(
        private router: Router, 
        private _tokenService: TokenService,
        private _authService: AuthService,
        private logger: NGXLogger,
        ) {

    }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const loggedIn = this._tokenService.isValid();

        if(!loggedIn)
            return next.handle(request);

        request = request.clone({ setHeaders: { Authorization: `Bearer ${this._tokenService.get()}` }});

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    /* console.log('event--->>>', event); */
                    /* token_is_invalid
                    token_is_expired
                    authorization_token_not_found */

                    const status = event.body["status"] || undefined;

                    switch (status) {
                        case "token_is_invalid":
                            console.log("token_is_invalid");
                            this._authService.logout();
                            break;
                        case "token_is_expired":
                            console.log("token_is_expired");
                            this._authService.logout();
                            break;
                        case "authorization_token_not_found":
                            console.log("authorization_token_not_found");
                            this._authService.logout();
                            break;
                    }
                    

                }
                return event;
            }));



}


}






