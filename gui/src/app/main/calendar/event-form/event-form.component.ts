import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';

import { MatColors } from '@fuse/mat-colors';
import { CalendarEventModel } from 'app/main/calendar/event.model';
import {UsersService} from "../../administration/users/users.service";
import {ProjectService} from "../../partner/project.service";
import {ProductsService} from "../../gestion-project/projects/products.service";
import {CalendarService} from "../calendar.service";

@Component({
    selector     : 'calendar-event-form-dialog',
    templateUrl  : './event-form.component.html',
    styleUrls    : ['./event-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CalendarEventFormDialogComponent
{
    action: string;
    event: CalendarEvent;
    eventForm: FormGroup;
    dialogTitle: string;
    presetColors = MatColors.presets;
    projects: any;
    partners: any;
    // users: any;

    // TODO verify right before accesing calendar part

    /**
     * Constructor
     *
     * @param {MatDialogRef<CalendarEventFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private projectService: ProductsService,
        private calendarService: CalendarService,
        public matDialogRef: MatDialogRef<CalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    )
    {
        this.event = _data.event;
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = this.event.partname;
        }
        else
        {
            this.dialogTitle = 'New Event';
            this.event = new CalendarEventModel({
                start: _data.date,
                end  : _data.date
            });
        }

        // this.users =  this.userService.getUsers();

        this.projectService.getProjects().subscribe(projects => {
            this.projects = projects;
        });
        this.calendarService.getPartners().subscribe(partners =>{
            this.partners = partners;
        });


        this.eventForm = this.createEventForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    searchProject(nom) {
        if(nom != "")
            this.projects = this.projects.length > 0 ? this.projects.filter(project =>   ("" + project.nom).toLocaleLowerCase().indexOf(nom.toLocaleLowerCase().trim()) > -1) : [];
        else
            this.projects = this.projects;
    }

    // searchUser(nom) {
    //     if(nom != "")
    //         this.users = this.users.length > 0 ? this.users.filter(users =>   ("" + users.nom).toLocaleLowerCase().indexOf(nom.toLocaleLowerCase().trim()) > -1) : [];
    //     else
    //         this.users = this.users;
    // }


    /**
     * Create the event form
     *
     * @returns {FormGroup}
     */
    createEventForm(): FormGroup
    {
        return new FormGroup({
            title : new FormControl(this.event.title),
            start : new FormControl(this.event.start),
            color : this._formBuilder.group({
                primary  : new FormControl(this.event.color.primary),
            }),
        });
    }
}
