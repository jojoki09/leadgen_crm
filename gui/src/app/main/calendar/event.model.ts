import { CalendarEventAction } from 'angular-calendar';
import { startOfDay, endOfDay } from 'date-fns';

export class CalendarEventModel
{   id: number;
    start: Date;
    end?: Date;
    title: string;
    partname: string;
    projectname: string;
    color: {
        primary: string;
    };
    actions?: CalendarEventAction[];
    allDay?: boolean;
    cssClass?: string;
    resizable?: {
        beforeStart?: boolean;
        afterEnd?: boolean;
    };
    draggable?: boolean;
    meta?: {
        part_id: number,
        project_id: number
    };

    /**
     * Constructor
     *
     * @param data
     */
    constructor(data?)
    {
        data = data || {};
        this.id = data.id || '';
        this.start = new Date(data.date_planif) || startOfDay(new Date());
        this.end = new Date(data.end) || endOfDay(new Date());
        this.title = data.object || '';
        this.partname = data.partname || '';
        this.projectname = data.projectname || '';
        this.color = {
            primary  : data.color || '#1e90ff'
        };
        this.draggable = data.draggable;
        this.resizable = {
            beforeStart: data.resizable && data.resizable.beforeStart || true,
            afterEnd   : data.resizable && data.resizable.afterEnd || true
        };
        this.actions = data.actions || [];
        this.allDay = data.allDay || false;
        this.cssClass = data.cssClass || '';
        this.meta = {
            part_id: data.part_id || '',
            project_id   : data.project_id || ''

        };
    }
    // part_id
    // project_id
}
