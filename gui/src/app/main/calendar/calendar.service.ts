import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import {environment} from "../../../environments/environment";
import {tap} from "rxjs/operators";

@Injectable()
export class CalendarService implements Resolve<any>
{
    apiUrl = environment["API_URL"];
    events: any;
    onEventsUpdated: Subject<any>;
    routeParams: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        // Set the defaults
        this.onEventsUpdated = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {
            Promise.all([
                this.getEvents()
            ]).then(
                ([events]: [any]) => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get events
     *
     * @returns {Promise<any>}
     */
    getEvents(): Promise<any>
    {
        return new Promise((resolve, reject) => {

            this._httpClient.get(`${this.apiUrl}calendars/`)
                .subscribe((response: any) => {
                    console.log(response);
                    this.events = response;
                    this.onEventsUpdated.next(this.events);
                    resolve(this.events);
                }, reject);


            // if ( this.routeParams.id === 'new' )
            // {
            //     this.onPartnertChanged.next(false);
            //     resolve(false);
            // }
            // else
            // {
            //
            //     this.http.get<any>(`${this.apiUrl}ProcepectByPartId/${this.routeParams.id}`)
            //         .subscribe((response: any) => {
            //             console.log(response);
            //             this.partner = response;
            //             this.onPartnertChanged.next(this.partner);
            //             resolve(response);
            //         }, reject);
            // }
        });
    }

    getPartners(): Observable<any> {
        return this._httpClient.get(`${this.apiUrl}Procepect`);
    }

    /**
     * Update events
     *
     * @param events
     * @returns {Promise<any>}
     */
    updateEvents(events): Promise<any>
    {
        console.log(events);
        return new Promise((resolve, reject) => {
            this._httpClient.put(`${this.apiUrl}calendars/${events.id}`, events)
                .subscribe((response: any) => {
                    this.getEvents();
                }, reject);
        });
    }

    deleteEvents(event_id): Observable<any> {

        return this._httpClient.delete(`${this.apiUrl}calendars/${event_id}`)
    }

}
