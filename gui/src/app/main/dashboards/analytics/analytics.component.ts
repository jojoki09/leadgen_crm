import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { AnalyticsDashboardService } from 'app/main/dashboards/analytics/analytics.service';
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import * as moment from "moment";
declare let $: any;
// import * as $ from "jquery";



@Component({
    selector     : 'analytics-dashboard',
    templateUrl  : './analytics.component.html',
    styleUrls    : ['./analytics.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AnalyticsDashboardComponent implements OnInit
{
    public barChartOptions: ChartOptions = {
        responsive: true,
    };
     mois = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

    years = ["2018","2019","2020","2021","2022","2023","2024","2025"];
    monthChange : any;
    yearChange : any;
    project_id: any;
    filterdProjects = [];
    projects = [];


    public barChartLabels :any[] = [];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];

    barChartLabels1: Label[] = ['Apple', 'Banana', 'Kiwifruit', 'Blueberry', 'Orange', 'Grapes'];

    barChartData1 = [
        { data: [], label: '' }
    ];

    private _unsubscribeAll: Subject<any>;

    public barChartData: ChartDataSets[] = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Project A' },
        { data: [28, 48, 40, 19, 86, 27, 90], label: 'Project B' }
    ];


    widgets: any;
    widget1SelectedYear = '2016';
    widget5SelectedDay = 'today';

    /**
     * Constructor
     *
     * @param {AnalyticsDashboardService} _analyticsDashboardService
     */
    constructor(
        private _analyticsDashboardService: AnalyticsDashboardService
    )
    {
        // Register the custom chart.js plugin
        this._registerCustomChartJSPlugin();
        this._unsubscribeAll = new Subject();

    }

    getComercialByProject(payload) {

        // pipe(takeUntil(this._unsubscribeAll)).
        this._analyticsDashboardService.getComercialByProject(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe( Data =>
            {
                    console.log(Data);
                    this.barChartLabels = [];
                    let labels = [];
                    let data = [];
                    let project : string;
                    $.each(Data, function (key, value) {

                        // this.barChartLabels = [value.commercial]
                        // this.barChartData
                        console.log(value.commercial);
                        labels.push(value.commercial);
                        data.push(value.NbPropsect);
                        // this.barChartData1[0].data.push(value.NbPropsect);
                        project = value.nom;
                        // public barChartData: ChartDataSets[] = [
                    //         { data: [65, 59, 80, 81, 56, 55, 40], label: 'Project A' },
                    //         { data: [28, 48, 40, 19, 86, 27, 90], label: 'Project B' }
                    //     ];

                        // barChartData1: ChartDataSets[] = [
                        //     { data: [45, 37, 60, 70, 46, 33], label: 'Best Fruits' }
                        // ];




                    });
                console.log(data);


                this.barChartData1[0].label = project;

                this.barChartData1[0].data = data;

                // for(let i = 0; i < data.length; i++)
                // {
                //     this.barChartData1[i].data.push(data[i]);
                // }

                this.barChartLabels1 = labels;
                this.barChartLabels = labels;

                console.log(this.barChartLabels);

            },
            (error: any) =>{
            console.log(error);


            }

        );

    }

    yearChanged(event)
    {
       console.log(event);

        var date=new Date();

        const year = date.getFullYear().toString().substr(-2);
        const month = this.mois[date.getMonth()];
        console.log(month);
        console.log(date.getFullYear()  + '----'+ month);

        let payload = {};

        payload["year"]=event ;


        payload["month"]= this.monthChange;

        console.log(this.monthChange);

        if(payload["month"] == null)
        {
            payload["month"]= month;
            this.monthChange = month;

        }

        payload["project"]= this.project_id;

        // $('#month option').each(function() {
        //     console.log("test allah allah");
        //     if ($(this).is(':selected'))
        //     {
        //         console.log($(this).val());
        //     }
        //
        // });

        // if ($('#month')[0].selectedIndex <= 0) {
        //     payload["month"]= month;
        // }
        // else
        //     payload["month"]= $('#month').val();

        console.log(payload);


        this.getComercialByProject(payload)

        // this._analyticsDashboardService.ProjectBycommercial(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((response: any) => {
        //
        //
        //
        //     try {
        //         $.each(response, function (key, value) {
        //
        //             this.barChartLabels.push(value.)
        //             installateur.push(value.first_name);
        //             inst.push(value.instalation);
        //             des.push(value.desinstallation);
        //             reco.push(value.reconfiguration);
        //             ver.push(value.verification);
        //
        //         });
        //     }
        //     catch (e) {
        //         this._matSnackBar.open(response.error, '', {
        //             duration: 10000,
        //             horizontalPosition: "end",
        //             verticalPosition: "bottom",
        //         });
        //     }
        // });


    }

    // searchYear(nom) {
    //     if(nom != "")
    //         this.filterdYears = this.years.length > 0 ? this.years.filter(year =>   ("" + year.nom).toLocaleLowerCase().indexOf(nom.toLocaleLowerCase().trim()) > -1) : [];
    //     else
    //         this.filterdYears = this.years;
    // }



    projectChanged(event)
    {
        var date=new Date();

        const year = date.getFullYear().toString();
        const month = this.mois[date.getMonth()];

        let payload = {};

        payload["month"]= this.monthChange;
        payload["year"]= this.yearChange;

        if(payload["month"] == null)
        {
            payload["month"]= month;
            this.monthChange = month;

        }
        if(payload["year"] == null)
        {
            payload["year"] = year;

            this.yearChange = year;
        }

        payload["project"]= this.project_id;

        console.log(payload);

        this.getComercialByProject(payload)

    }

    monthChanged(event)
    {

        var date=new Date();

        const year = date.getFullYear().toString().substr(-2);

        let payload = {};

        payload["month"]=event ;

        payload["year"]= this.yearChange;

        console.log(this.monthChange);

        if(payload["year"] == null)
        {
            payload["year"] = year;

            this.yearChange = year;
        }

        payload["project"]= this.project_id;
        // project

        console.log(payload);

        this.getComercialByProject(payload)

    }


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Get the widgets from the service
        this.widgets = this._analyticsDashboardService.widgets;

        this._analyticsDashboardService.projects.subscribe(projects => { this.projects = projects; this.filterdProjects = projects;})

        this._analyticsDashboardService.getProjects().subscribe();

    }
    touchDate(event){

        console.log(event);

        $('.date-picker').datepicker( {
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy',
            onClose: function(dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, 1));
            }
        });

    }




    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register a custom plugin
     */
    private _registerCustomChartJSPlugin(): void
    {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                )
                {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if ( !meta.hidden )
                    {
                        meta.data.forEach(function (element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + 'k';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

}

