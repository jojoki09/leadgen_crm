import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from "../../../../environments/environment";
import {tap} from "rxjs/operators";

@Injectable()
export class AnalyticsDashboardService implements Resolve<any>
{

    apiUrl = environment["API_URL"];
    private _projects: BehaviorSubject<any[]> = new BehaviorSubject([]);
    public readonly projects: Observable<any[]> = this._projects.asObservable();

    widgets: any[];

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
    }
    getProjects(): Observable<any> {
        return this._httpClient.get(`${this.apiUrl}projects`)
            .pipe(tap((projects: any[]) => this._projects.next(projects) ));
    }

    getComercialByProject(Payload)
    {
        return  this._httpClient.post(`${this.apiUrl}ProjectBycommercial`,Payload);


    }


    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getWidgets()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get widgets
     *
     * @returns {Promise<any>}
     */
    getWidgets(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/analytics-dashboard-widgets')
                .subscribe((response: any) => {
                    this.widgets = response;
                    resolve(response);
                }, reject);
        });
    }
}
