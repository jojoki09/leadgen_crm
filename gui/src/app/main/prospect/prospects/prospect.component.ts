import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ProspectService} from "./prospect.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from "@angular/material";
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'app-prospect',
  templateUrl: './prospect.component.html',
  styleUrls: ['./prospect.component.scss']
})
export class ProspectComponent implements OnInit {
    dataSource: any;
    product:any;
    displayedColumns = ['nom','categories','villes', 'created_by', 'date_ouverture', 'etat', 'buttons'];
    dialogRef: any;
    totalSize = 0;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

  constructor( private prospectService: ProspectService, private http: HttpClient,private router:Router,public _matDialog: MatDialog,private _matSnackBar: MatSnackBar) {
}

  ngOnInit() {
      this.getProspect();

  }

  getProspect() {
      this.prospectService.getProspect().subscribe(prospects => {

          console.log(prospects);
          this.dataSource = new MatTableDataSource<Element>(prospects);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.product = prospects;
          this.totalSize = this.product.length;
      });

      // getProspect
  }

}
