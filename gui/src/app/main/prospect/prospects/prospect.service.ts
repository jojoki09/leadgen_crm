import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, EMPTY, Observable} from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import {Prospect} from "./prospect";
import { environment } from 'environments/environment';
import {HandleError, HttpErrorHandler} from "../../administration/services/http-error-handler.service";
import {catchError, tap} from 'rxjs/operators';
import { List } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ProspectService implements Resolve<any> {

    apiUrl = environment["API_URL"];  // URL to web api
    private handleError: HandleError;

    private _prospects: BehaviorSubject<List<any>> = new BehaviorSubject([]);
    public readonly prospects: Observable<List<any>> = this._prospects.asObservable();

    prospects_list: any[] = [];

    constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {

        this.handleError = httpErrorHandler.createHandleError('ProspectService');
        this.prospects.subscribe((users: any[]) => {
            this.prospects_list = users;
        });

        this.getProspect().subscribe((prospects: any[]) => {
            this._prospects.next(prospects);
        });

    }


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            if (!this.prospects_list.length)
                this.getProspect().pipe(catchError(error   => {
                    return EMPTY;
                })).subscribe(response => {
                    resolve(response);
                }, reject);
            else resolve(this.prospects_list);
        });
    }

    /** GET useres from the server */
    getProspect(): Observable<any> {
        return this.http.get(`${this.apiUrl}partners`)
            .pipe(
                catchError(this.handleError('getProspect', [])),
                tap((prospects: any[]) => this._prospects.next(prospects))
            );
    }



}
