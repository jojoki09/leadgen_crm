import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import { ProspectComponent } from './prospects/prospect.component';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';
import { FlexLayoutModule } from "@angular/flex-layout";

import {
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatMenuModule,
    MatCheckboxModule,
    MatDialogModule, MatRadioModule, MatDividerModule, MatToolbarModule, MatTooltipModule
} from "@angular/material";

import {NgxChartsModule} from "@swimlane/ngx-charts";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProspectService} from "./prospects/prospect.service";

const routes: Routes = [
    {
        path     : '',
        component: ProspectComponent,
    },
    // {
    //     path     : 'projects/:id',
    //     component: ProductComponent,
    //     resolve  : {
    //         data: ProductService,
    //     }
    // },


];



@NgModule({
  declarations: [ProspectComponent],
  imports: [
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        NgxChartsModule,
        MatMenuModule,
        MatCheckboxModule,
        MatDialogModule,
        MatRadioModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
        FormsModule,
        MatDividerModule,
        MatToolbarModule,
        FlexLayoutModule,
        MatTooltipModule,

    ],
    providers   : [
        ProspectService,
    ]

})
export class ProspectModule { }
