import { Injectable } from '@angular/core';
import { FormGroup } from "@angular/forms";


@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor() { }

  /**
   * 
   * @param fg 
   * @param how  "" || undefined || null
   */
    filterFormGroupValues(fg: FormGroup, how = "all") {
        let formInformation: {} = fg.getRawValue();


        switch (how) {
          case "":
            for(let prop in formInformation) {
                if(formInformation[prop] === "" ) {
                    delete formInformation[prop];
                }
            }
            break;

          case null:
            for(let prop in formInformation) {
              if(formInformation[prop] === null ) {
                  delete formInformation[prop];
              }
            }
            break;
        
          default:
            for(let prop in formInformation) {
              if(formInformation[prop] === "" || formInformation[prop] === undefined || formInformation[prop] === null ) {
                  delete formInformation[prop];
              }
            }
            break;
        }


        
        return formInformation;
    }

    cleanObj(obj, removeValues = ["", null, undefined]) {
      let body = Object.assign({}, obj);

      for(let prop in body) {
        removeValues.forEach(forbidden => {
          if(body[prop] == forbidden ) {
              delete body[prop];
          }
        });
      }
      return body;
    }

    objectToFormData(object): FormData {

      if(!Object.keys(object).length)
        return;
      
      
      // prepare formdata
      let formData: any = new FormData();

      for(let prop in object) {
        formData.append(prop, object[prop]);
      }
      return formData;
    }

    getBase64(file) {
      return new Promise((resolve, reject) => {
        const reader: any = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          // let encoded = reader.result.replace(/^data:(.*;base64,)?/, '');
          let encoded = reader.result;
          /* if ((encoded.length % 4) > 0) {
            encoded += '='.repeat(4 - (encoded.length % 4));
          } */
          resolve(encoded);
        };
        reader.onerror = error => reject(error);
      });
    }


    find_duplicate_in_array(arra1) {
        var object = {};
        var result = [];

        arra1.forEach(function (item) {
            if(!object[item])
                object[item] = 0;
            object[item] += 1;
        })

        for (var prop in object) {
            if(object[prop] >= 2) {
                result.push(prop);
            }
        }

        return result;

    }

    cleanArray(array) {
        var i, j, len = array.length, out = [], obj = {};
        for (i = 0; i < len; i++) {
            obj[array[i]] = 0;
        }
        for (j in obj) {
            out.push(j);
        }
        return out;
    }

    
}
