import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  constructor() { }

  log(body, type = "info") {
    switch (type) {
        default:
          console.log("%c" + new Date().toISOString().slice(0,10) + ": INFO :// ", "color: blue;")
          console.log(body);
    }
  }
}
