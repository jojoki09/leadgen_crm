import { Component, OnInit, OnDestroy } from '@angular/core';
import { DepartementsService } from '../departements.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Module } from '../../modules/module.model';
import { Right } from '../../rights/right.model';
import { RightsService } from '../../rights/rights.service';
import { ModulesService } from '../../modules/modules.service';
import { UtilitiesService } from '../../services/utilities.service';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { Departement } from '../departement.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


export interface ModuleRights {
  module_id?: number,
  rights?: {key: number, value: 0 |1 }[]
  }
  

@Component({
  selector: 'app-departement-add',
  templateUrl: './departement-add.component.html',
  styleUrls: ['./departement-add.component.scss']
})
export class DepartementAddComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;
  constructor(
    private _departementsService: DepartementsService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute:ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _rightsService: RightsService,
    private _moduleService: ModulesService,
    private _util: UtilitiesService,
  ) { 
    this.fg1 = this._formBuilder.group({ "departement_name"    : ['', Validators.required], "description": [''], });
    this._unsubscribeAll = new Subject();
  }

  departementRights: Right[];

  fg1: FormGroup;
  modules: Module[] = [];
  rights: Right[] = [];
  module_rights: ModuleRights[] = [];
  departements: Departement[] = [];
  action: string;
  departement_id: number;
  selectedDepartement: Departement;

  ngOnInit() {

    
    this._departementsService.getDepartements().pipe(takeUntil(this._unsubscribeAll)).subscribe((departements: Departement[]) => {
      this.departements = departements;

      this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((params: any) => {
     
        this.action = params.do;
        this.departement_id = params.departement_id;
  
        if(this.action == 'edit') {
  
          // Edit 
          this._departementsService.breadcrumb.next({
            steps: [
                "Departements",
                "Mettre à jour"
            ],
            big_header: "Editer un departement"
          });
  
          // fetch for departement with id departement_id
          
          
          if(this.departements.length > 0 && this.departements.filter((departement: Departement) => departement.id == this.departement_id).length > 0) {
            let departement: Departement = this.departements.filter((departement: Departement) => departement.id == this.departement_id)[0];

            this.selectedDepartement = departement;

            
            this.fg1.patchValue({
              departement_name    : this.selectedDepartement.departement_name,
              description  : this.selectedDepartement.description,
            })

            this.setupUpdate();
          } else {
            this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
            return;
          }
  
         
  
        } else {
  
          // Add 
          this._departementsService.breadcrumb.next({
            steps: [
                "Departements",
                "Ajouter"
            ],
            big_header: "L'ajout d'un nouveau Departement"
          });
  
          

          this.setupAdd();
  
        }
  
        
      });

      




    })

    


    

    
    
  }

  ngOnDestroy(): void
 {
     // Unsubscribe from all subscriptions
     this._unsubscribeAll.next();
     this._unsubscribeAll.complete();
 }


  setupAdd() {
    this._rightsService.getRights().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Right[]) => {
        this.rights = data || [];

        this._moduleService.getModules().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
            this.modules = data || [];

            this.modules.forEach((module: Module, i) => {
                let rights = [];
                this.getRightsByModule(module.id).forEach((right: Right, i) => {
                    rights.push({ key: right.id, value: 0});
                })

                this.module_rights.push({ module_id: module.id, rights: rights});

            })

        });
    });
  }

  setupUpdate() {
    this._rightsService.getRights().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Right[]) => {
        this.rights = data || [];

        this._moduleService.getModules().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
            this.modules = data || [];

            this.modules.forEach((module: Module, i) => {
                let rights = [];
                this.getRightsByModule(module.id).forEach((right: Right, i) => {
                    rights.push({ key: right.id, value: 0});
                })

                this.module_rights.push({ module_id: module.id, rights: rights});

                this.adjustAccessRightSectin();
            })

        });
    });
  }


  adjustAccessRightSectin() {
    this._departementsService.getDepartementRights(this.departement_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((rights: Right[]) => {
      this.departementRights = rights;
      
      this.module_rights.forEach((module_rights: ModuleRights) => {
        
        this.departementRights.forEach((ur: Right) => {
          
          
          if(module_rights.module_id == ur.module_id) {
            module_rights.rights.forEach((r: { key: number, value: number}) => {
              if(r.key == ur.id) {
                r.value = 1;
              }
            })
          }
        })
        
      })
    })
  }

  /**
   * MAIN ACTIONS
   */

  onAddDepartement(): void
  {
      var formInformation: any  = {};
      formInformation = this._util.filterFormGroupValues(this.fg1);
      

      /*  */

      

      

      this._departementsService.addDepartement(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
          if(_.ok) {


            formInformation = {};
            let departement_id = _.departement_id;

            
            let offered_rights: number[] = [];

            this.module_rights.forEach((el: ModuleRights) => {
                el.rights.forEach(right => {
                    if(right.value) {
                      offered_rights.push(right.key);
                    }
                })
            })

              this._departementsService.refresh();
            formInformation["rights"] = offered_rights;
            formInformation["id"] = departement_id;

            this._departementsService.setDepartementRights(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {

                if(_.ok) {
                  const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                      width: '30rem',
                      data: {content: _.feedback, title: "Creation un nouveau departements et définir des permissions", noThanks: false}
                  });
          
                  dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                      if(result) {
                          this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
                      }
                  });
                }
              
            })

          } else {
              const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                  width: '30rem',
                  data: {content: _.error, title: "Creation un nouveau rôle et définir des permissions", noThanks: false}
              });
      
              dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                  if(result) {
                      
                  }
              });
          }
      });
      
  }

  onEditDepartement(): void
  {
      var formInformation: any  = {};
      formInformation = this._util.filterFormGroupValues(this.fg1, null);

      formInformation["id"] = this.departement_id;

      this._departementsService.updateDepartement(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
          if(_.ok) {
            
            let offered_rights: number[] = [];

            this.module_rights.forEach((el: ModuleRights) => {
                el.rights.forEach(right => {
                    if(right.value) {
                      offered_rights.push(right.key);
                    }
                })
            })

            formInformation["rights"] = offered_rights;

            this._departementsService.refresh();

            this._departementsService.setDepartementRights(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {

                if(_.ok) {
                  const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                      width: '30rem',
                      data: {content: _.feedback, title: "Edition d'un rôle et définir des permissions", noThanks: false}
                  });
          
                  dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                      if(result) {
                          this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
                      }
                  });
                }
              
            })


              /*  */

              

          } else {
              const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                  width: '30rem',
                  data: {content: _.error, title: "Creation un nouveau rôle et définir des permissions", noThanks: true}
              });
      
              dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                  if(result) {
                      
                  }
              });
          }
      });
      
  }

  /**
   * HELPERS
   */

  getModuleProperty(module_id, property) {
    return this.modules.filter((module: Module) => { return module.id == module_id })[0][`${property}`] || undefined;
  }

  getRightProperty(right_id, property) {
      return this.rights.filter((right: Right) => { return right.id == right_id })[0][`${property}`] || undefined;
  }

  getRightsByModule(module_id): Right[]{
    return this.rights.filter((right: Right) => { return right.module_id == module_id });
  }

  setRightTo(module_id, e: Event) {
      e.stopPropagation();
      let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
          return el.module_id === module_id;
      })[0];


      matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
          el.value = 1;
      });
  }

  unsetRightTo(module_id, e: Event) {
      e.stopPropagation();

      let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
          return el.module_id == module_id;
      })[0];


      matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
          el.value = 0;
      })

  }

  onCancelEdit() {
    this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
  }

}
