import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DepartementsComponent} from "./departements.component";
import {RouterModule, Routes} from "@angular/router";
import {DepartementsService} from "./departements.service";
import { SideBarComponent } from "../side-bar/side-bar.component";

import { FuseSharedModule } from '@fuse/shared.module';
import {FuseConfirmDialogModule, FuseDemoModule, FuseSidebarModule, FuseHighlightModule} from '@fuse/components';
import {ConfirmDialogModule} from "../confirm-dialog/confirm-dialog.module";

import {MaterialUiModule} from "../../material-ui/material-ui.module";
import { FlexLayoutModule } from '@angular/flex-layout';
import { DepartementListComponent } from './departement-list/departement.component';
import { DepartementAddComponent } from './departement-add/departement-add.component';
import { SideBarModule } from '../side-bar/side-bar.module';



const routes: Routes = [
    {
        path     : '',
        component: DepartementsComponent,
        children: [
            { path: '', redirectTo: 'list', pathMatch: 'full' },
            { path: 'list', component: DepartementListComponent,},
            { path: 'add', component: DepartementAddComponent,},
        ],
    }
];

@NgModule({
  declarations: [
    
    DepartementListComponent,
    DepartementsComponent,
    DepartementAddComponent,
  ],
  imports: [
    // import module routes
    RouterModule.forChild(routes),
    // load material design style
    MaterialUiModule,
    // flexlayout for repensive design 
    FlexLayoutModule,

    FuseSharedModule,
    FuseConfirmDialogModule,
    FuseSidebarModule,
    
    // the common dialog for confirmatioin alerts
    ConfirmDialogModule,
    
    // import side bar component
    SideBarModule,

  ]
})
export class DepartementsModule { }
