import {Component, OnInit, ViewChild, OnDestroy, ElementRef} from '@angular/core';

import {
    MatDialog,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    Sort,
    MatSnackBar, MatInput,
} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import {DepartementsService} from "../departements.service";
import {Departement} from "../departement.model";

import { ConfirmDialogComponent } from "../../confirm-dialog/confirm-dialog.component";
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, map, tap } from 'rxjs/operators';
import { UsersService } from '../../users/users.service';
import { User } from '../../users/user.model';
import { instantiateRendererFactory } from '@angular/platform-browser/animations/src/providers';
import { AuthService } from 'app/services/auth.service';
import * as moment from 'moment';
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-departement-list',
  templateUrl: './departement-list.component.html',
  styleUrls: ['./departement-list.component.scss']
})
export class DepartementListComponent implements OnInit, OnDestroy {
    // Private
private _unsubscribeAll: Subject<any>;

    @ViewChild('fromInput', {
        read: MatInput
    }) fromInput: MatInput;

    @ViewChild('toInput', {
        read: MatInput
    }) toInput: MatInput;

    f = {
        "departement_name": null,
        "description": null,
        "total_users": null,
        "created_at": {
            "du": "",
            "a": "",
        },
    };


// --------------------------------------------------------
    onRefreshing: boolean = false;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filter') filter: ElementRef;
    departements: any = [];
    filteredDepartements: any = [];

    pageSize: number = 25;
    totalSize = 0;
    pageSizeOptions: number[] = [25, 75, 100, 500];

    displayedColumns: string[] =
    [
        /* 'select', */
        'departement_name',
        'description',
        'total_users',
        'created_at',
        'buttons'
    ];

    dataSource: any;
    enablePaginator: boolean = false;

    sortData(sort: Sort) {
        const data = this.departements.slice();
        if (!sort.active || sort.direction === '') {
            this.departements = data;
            return;
        }

        this.departements = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'departement_name': return compare(a.login, b.login, isAsc);
                case 'description': return compare(a.email, b.email, isAsc);
                case 'total_users': return compare(a.total_users, b.total_users, isAsc);
                case 'created_at': return compare(a.last_name, b.last_name, isAsc);
                default: return 0;
            }
        });
    }



    constructor(
        private _departementsService: DepartementsService,
        private _usersService: UsersService,
        public _authService: AuthService,
        public dialog: MatDialog,
        public snackBar: MatSnackBar,
        private router: Router,
        private activatedRoute:ActivatedRoute,
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this._departementsService.departements.pipe(takeUntil(this._unsubscribeAll)).subscribe((departements: any[]) => {
            if(departements) {
                this.dataSource = new MatTableDataSource<Element>(departements);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.departements = departements || [];
                this.totalSize = this.departements.length;
            }
        });
    }

    ngOnDestroy(): void
     {
         // Unsubscribe from all subscriptions
         this._unsubscribeAll.next();
         this._unsubscribeAll.complete();
     }

    ngOnInit() {

        this._departementsService.breadcrumb.next({
            steps: [
                "Departements",
                "List"
            ],
            big_header: "LISTE DES DEPARTEMENTS"
        });

    }





onDeleteDepartement(row: any) {
    /*
    * throw confirmation message
    * remove with success alert
    * */

    let id = row.id;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '30rem',
        data: {content: "Êtes-vous sûr de vouloir supprimer cet élément?", title: "Alerte de confirmation"}
    });

    dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
        if(result) {

            this._departementsService.deleteDepartement(id).pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
                if(res.ok){
                    this.snackBar.open(res.feedback, "ok", {
                        duration: 2500,
                        horizontalPosition: "end",
                        verticalPosition: "bottom",
                    });

                    this._departementsService.refresh();
                }
            })
        }
    });

}

  onAddDepartement() {
    this.router.navigate([`../add`], {relativeTo: this.activatedRoute});
  }


  onEditDepartement(departement_id: number) {

    const queryParams = {
      departement_id: departement_id,
      do: "edit",
    }

    Object.keys(queryParams).filter(key => queryParams[key] === null || queryParams[key] === undefined).forEach(key => delete queryParams[key])

    this.router.navigate([`../add`], { queryParams, relativeTo: this.activatedRoute});
  }

  getDepartementCount(dept_id: number) {
    return this._usersService.users.pipe(map(
        (users: User[]) =>   {
            if(users instanceof Object && users.length > 0)
                return users.filter((user: User) => user.departement_id == dept_id).length;
            else
                return 0;
        }
    ));
    /* return 99; */
  }

  onShowUsersUnderDept(dept_id: number) {

        const queryParams = {
            do: "filter",
            by: "departement",
            departement_id: dept_id,
          }
      
          this.router.navigate(['management_tools', 'users', 'list'], { queryParams });
  }

    formatDate(date, pattern, behavior) {
        return moment(date, pattern).format(behavior);
    }


    refresh() {
        this._departementsService.refresh();
    }

    clear()
    {
        this.fromInput.value = "";
        this.toInput.value = "";
        this.f.created_at.a = "";
        this.f.created_at.du = "";
        this.super_filter();
    }

    super_filter() {

        let d = true;
        let filteredData = [];

        /*f = {
            "departement_name": null,
            "description": null,
            "total_users": null,
            "created_at": {
                "du": "",
                "a": "",
            },
        };*/


        if(this.f.departement_name != null && this.f.departement_name  != "") {
            let data = filteredData.length > 0 ? filteredData : this.departements;
            filteredData = data.filter(dept => dept.departement_name.toLowerCase().indexOf(this.f.departement_name.toLowerCase()) > -1);
            d = false;
        }
        if(this.f.description != null && this.f.description  != "") {
            let data = filteredData.length > 0 ? filteredData : this.departements;
            filteredData = data.filter(dept => dept.description.toLowerCase().indexOf(this.f.description.toLowerCase()) > -1);
            d = false;
        }

        if(this.f.total_users != null && this.f.total_users  != "") {
            let data = filteredData.length > 0 ? filteredData : this.departements;
            filteredData = data.filter(dept => dept.total_users === this.f.total_users);
            d = false;
        }


        if(this.f.created_at.du != null && this.f.created_at.a != null)
        {
            var created_atPipe = new DatePipe("en-US");
            this.f.created_at.du = created_atPipe.transform(this.f.created_at.du, 'yyyy-MM-dd');
            this.f.created_at.a= created_atPipe.transform(this.f.created_at.a, 'yyyy-MM-dd');
        }

        if(this.f.created_at.du != null && this.f.created_at.a != null)
        {
            let data = filteredData.length > 0 ? filteredData : this.departements;
            filteredData = data.filter(dept => dept.created_at >= this.f.created_at.du && dept.created_at <= this.f.created_at.a );
            d = false;
        }

        if(this.f.created_at.du !=null && this.f.created_at.a == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.departements;
            filteredData = data.filter(dept => dept.created_at >= this.f.created_at.du);
            d = false;
        }

        if(this.f.created_at.a !=null && this.f.created_at.du == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.departements;
            filteredData = data.filter(dept => dept.last_login <= this.f.created_at.a);
            d = false;
        }


        this.dataSource = new MatTableDataSource<Element>(d ? this.departements : filteredData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.totalSize = d ? this.departements.length : filteredData.length;

        return d ? this.departements : filteredData;
    }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}