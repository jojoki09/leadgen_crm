export class Departement
{
    "id"?: number;
    "departement_name": string;
    "description"?: string;
    "created_at"?: string;
    "updated_at"?: string;

    constructor(departement)
    {
        {
            this.id = departement.id || undefined;
            this.departement_name = departement.departement_name || undefined;
            this.description = departement.description || undefined;
            this.created_at = departement.created_at || undefined;
            this.updated_at = departement.updated_at || undefined;

        }
    }
}
