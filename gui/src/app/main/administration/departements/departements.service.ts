import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable, BehaviorSubject } from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

import { Departement } from './departement.model';
import { HttpErrorHandler, HandleError } from '../services/http-error-handler.service';
import {environment} from "../../../../environments/environment";
import { Right } from '../rights/right.model';
import { List } from 'lodash';
import {User} from "../users/user.model";
import {Job} from "../users/job.model";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
    })
};

export interface BreadCrumb {
    steps?: string[],
    big_header?: string
}

@Injectable({
    providedIn: 'root'
})
export class DepartementsService {
    apiUrl = environment["API_URL"];  // URL to web api
    private handleError: HandleError;


    departements_list: any[] = [];

    private _departements: BehaviorSubject<List<Departement>> = new BehaviorSubject([]);
    public readonly departements: Observable<List<Departement>> = this._departements.asObservable();
   
    public breadcrumb = new BehaviorSubject<BreadCrumb>({
        steps: [
            null,
            null,
        ],
        big_header: null,
    });

    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler
    ) {
        
        this.handleError = httpErrorHandler.createHandleError('DepartementsService');


        this.departements.subscribe((departements: any[]) => { this.departements_list = departements; });

        this.getDepartements().subscribe((departements: Departement[]) => {
            this._departements.next(departements);
        });
        
    }

    /** GET departements from the server */
    getDepartements(): Observable<Departement[]> {
        return this.http.get<Departement[]>(`${this.apiUrl}departements`)
            .pipe(
                catchError(this.handleError('getDepartements', [])),
                tap((departements: any[]) => this._departements.next(departements) )
            );
    }

    getDepartementRights(departement_id: number): Observable<Right[]> {
        return this.http.get<Right[]>(`${this.apiUrl}departements/${departement_id}/rights`)
        .pipe(
            catchError(this.handleError('getDepartementRights', []))
        );
    }

    /* GET departements whose name contains search term */
    searchDepartements(term: string): Observable<Departement[]> {
        term = term.trim();

        // Add safe, URL encoded search parameter if there is a search term
        const options = term ?
            { params: new HttpParams().set('name', term) } : {};

        return this.http.get<Departement[]>(`${this.apiUrl}`, options)
            .pipe(
                catchError(this.handleError<Departement[]>('searchDepartementes', []))
            );
    }

    //////// Save methods //////////

    /** POST: add a new departement to the database */
    addDepartement(departement: Departement): Observable<Departement> {
        return this.http.post<Departement>(`${this.apiUrl}departements`, departement, httpOptions)
            .pipe(
                catchError(this.handleError('addDepartement', departement))
            );
    }

    /** DELETE: delete the departement from the server */
    deleteDepartement(id: number): Observable<{}> {
        const url = `${this.apiUrl}departements/${id}/delete`; // DELETE api/departementes/42
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteDepartement'))
            );
    }

    /** PUT: update the departement on the server. Returns the updated departement upon success. */
    updateDepartement(departement: Departement): Observable<Departement> {
        httpOptions.headers =
            httpOptions.headers.set('Authorization', 'my-new-auth-token');

        return this.http.put<Departement>(`${this.apiUrl}departements`, departement, httpOptions)
            .pipe(
                catchError(this.handleError('updateDepartement', departement))
            );
    }

    setDepartementRights(user: {id: number, rights: number[]}) {

        return this.http.post(`${this.apiUrl}departements/offer_rights`, user)
            .pipe(
                catchError(this.handleError('setDepartementRights', user))
            );
    }

    // ---------------------------------------------------------------------

    set(departements: Departement[]) {
        this._departements.next(departements);
    }

    refresh() {
        this.getDepartements().subscribe();
    }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/