import { Component, OnInit, OnDestroy } from '@angular/core';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ActivatedRoute, NavigationEnd, Router, RoutesRecognized} from "@angular/router";
import {PrintService} from "../services/print.service";
import { filter, map, takeUntil } from 'rxjs/operators';
import {BreadCrumb, DepartementsService} from "./departements.service";
import { Subject } from 'rxjs';
// import { map } from "rxjs/add/operator/map";



@Component({
  selector: 'app-departements',
  templateUrl: './departements.component.html',
  styleUrls: ['./departements.component.scss']
})
export class DepartementsComponent implements OnInit, OnDestroy {
    private _unsubscribeAll: Subject<any>;
    breadcrumb: BreadCrumb;


    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _departementsService: DepartementsService) { 
            this._unsubscribeAll = new Subject();
        }

    ngOnInit() {

        this._departementsService.breadcrumb.pipe(takeUntil(this._unsubscribeAll)).subscribe((breadcrumb: BreadCrumb) => {
            this.breadcrumb = breadcrumb;
        });


    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}
