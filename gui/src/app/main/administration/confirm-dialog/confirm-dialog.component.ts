import {Component, EventEmitter, Inject, Output, OnInit} from '@angular/core';

import {
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material';

export interface DialogData {
    title: string;
    content: string;
    type?: "info" | "success" | "danger" | "warning";
    noThanks?: boolean,
}

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
})

export class ConfirmDialogComponent implements OnInit {

    @Output() dialogClosed = new EventEmitter();
    constructor(
        public dialogRef: MatDialogRef<ConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

    onNoClick(): void {
        this.dialogRef.close();
    }

    onOkClick(): void {
        this.dialogClosed.emit();
    }

    noThanks: boolean = true;

    ngOnInit() {

        this.noThanks = this.data.noThanks;

    }
}