import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulesComponent } from './modules.component';
import { ModuleListComponent } from './module-list/module-list.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialUiModule } from 'app/main/material-ui/material-ui.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseSidebarModule, FuseDemoModule, FuseHighlightModule } from '@fuse/components';
import { ConfirmDialogModule } from '../confirm-dialog/confirm-dialog.module';
import { SideBarComponent } from '../side-bar/side-bar.component';
import { ModuleAddComponent } from './module-add/module-add.component';
import { SideBarModule } from '../side-bar/side-bar.module';


const routes: Routes = [
  {
      path     : '',
      component: ModulesComponent,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: ModuleListComponent},
      ],
  }
];


@NgModule({
  declarations: [
    ModulesComponent,
    ModuleListComponent,
    ModuleAddComponent,
  ],
  imports: [
    RouterModule.forChild(routes),

        MaterialUiModule,

        FlexLayoutModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
        FuseDemoModule,
        FuseHighlightModule,
        
        ConfirmDialogModule,

        // html editor

        // import side bar component
        SideBarModule,
        
  ],
  entryComponents: [
    ModuleAddComponent,
  ]
})
export class ModulesModule { }
