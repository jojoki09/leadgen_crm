import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Module } from './module.model';
import { HttpErrorHandler, HandleError } from '../services/http-error-handler.service';
import {environment} from "../../../../environments/environment";
import { List } from 'lodash';

export interface BreadCrumb {
    steps?: string[],
    big_header?: string
}

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
    })
};

@Injectable({
    providedIn: 'root'
})
export class ModulesService {
    apiUrl = environment["API_URL"];  // URL to web api
    private handleError: HandleError;

    private _modules: BehaviorSubject<List<Module>> = new BehaviorSubject([]);
    public readonly modules: Observable<List<Module>> = this._modules.asObservable();

    public breadcrumb = new BehaviorSubject<BreadCrumb>({
        steps: [
            "Cupiditate dignissimos quo quia quam?",
            "quia repellat nisi maiores?"
        ],
        big_header: "adipisicing elit. Voluptatibus officiis culpa cupiditate at molestiae id minima dicta quaerat blanditiis facilis, eos"
    });

    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler
    ) {

        this.handleError = httpErrorHandler.createHandleError('ModulesService');

        this.getModules().subscribe((modules: Module[]) => {
            this._modules.next(modules);
        });

    }

    /** GET modules from the server */
    getModules(): Observable<Module[]> {
        return this.http.get<Module[]>(`${this.apiUrl}modules`)
            .pipe(
                catchError(this.handleError('getModules', []))
            );
    }
    

    /* GET modules whose name contains search term */
    searchModules(term: string): Observable<Module[]> {
        term = term.trim();

        // Add safe, URL encoded search parameter if there is a search term
        const options = term ?
            { params: new HttpParams().set('name', term) } : {};

        return this.http.get<Module[]>(`${this.apiUrl}`, options)
            .pipe(
                catchError(this.handleError<Module[]>('searchModulees', []))
            );
    }

    //////// Save methods //////////

    /** POST: add a new module to the database */
    addModule(module: Module): Observable<Module> {
        return this.http.post<Module>(`${this.apiUrl}modules`, module, httpOptions)
            .pipe(
                catchError(this.handleError('addModule', module))
            );
    }

    /** DELETE: delete the module from the server */
    deleteModule(id: number): Observable<{}> {
        const url = `${this.apiUrl}modules/${id}/delete`; // DELETE api/modules/42
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteModule'))
            );
    }

    /** PUT: update the module on the server. Returns the updated module upon success. */
    updateModule(module: any): Observable<Module> {
        httpOptions.headers =
            httpOptions.headers.set('Authorization', 'my-new-auth-token');

        return this.http.put<Module>(`${this.apiUrl}modules`, module, httpOptions)
            .pipe(
                catchError(this.handleError('updateModule', module))
            );
    }


    // ---------------------------------------------------------------------

    set(modules: Module[]) {
        this._modules.next(modules);
    }
}


/*
Copymodule Google LLC. All Modules Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
