import {RightsService} from "../rights/rights.service";

export class Module
{
    "id"?: number;
    "module_name": string;
    "description"?: string;
    "created_at"?: string;
    "updated_at"?: string;

    constructor(module)
    {
        {
            this.id = module.id || undefined;
            this.module_name = module.module_name || undefined;
            this.description = module.description || undefined;
            this.created_at = module.created_at || undefined;
            this.updated_at = module.updated_at || undefined;

        }
    }
}
