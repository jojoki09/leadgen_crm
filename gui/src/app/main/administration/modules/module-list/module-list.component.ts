import { Component, OnInit, ViewChild, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog, MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { ModulesService } from '../modules.service';
import { Module } from '../module.model';
import { SelectionModel } from '@angular/cdk/collections';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { ModuleAddComponent } from '../module-add/module-add.component';
import { FormGroup } from '@angular/forms';
import {UtilitiesService} from "../../services/utilities.service";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModuleListComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;


  // --------------------------------------------------------
  enablePaginator: boolean = false;
  pageSize: number = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] =
      [
          'select',
          'module_name',
          'description',
          'created_at',
          'buttons'
      ];


  dataSource: MatTableDataSource<Module>;

  applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();

      if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
      }
  }

  sortData(sort: Sort) {
      const data = this.data.slice();
      if (!sort.active || sort.direction === '') {
          this.sortedData = data;
          return;
      }

      this.sortedData = data.sort((a, b) => {
          const isAsc = sort.direction === 'asc';
          switch (sort.active) {
              case 'module_name': return compare(a.module_name, b.module_name, isAsc);
              case 'description': return compare(a.description, b.description, isAsc);
              case 'created_at': return compare(a.created_at, b.created_at, isAsc);
              default: return 0;
          }
      });
  }
  
  // --------------------------------------------------------
  sortedData: Module[];
  checkboxes: {};
  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.data.forEach(row => this.selection.select(row));
  }
  // --------------------------------------------------------
  dialogRef: any;
  // --------------------------------------------------------

  data: any = [];


  constructor(
    public _modulesService: ModulesService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute:ActivatedRoute,
    public _matDialog: MatDialog,
    public _util: UtilitiesService,
  ) { 
    this._unsubscribeAll = new Subject();


    // initiate date table datasource && setting the sorted date
    this.dataSource = new MatTableDataSource([]);

    this._modulesService.modules.pipe(takeUntil(this._unsubscribeAll)).subscribe((modules: Module[]) => {
      this.data = modules  || [];

      // enable paginator in need
      this.enablePaginator = this.data.length > Math.min.apply(Math, this.pageSizeOptions) ? true : false;

      // initiate date table datasource && setting the sorted date
      this.dataSource = new MatTableDataSource(this.data);
      this.sortedData = this.data.slice();

      // prepair the table to be sortable
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      // empty checked modules
      this.checkboxes = {};
      // this.data.map(module => {
      //     this.checkboxes[module.id] = false;
      // });
    });
  }

  ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
  
  ngOnInit() {
    
    this._modulesService.breadcrumb.next({
        steps: [
            "Modules",
            "List"
        ],
        big_header: "liste des Modules."
    });

    this._modulesService.getModules().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {

        // waiting for data come & save the results into array
        this.data = data || [];

        // enable paginator in need
        this.enablePaginator = this.data.length > Math.min.apply(Math, this.pageSizeOptions) ? true : false;

        // initiate date table datasource && setting the sorted date
        this.dataSource = new MatTableDataSource(this.data);
        this.sortedData = this.data.slice();

        // prepair the table to be sortable
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        // empty checked modules
        this.checkboxes = {};
        // this.data.map(user => {
        //     this.checkboxes[user.id] = false;
        // });

    });
  }

  onDeleteModule(module_id: number) {
    /*
    * throw confirmation message
    * remove with success alert
    * */

    let id = module_id;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '30rem',
        data: {content: "Êtes-vous sûr de vouloir supprimer cet élément?", title: "Alerte de confirmation"}
    });

    dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
        if(result) {

            this._modulesService.deleteModule(id).pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
                if(res.ok){
                    this.snackBar.open(res.feedback, "ok", {
                        duration: 2500,
                        horizontalPosition: "end",
                        verticalPosition: "bottom",
                    });

                    this.ngOnInit();
                }
            })
        }
    });

  }

  onAddModuleClick() {
    this.dialogRef = this._matDialog.open(ModuleAddComponent, {
        panelClass: 'module-dialog-container',
        data      : {
            action: 'new'
        }
    });

    this.dialogRef.afterClosed()
        .pipe(takeUntil(this._unsubscribeAll)).subscribe((response: FormGroup) => {
          
            if ( !response )
            {
                return;
            }

            let formInformation: any = this._util.filterFormGroupValues(response);
            this._modulesService.addModule(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {

              if(_.ok) {
                this.snackBar.open(_.feedback, "ok", {
                  duration: 2500,
                  horizontalPosition: "end",
                  verticalPosition: "bottom",
                });

                this.ngOnInit();
                
              } else {
                this.snackBar.open(_.error, "ok", {
                  duration: 2500,
                  horizontalPosition: "end",
                  verticalPosition: "bottom",
                });
              }
            
            });
        });
    }


    onEditModuleClick(module: Module) {
      this.dialogRef = this._matDialog.open(ModuleAddComponent, {
          panelClass: 'module-dialog-container',
          data      : {
              module: module,
              action: 'edit'
          }
      });
  
      this.dialogRef.afterClosed()
          .pipe(takeUntil(this._unsubscribeAll)).subscribe((response: FormGroup) => {
            
              if ( !response )
              {
                  return;
              } 

              const actionType: string = response[0];
              const formData: FormGroup = response[1];

              switch ( actionType )
              {
                  /**
                   * Save
                   */
                  case 'save':
                    this._modulesService.updateModule(this._util.filterFormGroupValues(formData)).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
                          
                      if(_.ok) {
                        this.snackBar.open(_.feedback, "ok", {
                          duration: 2500,
                          horizontalPosition: "end",
                          verticalPosition: "bottom",
                        });
        
                        this.ngOnInit();
                        
                      } else {
                        this.snackBar.open(_.error, "ok", {
                          duration: 2500,
                          horizontalPosition: "end",
                          verticalPosition: "bottom",
                        });
                      }
                      
                    });
                      break;
                  /**
                   * Delete
                   */
                  case 'delete':
                      this.onDeleteModule(module.id);
                      break;
              }
  
              
          });
      }
}




function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}