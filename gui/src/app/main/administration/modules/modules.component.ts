import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ModulesService } from './modules.service';
import { BreadCrumb } from './modules.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.scss'],
  // animations   : fuseAnimations,
})
export class ModulesComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;
  constructor(private _modulesService: ModulesService, private _fuseSidebarService: FuseSidebarService) { 
    this._unsubscribeAll = new Subject();
  }

  breadcrumb: BreadCrumb;

  ngOnInit() {
    this._modulesService.breadcrumb.pipe(takeUntil(this._unsubscribeAll)).subscribe((breadcrumb: BreadCrumb) => {
        this.breadcrumb = breadcrumb;
    })
  }

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  toggleSidebar(name): void
  {
      this._fuseSidebarService.getSidebar(name).toggleOpen();
  }

}
