import { Component, OnInit, Inject } from '@angular/core';
import { Module } from '../module.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-module-add',
  templateUrl: './module-add.component.html',
  styleUrls: ['./module-add.component.scss']
})
export class ModuleAddComponent implements OnInit {
  
  action: string;
  module: Module;
  moduleForm: FormGroup;
  dialogTitle: string;

  constructor(
    public matDialogRef: MatDialogRef<ModuleAddComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder
  )
  {
    // Set the defaults
    this.action = _data.action;

    if ( this.action === 'edit' )
    {
        this.dialogTitle = 'Editer Le Module';
        this.module = _data.module;
    }
    else
    {
        this.dialogTitle = 'Nouveau Module';
        this.module = new Module({});
    }

    this.moduleForm = this.createModuleForm();
  }

  createModuleForm(): FormGroup
  {
      return this._formBuilder.group({
          id      : [this.module.id],
          module_name    : [this.module.module_name],
          description: [this.module.description],
      });
  }

  ngOnInit() {
    
  }

}
