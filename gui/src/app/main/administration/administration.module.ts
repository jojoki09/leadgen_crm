import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";

import {AdministrationComponent} from "./administration.component";
/* import { JwtInterceptor } from '../interceptors/jwt-interceptor';
import { UsersModule } from './users/users.module';
import { DepartementsModule } from './departements/departements.module';
import { RolesModule } from './roles/roles.module';
import { RightsModule } from './rights/rights.module';
import { ModulesModule } from './modules/modules.module'; */
        

const routes: Routes = [
    { path: '', component: AdministrationComponent,
        children: [
            { path: '', redirectTo: 'users' , pathMatch: "full"},
            { path: 'users', loadChildren:  './users/users.module#UsersModule'  /* () => UsersModule */},
            { path: 'departements', loadChildren:  './departements/departements.module#DepartementsModule'   /* () => DepartementsModule */},
            { path: 'roles', loadChildren:  './roles/roles.module#RolesModule'   /* () => RolesModule */},
            { path: 'rights', loadChildren:  './rights/rights.module#RightsModule'   /* () => RightsModule */},
            { path: 'modules', loadChildren:  './modules/modules.module#ModulesModule'  /* () => ModulesModule */},
        ]
    },
];

@NgModule({
  declarations: [
  AdministrationComponent, 
],
  imports: [
      CommonModule,
      RouterModule.forChild(routes),
  ],
  providers: [
    
  ],
})
export class AdministrationModule { } 
