import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideBarComponent } from './side-bar.component';
import { MaterialUiModule } from 'app/main/material-ui/material-ui.module';

@NgModule({
  declarations: [
    SideBarComponent,
  ],
  imports: [
    CommonModule,
    // load material design style
    MaterialUiModule,
  ],
  exports: [
    SideBarComponent,
  ]
})
export class SideBarModule { }
