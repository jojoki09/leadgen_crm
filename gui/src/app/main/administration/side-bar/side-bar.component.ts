import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, UrlSegment } from '@angular/router';
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  target: string = 'users';

  ShowPermissionsSection: boolean = false;
  ShowModulesSection: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute:ActivatedRoute,
    public _authService: AuthService
  ) { 
    
  }

  ngOnInit() {
    this.target = this.router.url.split("/").reverse()[1];
  }

  navigate(where: string, action) {
    action = action || 'list';

    switch (action) {
      case 'add':
        this.router.navigate([`../${where}/add`], { relativeTo: this.activatedRoute });
        break;
    
      default:
        this.router.navigate([`../${where}`], { relativeTo: this.activatedRoute });
        break;
    }

    
  }

}
