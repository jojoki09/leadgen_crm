export class Role
{
    "id"?: number;
    "role_name": string;
    "description"?: string;
    "created_at"?: string;
    "updated_at"?: string;

    constructor(role)
    {
        {
            this.id = role.id || undefined;
            this.role_name = role.role_name || undefined;
            this.description = role.description || undefined;
            this.created_at = role.created_at || undefined;
            this.updated_at = role.updated_at || undefined;

        }
    }
}
