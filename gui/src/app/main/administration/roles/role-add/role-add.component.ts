import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { RolesService } from '../roles.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Module } from '../../modules/module.model';
import { Right } from '../../rights/right.model';
import { RightsService } from '../../rights/rights.service';
import { ModulesService } from '../../modules/modules.service';
import { UtilitiesService } from '../../services/utilities.service';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { Role } from '../role.model';
import { SideBarComponent } from '../../side-bar/side-bar.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


export interface ModuleRights {
  module_id?: number,
  rights?: {key: number, value: 0 |1 }[]
  }
  

@Component({
  selector: 'app-role-add',
  templateUrl: './role-add.component.html',
  styleUrls: ['./role-add.component.scss']
})
export class RoleAddComponent implements OnInit, OnDestroy  {

  private _unsubscribeAll: Subject<any>;

  constructor(
    private _rolesService: RolesService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute:ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _rightsService: RightsService,
    private _moduleService: ModulesService,
    private _util: UtilitiesService,
  ) { 
    this._unsubscribeAll = new Subject();
    this.fg1 = this._formBuilder.group({ "role_name"    : ['', Validators.required], "description": [''], });
  }
  

  roleRights: Right[];

  fg1: FormGroup;
  modules: Module[] = [];
  rights: Right[] = [];
  module_rights: ModuleRights[] = [];
  roles: Role[] = [];
  action: string;
  role_id: number;
  selectedRole: Role;

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  ngOnInit() {

    
    this._rolesService.getRoles().pipe(takeUntil(this._unsubscribeAll)).subscribe((roles: Role[]) => {
      this.roles = roles;

      this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((params: any) => {
     
        this.action = params.do;
        this.role_id = params.role_id;

        if(this.role_id == 1)
          history.back();
  
        if(this.action == 'edit') {
  
          // Edit 
          this._rolesService.breadcrumb.next({
            steps: [
                "Roles",
                "Mettre à jour"
            ],
            big_header: "Editer un role"
          });
  
          // fetch for role with id role_id
          
          
          if(this.roles.length > 0 && this.roles.filter((role: Role) => role.id == this.role_id).length > 0) {
            let role: Role = this.roles.filter((role: Role) => role.id == this.role_id)[0];

            this.selectedRole = role;

            
            this.fg1.patchValue({
              role_name    : this.selectedRole.role_name,
              description  : this.selectedRole.description,
            })

            this.setupUpdate();
          } else {
            this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
            return;
          }
  
         
  
        } else {
  
          // Add 
          this._rolesService.breadcrumb.next({
            steps: [
                "Roles",
                "Ajouter"
            ],
            big_header: "L'ajout d'un nouveau Role"
          });
  
          

          this.setupAdd();
  
        }
  
        
      });

      




    })

    


    

    
    
  }




  setupAdd() {
    this._rightsService.getRights().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Right[]) => {
        this.rights = data || [];

        this._moduleService.getModules().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
            this.modules = data || [];

            this.modules.forEach((module: Module, i) => {
                let rights = [];
                this.getRightsByModule(module.id).forEach((right: Right, i) => {
                    rights.push({ key: right.id, value: 0});
                })

                this.module_rights.push({ module_id: module.id, rights: rights});

            })

        });
    });
  }

  setupUpdate() {
    this._rightsService.getRights().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Right[]) => {
        this.rights = data || [];

        this._moduleService.getModules().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
            this.modules = data || [];

            this.modules.forEach((module: Module, i) => {
                let rights = [];
                this.getRightsByModule(module.id).forEach((right: Right, i) => {
                    rights.push({ key: right.id, value: 0});
                })

                this.module_rights.push({ module_id: module.id, rights: rights});

                this.adjustAccessRightSectin();
            })

        });
    });
  }


  adjustAccessRightSectin() {
    this._rolesService.getRoleRights(this.role_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((rights: Right[]) => {
      this.roleRights = rights;
      
      this.module_rights.forEach((module_rights: ModuleRights) => {
        
        this.roleRights.forEach((ur: Right) => {
          
          
          if(module_rights.module_id == ur.module_id) {
            module_rights.rights.forEach((r: { key: number, value: number}) => {
              if(r.key == ur.id) {
                r.value = 1;
              }
            })
          }
        })
        
      })
    })
  }

  /**
   * MAIN ACTIONS
   */

  onAddRole(): void
  {
      var formInformation: any  = {};
      formInformation = this._util.filterFormGroupValues(this.fg1);
      

      /*  */

      

      

      this._rolesService.addRole(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
          if(_.ok) {


            formInformation = {};
            let role_id = _.role_id;

            
            let offered_rights: number[] = [];

            this.module_rights.forEach((el: ModuleRights) => {
                el.rights.forEach(right => {
                    if(right.value) {
                      offered_rights.push(right.key);
                    }
                })
            })

            formInformation["rights"] = offered_rights;
            formInformation["id"] = role_id;

            this._rolesService.refresh();

            this._rolesService.setRoleRights(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {

                if(_.ok) {
                  const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                      width: '30rem',
                      data: {content: _.feedback, title: "Creation un nouveau rôle et définir des permissions", noThanks: false}
                  });
          
                  dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                      if(result) {
                          this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
                      }
                  });
                }
              
            })

          } else {
              const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                  width: '30rem',
                  data: {content: _.error, title: "Creation un nouveau rôle et définir des permissions", noThanks: false}
              });
      
              dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                  if(result) {
                      
                  }
              });
          }
      });
      
  }

  onEditRole(): void
  {
      var formInformation: any  = {};
      formInformation = this._util.filterFormGroupValues(this.fg1, null);

      formInformation["id"] = this.role_id;

      this._rolesService.updateRole(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
          if(_.ok) {
            
            let offered_rights: number[] = [];

            this.module_rights.forEach((el: ModuleRights) => {
                el.rights.forEach(right => {
                    if(right.value) {
                      offered_rights.push(right.key);
                    }
                })
            })

            formInformation["rights"] = offered_rights;

            this._rolesService.refresh();

            this._rolesService.setRoleRights(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {

                if(_.ok) {
                  const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                      width: '30rem',
                      data: {content: _.feedback, title: "Edition d'un rôle et définir des permissions", noThanks: false}
                  });
          
                  dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                      if(result) {
                          this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
                      }
                  });
                }
              
            })


              /*  */

              

          } else {
              const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                  width: '30rem',
                  data: {content: _.error, title: "Creation un nouveau rôle et définir des permissions", noThanks: true}
              });
      
              dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                  if(result) {
                      
                  }
              });
          }
      });
      
  }

  /**
   * HELPERS
   */

  getModuleProperty(module_id, property) {
    return this.modules.filter((module: Module) => { return module.id == module_id })[0][`${property}`] || undefined;
  }

  getRightProperty(right_id, property) {
      return this.rights.filter((right: Right) => { return right.id == right_id })[0][`${property}`] || undefined;
  }

  getRightsByModule(module_id): Right[]{
    return this.rights.filter((right: Right) => { return right.module_id == module_id });
  }

  setRightTo(module_id, e: Event) {
      e.stopPropagation();
      let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
          return el.module_id === module_id;
      })[0];


      matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
          el.value = 1;
      });
  }

  unsetRightTo(module_id, e: Event) {
      e.stopPropagation();

      let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
          return el.module_id == module_id;
      })[0];


      matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
          el.value = 0;
      })

  }

  onCancelEdit() {
    this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
  }

}
