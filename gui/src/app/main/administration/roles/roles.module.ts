import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RolesComponent} from "./roles.component";
import {RouterModule, Routes} from "@angular/router";
import {RolesService} from "./roles.service";

import { FuseSharedModule } from '@fuse/shared.module';
import {FuseConfirmDialogModule, FuseDemoModule, FuseSidebarModule, FuseHighlightModule} from '@fuse/components';
import {ConfirmDialogModule} from "../confirm-dialog/confirm-dialog.module";


import { FlexLayoutModule } from '@angular/flex-layout';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleAddComponent } from './role-add/role-add.component';
import { SideBarModule } from '../side-bar/side-bar.module';
import { MaterialUiModule } from 'app/main/material-ui/material-ui.module';


const routes: Routes = [
    {
        path     : '',
        component: RolesComponent,
        children: [
            { path: '', redirectTo: 'list', pathMatch: 'full' },
            { path: 'list', component: RoleListComponent,},
            { path: 'add', component: RoleAddComponent,},
        ],
    }
];

@NgModule({
  declarations: [
    RoleListComponent,
    RolesComponent,
    RoleAddComponent,
  ],
  imports: [
    // import module routes
    RouterModule.forChild(routes),
    // load material design style
    MaterialUiModule,
    // flexlayout for repensive design 
    FlexLayoutModule,

    FuseSharedModule,
    FuseConfirmDialogModule,
    FuseSidebarModule,
    
    // the common dialog for confirmatioin alerts
    ConfirmDialogModule,
    // import side bar component
    SideBarModule,

  ]
})
export class RolesModule { }
