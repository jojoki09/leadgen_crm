import { Component, OnInit, OnDestroy } from '@angular/core';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ActivatedRoute, NavigationEnd, Router, RoutesRecognized} from "@angular/router";
import {PrintService} from "../services/print.service";
import { filter, map, takeUntil } from 'rxjs/operators';
import {BreadCrumb, RolesService} from "./roles.service";
import { Subject } from 'rxjs';
// import { map } from "rxjs/add/operator/map";



@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit, OnDestroy {

    breadcrumb: BreadCrumb;

    private _unsubscribeAll: Subject<any>;
        
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _rolesService: RolesService) 
    {
        this._unsubscribeAll = new Subject();            
    
    }

    ngOnInit() {

        this._rolesService.breadcrumb.pipe(takeUntil(this._unsubscribeAll)).subscribe((breadcrumb: BreadCrumb) => {
            this.breadcrumb = breadcrumb;
        })


    }

    ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}
