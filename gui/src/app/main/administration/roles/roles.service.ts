import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable, BehaviorSubject } from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

import { Role } from './role.model';
import { HttpErrorHandler, HandleError } from '../services/http-error-handler.service';
import {environment} from "../../../../environments/environment";
import { Right } from '../rights/right.model';
import { List } from 'lodash';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
    })
};

export interface BreadCrumb {
    steps?: string[],
    big_header?: string
}

@Injectable({
    providedIn: 'root'
})
export class RolesService {

    roles_list: Role[] = [];

    apiUrl = environment["API_URL"];  // URL to web api
    private handleError: HandleError;

    private _roles: BehaviorSubject<List<Role>> = new BehaviorSubject([]);
    public readonly roles: Observable<List<Role>> = this._roles.asObservable();

   
    public breadcrumb = new BehaviorSubject<BreadCrumb>({
        steps: [
            "Cupiditate dignissimos quo quia quam?",
            "quia repellat nisi maiores?"
        ],
        big_header: "adipisicing elit. Voluptatibus officiis culpa cupiditate at molestiae id minima dicta quaerat blanditiis facilis, eos"
    });

    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler
    ) {
            
        this.handleError = httpErrorHandler.createHandleError('RolesService');


        this.roles.subscribe((roles: any[]) => { this.roles_list = roles; });

        this.getRoles().subscribe((roles: Role[]) => {
            this._roles.next(roles);
            this.roles_list = roles;
        });
        
    }

    /** GET roles from the server */
    getRoles(): Observable<Role[]> {
        return this.http.get<Role[]>(`${this.apiUrl}roles`)
            .pipe(
                catchError(this.handleError('getRoles', [])),
                tap((roles: any[]) => this._roles.next(roles) )
            );
    }

    getRoleRights(role_id: number): Observable<Right[]> {
        return this.http.get<Right[]>(`${this.apiUrl}roles/${role_id}/rights`)
        .pipe(
            catchError(this.handleError('getRoleRights', []))
        );
    }

    /* GET roles whose name contains search term */
    searchRoles(term: string): Observable<Role[]> {
        term = term.trim();

        // Add safe, URL encoded search parameter if there is a search term
        const options = term ?
            { params: new HttpParams().set('name', term) } : {};

        return this.http.get<Role[]>(`${this.apiUrl}`, options)
            .pipe(
                catchError(this.handleError<Role[]>('searchRolees', []))
            );
    }

    //////// Save methods //////////

    /** POST: add a new role to the database */
    addRole(role: Role): Observable<Role> {
        return this.http.post<Role>(`${this.apiUrl}roles`, role, httpOptions)
            .pipe(
                catchError(this.handleError('addRole', role))
            );
    }

    /** DELETE: delete the role from the server */
    deleteRole(id: number): Observable<{}> {
        const url = `${this.apiUrl}roles/${id}/delete`; // DELETE api/rolees/42
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteRole'))
            );
    }

    /** PUT: update the role on the server. Returns the updated role upon success. */
    updateRole(role: Role): Observable<Role> {
        httpOptions.headers =
            httpOptions.headers.set('Authorization', 'my-new-auth-token');

        return this.http.put<Role>(`${this.apiUrl}roles`, role, httpOptions)
            .pipe(
                catchError(this.handleError('updateRole', role))
            );
    }

    setRoleRights(user: {id: number, rights: number[]}) {

        return this.http.post(`${this.apiUrl}roles/offer_rights`, user)
            .pipe(
                catchError(this.handleError('setRoleRights', user))
            );
    }

    // ---------------------------------------------------------------------

    set(roles: Role[]) {
        this._roles.next(roles);
    }

    getRoleProperty(role_id, property) {
        if(!role_id || !property)
          return null;
        else
        return this.roles_list.filter((role: Role) => { return role.id == role_id })[0][`${property}`] || undefined;
    }

    refresh() {
        this.getRoles().subscribe();
    }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/