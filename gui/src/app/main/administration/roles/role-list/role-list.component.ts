import {Component, OnInit, ViewChild, OnDestroy, ElementRef} from '@angular/core';

import {
    MatDialog,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    Sort,
    MatSnackBar, MatInput,
} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import {RolesService} from "../roles.service";
import {Role} from "../role.model";

import { ConfirmDialogComponent } from "../../confirm-dialog/confirm-dialog.component";
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'app/services/auth.service';
import {DatePipe} from "@angular/common";
import * as moment from 'moment';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnInit, OnDestroy {

    @ViewChild('fromInput', {
        read: MatInput
    }) fromInput: MatInput;

    @ViewChild('toInput', {
        read: MatInput
    }) toInput: MatInput;

    f = {
        "role_name": null,
        "description": null,
        "created_at": {
            "du": "",
            "a": "",
        },
    };


    // --------------------------------------------------------
    onRefreshing: boolean = false;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filter') filter: ElementRef;
    roles: any = [];

    pageSize: number = 25;
    totalSize = 0;
    pageSizeOptions: number[] = [25, 75, 100, 500];

    dataSource: any;
    enablePaginator: boolean = false;

    displayedColumns: string[] =
        [
            /* 'select', */
            'role_name',
            'description',
            'created_at',
            'buttons'
            ];

    sortData(sort: Sort) {
        const data = this.roles.slice();
        if (!sort.active || sort.direction === '') {
            this.roles = data;
            return;
        }

        this.roles = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'role_name': return compare(a.login, b.login, isAsc);
                case 'description': return compare(a.email, b.email, isAsc);
                case 'created_at': return compare(a.last_name, b.last_name, isAsc);
                default: return 0;
            }
        });
    }



    private _unsubscribeAll: Subject<any>;

    constructor(
        private _rolesService: RolesService,
        public _authService: AuthService,
        public dialog: MatDialog,
        public snackBar: MatSnackBar,
        private router: Router,
        private activatedRoute:ActivatedRoute,


    ) {

        this._unsubscribeAll = new Subject();

        this._rolesService.roles.pipe(takeUntil(this._unsubscribeAll)).subscribe((roles: any[]) => {
            if(roles) {
                this.dataSource = new MatTableDataSource<Element>(roles);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.roles = roles || [];
                this.totalSize = this.roles.length;
            }
        });

    }

    ngOnDestroy(): void
      {
          // Unsubscribe from all subscriptions
          this._unsubscribeAll.next();
          this._unsubscribeAll.complete();
      }

    ngOnInit() {

        this._rolesService.breadcrumb.next({
            steps: [
                "Roles",
                "List"
            ],
            big_header: "LISTE DES ROLES"
        });


    }




    onDeleteRole(row: any) {
    /*
    * throw confirmation message
    * remove with success alert
    * */

    let id = row.id;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '30rem',
        data: {content: "Êtes-vous sûr de vouloir supprimer cet élément?", title: "Alerte de confirmation"}
    });

    dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
        if(result) {

            this._rolesService.deleteRole(id).pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
                if(res.ok){
                    this.snackBar.open(res.feedback, "ok", {
                        duration: 2500,
                        horizontalPosition: "end",
                        verticalPosition: "bottom",
                    });

                    this.refresh();
                }
            })
        }
    });

}

  onAddRole() {
    this.router.navigate([`../add`], {relativeTo: this.activatedRoute});
  }


  onEditRole(role_id: number) {

    const queryParams = {
      role_id: role_id,
      do: "edit",
    }

    Object.keys(queryParams)
    .filter(key => queryParams[key] === null || queryParams[key] === undefined)
    .forEach(key => delete queryParams[key])

    this.router.navigate([`../add`], { queryParams, relativeTo: this.activatedRoute});
  }



    formatDate(date, pattern, behavior) {
        return moment(date, pattern).format(behavior);
    }


    refresh() {
        this._rolesService.refresh();
    }

    clear()
    {
        this.fromInput.value = "";
        this.toInput.value = "";
        this.f.created_at.a = "";
        this.f.created_at.du = "";
        this.super_filter();
    }

    super_filter() {

        let d = true;
        let filteredData = [];

        /*f = {
            "departement_name": null,
            "description": null,
            "total_users": null,
            "created_at": {
                "du": "",
                "a": "",
            },
        };*/


        if(this.f.role_name != null && this.f.role_name  != "") {
            let data = filteredData.length > 0 ? filteredData : this.roles;
            filteredData = data.filter(dept => dept.role_name.toLowerCase().indexOf(this.f.role_name.toLowerCase()) > -1);
            d = false;
        }
        if(this.f.description != null && this.f.description  != "") {
            let data = filteredData.length > 0 ? filteredData : this.roles;
            filteredData = data.filter(dept => dept.description.toLowerCase().indexOf(this.f.description.toLowerCase()) > -1);
            d = false;
        }


        if(this.f.created_at.du != null && this.f.created_at.a != null)
        {
            var created_atPipe = new DatePipe("en-US");
            this.f.created_at.du = created_atPipe.transform(this.f.created_at.du, 'yyyy-MM-dd');
            this.f.created_at.a= created_atPipe.transform(this.f.created_at.a, 'yyyy-MM-dd');
        }

        if(this.f.created_at.du != null && this.f.created_at.a != null)
        {
            let data = filteredData.length > 0 ? filteredData : this.roles;
            filteredData = data.filter(dept => dept.created_at >= this.f.created_at.du && dept.created_at <= this.f.created_at.a );
            d = false;
        }

        if(this.f.created_at.du !=null && this.f.created_at.a == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.roles;
            filteredData = data.filter(dept => dept.created_at >= this.f.created_at.du);
            d = false;
        }

        if(this.f.created_at.a !=null && this.f.created_at.du == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.roles;
            filteredData = data.filter(dept => dept.last_login <= this.f.created_at.a);
            d = false;
        }


        this.dataSource = new MatTableDataSource<Element>(d ? this.roles : filteredData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.totalSize = d ? this.roles.length : filteredData.length;

        return d ? this.roles : filteredData;
    }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}