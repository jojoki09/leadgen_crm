import { Component, OnInit, OnDestroy } from '@angular/core';
import { RightsService, BreadCrumb } from './rights.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-rights',
  templateUrl: './rights.component.html',
  styleUrls: ['./rights.component.scss']
})
export class RightsComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;

  constructor(private _rightsService: RightsService, private _fuseSidebarService: FuseSidebarService) { 
    this._unsubscribeAll = new Subject();
  }

  breadcrumb: BreadCrumb;

  ngOnInit() {
    this._rightsService.breadcrumb.pipe(takeUntil(this._unsubscribeAll)).subscribe((breadcrumb: BreadCrumb) => {
        this.breadcrumb = breadcrumb;
    })
  }

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  toggleSidebar(name): void
  {
      this._fuseSidebarService.getSidebar(name).toggleOpen();
  }

}
