export class Right
{
    "id"?: number;
    "module_id"?: number;
    "right_name": string;
    "description"?: string;
    "created_at"?: string;
    "updated_at"?: string;

    constructor(right)
    {
        {
            this.id = right.id || undefined;
            this.right_name = right.right_name || undefined;
            this.description = right.description || undefined;
            this.created_at = right.created_at || undefined;
            this.updated_at = right.updated_at || undefined;

        }
    }
}
