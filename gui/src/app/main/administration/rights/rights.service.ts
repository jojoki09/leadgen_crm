import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Right } from './right.model';
import { HttpErrorHandler, HandleError } from '../services/http-error-handler.service';
import {environment} from "../../../../environments/environment";
import { List } from 'lodash';


export interface BreadCrumb {
    steps?: string[],
    big_header?: string
}

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
    })
};

@Injectable({
    providedIn: 'root'
})
export class RightsService {
    apiUrl = environment["API_URL"];  // URL to web api
    private handleError: HandleError;

    private _rights: BehaviorSubject<List<Right>> = new BehaviorSubject([]);
    public readonly rights: Observable<List<Right>> = this._rights.asObservable();

    public breadcrumb = new BehaviorSubject<BreadCrumb>({
        steps: [
            "Cupiditate dignissimos quo quia quam?",
            "quia repellat nisi maiores?"
        ],
        big_header: "adipisicing elit. Voluptatibus officiis culpa cupiditate at molestiae id minima dicta quaerat blanditiis facilis, eos"
    });
    
    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler
    ) {
        
        this.handleError = httpErrorHandler.createHandleError('RightsService');

        this.getRights().subscribe((rights: Right[]) => {
            this._rights.next(rights);
        });
        
    }

    /** GET rights from the server */
    getRights(): Observable<Right[]> {
        return this.http.get<Right[]>(`${this.apiUrl}rights`)
            .pipe(
                catchError(this.handleError('getRights', []))
            );
    }

    /* GET rights whose name contains search term */
    searchRights(term: string): Observable<Right[]> {
        term = term.trim();

        // Add safe, URL encoded search parameter if there is a search term
        const options = term ?
            { params: new HttpParams().set('name', term) } : {};

        return this.http.get<Right[]>(`${this.apiUrl}`, options)
            .pipe(
                catchError(this.handleError<Right[]>('searchRightes', []))
            );
    }

    //////// Save methods //////////

    /** POST: add a new right to the database */
    addRight(right: Right): Observable<Right> {
        return this.http.post<Right>(`${this.apiUrl}rights`, right, httpOptions)
            .pipe(
                catchError(this.handleError('addRight', right))
            );
    }

    /** DELETE: delete the right from the server */
    deleteRight(id: number): Observable<{}> {
        const url = `${this.apiUrl}rights/${id}/delete`; // DELETE api/rights/42
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteRight'))
            );
    }

    /** PUT: update the right on the server. Returns the updated right upon success. */
    updateRight(right: any): Observable<Right> {
        httpOptions.headers =
            httpOptions.headers.set('Authorization', 'my-new-auth-token');

        return this.http.put<Right>(`${this.apiUrl}rights`, right, httpOptions)
            .pipe(
                catchError(this.handleError('updateRight', right))
            );
    }

    // ---------------------------------------------------------------------

    set(rights: Right[]) {
        this._rights.next(rights);
    }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/