import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RightsComponent } from './rights.component';
import { RightListComponent } from './right-list/right-list.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialUiModule } from 'app/main/material-ui/material-ui.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseSidebarModule, FuseDemoModule, FuseHighlightModule } from '@fuse/components';
import { ConfirmDialogModule } from '../confirm-dialog/confirm-dialog.module';
import { RightFormComponent } from './right-form/right-form.component';
import { SideBarModule } from '../side-bar/side-bar.module';

const routes: Routes = [
  {
      path     : '',
      component: RightsComponent,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: RightListComponent},
      ],
  }
];

@NgModule({
  declarations: [
    RightsComponent,
    RightListComponent,
    RightFormComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    MaterialUiModule,

    FlexLayoutModule,

    FuseSharedModule,
    FuseConfirmDialogModule,
    FuseSidebarModule,
    FuseDemoModule,
    FuseHighlightModule,
    
    ConfirmDialogModule,

    // import side bar component
    SideBarModule,
    
  ],
  entryComponents: [
    RightFormComponent,
  ]
})
export class RightsModule { }
