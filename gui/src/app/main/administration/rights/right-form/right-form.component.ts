import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Right } from '../right.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Module } from '../../modules/module.model';
import { ModulesService } from '../../modules/modules.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-right-form',
  templateUrl: './right-form.component.html',
  styleUrls: ['./right-form.component.scss']
})
export class RightFormComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;
  action: string;
  right: Right;
  rightForm: FormGroup;
  dialogTitle: string;
  modules: Module[] = [];

  constructor(
    public matDialogRef: MatDialogRef<RightFormComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
    private _moduleService: ModulesService,
  )
  {
    this._unsubscribeAll = new Subject();

    // Set the defaults
    this.action = _data.action;

    if ( this.action === 'edit' )
    {
        this.dialogTitle = 'Editer Le Module';
        this.right = _data.right;
    }
    else
    {
        this.dialogTitle = 'Nouveau Module';
        this.right = new Right({});
    }

    this.rightForm = this.createModuleForm();
  }

  createModuleForm(): FormGroup
  {
      return this._formBuilder.group({
          id      : [this.right.id],
          module_id    : [this.right.module_id],
          right_name    : [this.right.right_name],
          description: [this.right.description],
      });
  }

  ngOnInit() {
    this.getModules();
  }

  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  getModules() {
    this._moduleService.getModules().pipe(takeUntil(this._unsubscribeAll)).subscribe((modules: Module[]) => {
      this.modules = modules;
    });
  }

}
