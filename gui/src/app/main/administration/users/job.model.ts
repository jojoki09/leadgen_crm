export class Job
{
    "id"?: number;
    "job_title"?: string;
    "created_at"?: string;
    "updated_at"?: string;

    constructor(job)
    {
        {
            this.id = job.id || undefined;
            this.job_title = job.job_title || undefined;
            this.created_at = job.created_at || undefined;
            this.updated_at = job.updated_at || undefined;
        }
    }
}
