import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UsersComponent} from "./users.component";
import {RouterModule, Routes} from "@angular/router";
import {UsersService} from "./users.service";


import { FuseSharedModule } from '@fuse/shared.module';
import {FuseConfirmDialogModule, FuseDemoModule, FuseSidebarModule, FuseHighlightModule} from '@fuse/components';
import {UserEditComponent} from "./user-edit/user-edit.component";
import {UserShowComponent} from "./user-show/user-show.component";
import {UserAddComponent} from "./user-add/user-add.component";
import { UserListComponent} from "./user-list/user-list.component";
import {ConfirmDialogModule} from "../confirm-dialog/confirm-dialog.module";

import {MaterialUiModule} from "../../material-ui/material-ui.module";
import { FlexLayoutModule } from '@angular/flex-layout';
import { SideBarModule } from '../side-bar/side-bar.module';
import { EditPasswordFormComponent } from './edit-password-form/edit-password-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


const routes: Routes = [
    {
        path     : '',
        component: UsersComponent,
        children: [
            { path: '', redirectTo: 'list', pathMatch: 'full'},
            { path: 'list', component: UserListComponent,},
            { path: 'add', component: UserAddComponent,},
            { path: ':id/edit', component: UserEditComponent,},
        ],
    },
    {
        path     : ':id',
        component: UserShowComponent,
    }
];

@NgModule({
    declarations: [
        
        UsersComponent,
        UserAddComponent,
        UserEditComponent,
        UserShowComponent,
        UserListComponent,
        EditPasswordFormComponent,
    ],
    imports          : [
        // import module routes
        RouterModule.forChild(routes),
        // load material design style
        MaterialUiModule,
        // flexlayout for repensive design 
        FlexLayoutModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
        FuseDemoModule,
        FuseHighlightModule,
        
        // the common dialog for confirmatioin alerts
        ConfirmDialogModule,

        // import side bar component
        SideBarModule,
    ],
    providers        : [
        UsersService,
    ],
    entryComponents   : [
        EditPasswordFormComponent,
    ]
})
export class UsersModule { }
