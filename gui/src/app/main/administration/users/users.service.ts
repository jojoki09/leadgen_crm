import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable, BehaviorSubject } from 'rxjs';
import {catchError, filter, map, tap} from 'rxjs/operators';

import { User } from './user.model';
import { Job } from './job.model';
import { HttpErrorHandler, HandleError } from '../services/http-error-handler.service';
import {environment} from "../../../../environments/environment";
import { Right } from '../rights/right.model';
import { List } from 'lodash';

export interface BreadCrumb {
    steps?: string[],
    big_header?: string,
    goBackBtn?: boolean,
}

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    apiUrl = environment["API_URL"];  // URL to web api
    private handleError: HandleError;

    private _users: BehaviorSubject<List<User>> = new BehaviorSubject([]);
    public readonly users: Observable<List<User>> = this._users.asObservable();
    
    private _jobs: BehaviorSubject<List<Job>> = new BehaviorSubject([]);
    public readonly jobs: Observable<List<Job>> = this._jobs.asObservable();

    users_list: User[] = [];
    jobs_list: Job[] = [];


   public breadcrumb = new BehaviorSubject<BreadCrumb>({
        steps: [
            null,
            null,
        ],
        big_header: null,
        goBackBtn: false
    });


    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler,
    ) {

        this.handleError = httpErrorHandler.createHandleError('UsersService');

        this.users.subscribe((users: User[]) => { this.users_list = users; });
        this.jobs.subscribe((jobs: Job[]) => { this.jobs_list = jobs; });


        this.getUsers().subscribe((users: User[]) => { this._users.next(users); });
        this.getJobs().subscribe((jobs: Job[]) => { this._jobs.next(jobs); });
    }

    /** GET useres from the server */
    getUsers(): Observable<any> {
        return this.http.get(`${this.apiUrl}users`)
            .pipe(
                catchError(this.handleError('getUsers', [])),
                tap((users: any[]) => this._users.next(users) )
            );


    }

    getJobs(): Observable<Job[]> {
        return this.http.get<Job[]>(`${this.apiUrl}jobs`)
            .pipe(
                catchError(this.handleError('getJobs', [])),
                tap((jobs: any[]) => this._jobs.next(jobs) )
            );
    }

    getUserAvatar(user_id: number) {

        return this.http.get<any>(`${this.apiUrl}users/${user_id}/avatar`)
            .pipe(
                catchError(this.handleError('getUsers', []))
            );
    }

    getUserRights(user_id: number): Observable<Right[]> {
        return this.http.get<Right[]>(`${this.apiUrl}users/${user_id}/rights`)
        .pipe(
            catchError(this.handleError('getUserRights', []))
        );
    }

    getUserDeptRights(user_id: number): Observable<Right[]> {
        return this.http.get<Right[]>(`${this.apiUrl}users/${user_id}/group/rights`)
        .pipe(
            catchError(this.handleError('getUserRights', []))
        );
    }

    getUserRoleRights(user_id: number): Observable<Right[]> {
        return this.http.get<Right[]>(`${this.apiUrl}users/${user_id}/role/rights`)
        .pipe(
            catchError(this.handleError('getUserRoleRights', []))
        );
    }

    /* GET useres whose name contains search term */
    searchUsers(term: string): Observable<User[]> {
        term = term.trim();

        // Add safe, URL encoded search parameter if there is a search term
        const options = term ?
            { params: new HttpParams().set('name', term) } : {};

        return this.http.get<User[]>(`${this.apiUrl}`, options)
            .pipe(
                catchError(this.handleError<User[]>('searchUseres', []))
            );
    }

    //////// Save methods //////////

    /** POST: add a new user to the database */
    addUser(user: any): Observable<any> {

        return this.http.post<any>(`${this.apiUrl}auth/register`, user, {
            headers: new HttpHeaders({
                // 'Content-Type': 'multipart/form-data',
            })
            })
            .pipe(
                catchError(this.handleError('addUser', user))
            );
    }

    /** DELETE: delete the user from the server */
    deleteUser(id: number): Observable<{}> {
        const url = `${this.apiUrl}users/${id}/delete`; // DELETE api/usres/1
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteUser'))
            );
    }

    deleteAvatar(user_id: number): Observable<{}> {
        const url = `${this.apiUrl}users/${user_id}/avatar`; // DELETE api/users/1/avatar
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteUserAvatar'))
            );
    }

    /** PUT: update the user on the server. Returns the updated user upon success. */
    updateUser(user: User): Observable<User> {

        return this.http.post<User>(`${this.apiUrl}users/update`, user)
            .pipe(
                catchError(this.handleError('updateUser', user))
            );
    }

    setUserRights(user: {id: number, rights: number[]}) {

        return this.http.post<User>(`${this.apiUrl}users/offer_rights`, user)
            .pipe(
                catchError(this.handleError('setUserRights', user))
            );
    }

    editPassword(data: { current: string, new: string }) {
        return this.http.post<any>(`${this.apiUrl}users/edit_password`, data)
            .pipe( catchError(this.handleError('editPassword', data)) );
    }


    // ---------------------------------------------------------------------

    set(users: User[]) {
        this._users.next(users);
    }

    isMySuperior(child_id: number, parent_id: number) {
        return this.users.pipe(
            map((users: User[]) => {
                    const child = users.find((user: User) => user.id == child_id);
                    if(child)
                        return child.superior_id == parent_id;
                    else
                        return false;
                }
            ));
    }

    getUserProperty(user_id: number, property: string) {
        if(!user_id || !property)
            return;
        if(this.users_list.length < 1)
            return
        return this.users_list.length > 0 ? this.users_list.filter((user: User) => { return user.id == user_id })[0][`${property}`] || undefined : undefined;
    }

    getUserJob(user_id: number) {
        let user = this.getUserById(user_id);
        if(!user)
            return;

        return this.getJobById(user.job_id);
    }

    getUserById(user_id: number) {
        let expectedUsers = this.users_list.filter((user: User) => user.id === user_id);

        if(!expectedUsers.length)
            return;
        return expectedUsers[0];
    }

    getJobById(job_id: number) {
        let expectedJobs = this.jobs_list.filter((job: Job) => job.id === job_id);
        if(!expectedJobs.length)
            return;
        return expectedJobs[0];
    }

    getInstallateurs() {
        let installateurs = [];

        this.users_list.forEach((user: User) => {
            let job = this.getJobById(user.job_id);
            if(job) {
                if(job.job_title == "installateur")
                    installateurs.push(user);
            }
        });

        return installateurs;
    }



    getUserChildrens(user_id: number, job_title: string = "all"): Observable<User[]> {
        return this.http.get<User[]>(`${this.apiUrl}getUserChildrens/${user_id}/${job_title}`);
    }

    refresh() {
        this.getUsers().subscribe();
    }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/