import { FuseUtils } from '@fuse/utils';

export class User
{
    "id"?: number;
    "rights_depending"?:  number;
    "departement_id"?: number;
    "superior_id"?: number;
    "role_id": number;
    "job_id"?: number;
    "gender"?: 0 | 1;
    "login": string;
    "email": string;
    "password": string;
    "first_name"?: string;
    "last_name"?: string;
    "birth_day"?: string;
    "phone_number"?: string;
    "cin"?: string;
    "address"?: string;
    "photo"?: string;
    "signature"?: string;
    "function"? : string;
    "disabled"?: 0 | 1;
    "is_verified"?: 0 | 1;
    "created_at"?: string;
    "updated_at"?: string;
    "last_login"?: string;
    constructor(user)
    {
        {
            this.id = user.id || undefined;
            this.rights_depending = user.rights_depending || undefined;
            this.departement_id = user.departement_id || undefined;
            this.superior_id = user.superior_id || undefined;
            this.role_id = user.role_id || undefined;
            this.gender = user.gender || 0;
            this.login = user.login || undefined;
            this.email = user.email || undefined;
            this.password = user.password || undefined;
            this.first_name = user.first_name || undefined;
            this.last_name = user.last_name || undefined;
            this.birth_day = user.birth_day || undefined;
            this.phone_number = user.phone_number || undefined;
            this.cin = user.cin || undefined;
            this.address = user.address || undefined;
            this.photo = user.photo || 'assets/images/avatars/profile.jpg';
            this.signature = user.signature || undefined;
            this.function = user.function || undefined;
            this.disabled = user.disabled || undefined;
            this.is_verified = user.is_verified || undefined;
            this.created_at = user.created_at || undefined;
            this.updated_at = user.updated_at || undefined;
            this.last_login = user.last_login || undefined;
        }
    }
}
