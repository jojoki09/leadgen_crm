import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RightsService } from '../../rights/rights.service';
import { ModulesService } from '../../modules/modules.service';
import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { RolesService } from '../../roles/roles.service';
import { UsersService } from '../users.service';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { Module } from '../../modules/module.model';
import { Right } from '../../rights/right.model';
import { Role } from '../../roles/role.model';
import { User } from '../user.model';
import { ModuleRights } from '../user-add/user-add.component';
import * as moment from 'moment';
import * as $ from 'jquery';
import { DepartementsService } from '../../departements/departements.service';
import { Departement } from '../../departements/departement.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UtilitiesService } from '../../services/utilities.service';
import { Job } from '../job.model';
import { AuthService } from 'app/services/auth.service';
import { EditPasswordFormComponent } from '../edit-password-form/edit-password-form.component';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-user-show',
  templateUrl: './user-show.component.html',
  styleUrls: ['./user-show.component.scss'],
  /* encapsulation: ViewEncapsulation.None */
})
export class UserShowComponent implements OnInit {

  constructor(
    public _authService: AuthService,
    private _formBuilder: FormBuilder,
    private  _usersService: UsersService,
    private  _groupsService: DepartementsService,
    private  _rolesService: RolesService,
    private _rightsService: RightsService,
    private _moduleService: ModulesService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private activatedRoute:ActivatedRoute,
    private _util: UtilitiesService,
    public _matDialog: MatDialog,
    private logger: NGXLogger,
    )
    {
      this.fg1 = this.createForms().fg1;
      this.fg2 = this.createForms().fg2;
      this.fg3 = this.createForms().fg3;
      this.fg4 = this.createForms().fg4;

      // Set the private defaults
      this._unsubscribeAll = new Subject();
    }

  // Private
  private _unsubscribeAll: Subject<any>;




   /*
    who can be = "i'm + super admin"
    who can be = "not me  + superior"
    who can be = "i'm simple user"
    who can be = "other"
  */

  hideEditPermissions: boolean = true;
  showEnableAndDesibaleAccount: boolean = false;
  allowEditRole: boolean = false;
  allowEditDepartement: boolean = false;
  disableEditMode: boolean = false;

  /* profile_permission: any = {
    me: {
      "super admin": {
        hideEditPermissions: true,
        showEnableAndDesibaleAccount: false,
        allowEditRole: false,
        allowEditDepartement: false,
      },
    },

    "not-me" : {
      "superior": {
        hideEditPermissions: false,
        showEnableAndDesibaleAccount: true,
        allowEditRole: true,
        allowEditDepartement: true,
      },
      "other": {
        disableEditMode: true
      },
    }
  }; */





  readyComponent: boolean = false;

  allUserRoleRights: Right[];
  allUserGroupRights: Right[];

  editMode: boolean = false;
  user_id: any;

  noAvatar: boolean = true;

  photo: string;

  dialogRef: any;
  confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;

  modules: Module[] = [];
  rights: Right[] = [];
  roles: Role[] = [];
  users: User[] = [];
  departements: Departement[] = [];
  jobs: Job[]  = [];

  userRights: Right[] = [];
  userDeptRights: Right[] = [];
  userRoleRights: Right[] = [];

  selectedUser: User = {
    role_id: null,
    login: null,
    email: null,
    password: null
  };

  fg1: FormGroup;
  enableEditfg1: boolean = false;
  fg2: FormGroup;
  enableEditfg2: boolean = false;
  fg3: FormGroup;
  enableEditfg3: boolean = false;
  fg4: FormGroup;
  enableEditfg4: boolean = false;
  fg5: FormGroup;
  showEditRightsDependingOnInput: boolean = false;

  module_rights: ModuleRights[] = [];

  /**
   * On destroy
   */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  ngOnInit() {



    this.activatedRoute.params
      .pipe(takeUntil(this._unsubscribeAll)).subscribe(params => {
        this.user_id = params['id'];




        if(this.user_id == this._authService.getAuthenticatedUserId()) {


          /*
            IT'S ME
          */

          this.hideEditPermissions = true;
          this.showEnableAndDesibaleAccount = false;
          this.allowEditRole = false;
          this.allowEditDepartement = false;

          this._authService.authenticatedUser.subscribe((user: User) => {
            if(user.rights_depending == 1 && user.role_id == 1) {
              this.allowEditDepartement = true;
            }
          });


        } else {

          /*
            IT'S NOT ME
          */

          this._authService.authenticatedUser.subscribe((user: User) => {

            if(user.rights_depending == 1 && user.role_id == 1) {
              this.logger.info(`Play As: Super Admin`, `"${user.first_name} ${user.last_name}"`);
              this.hideEditPermissions = false;
              this.showEnableAndDesibaleAccount = true;
              this.allowEditRole = true;
              this.allowEditDepartement = true;
            } else {
              this._usersService.isMySuperior(this.user_id, user.id).subscribe(isMySuperior => {
                this.logger.debug(`isMySuperior(${this.user_id}, ${user.id})`, isMySuperior);
                if(isMySuperior) {
                  this.logger.info(`Play As: My Superior`, `"${user.first_name} ${user.last_name}"`);
                  this.hideEditPermissions = false;
                  this.showEnableAndDesibaleAccount = true;
                  this.allowEditRole = true;
                  this.allowEditDepartement = true;
                } else {
                  this.logger.info(`Play As: Other`, `"${user.first_name} ${user.last_name}"`);
                  this.disableEditMode = true;
                  this.hideEditPermissions = true;
                  this.showEnableAndDesibaleAccount = false;
                  this.allowEditRole = false;
                  this.allowEditDepartement = false;
                }
              });
            }

          });

        }




      // bring the user data
      this.aboutUser();
    });

    this._groupsService.getDepartements().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Departement[]) => {
        this.departements = data || [];
    });

    this._rolesService.roles.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Role[]) => {
      if(data && data.length > 0)
          this.roles =  data.filter(role => role.id != 1 || role.role_name != 'super admin' );
    });


    this._rightsService.getRights().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Right[]) => {
        this.rights = data || [];


        this._moduleService.getModules().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
            this.modules = data || [];

            this.resetModuleRightsArray();

        });
    });


    this._usersService.getJobs().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Job[]) => {
        this.jobs = data || [];
    });








  }

  resetModuleRightsArray() {

    this.module_rights = [];
    /* ------------------------------------------------------------------------------- */

    this._moduleService.modules.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
      this.modules = data || [];

      let module_rights  = [];

      /* get modules */
      const myRights: Right[] = this._authService.myRights();
      let modulesIds  = [];
      myRights.forEach(right => { if(modulesIds.indexOf(right.module_id) < 0) { modulesIds.push(right.module_id); } });


      for (let i = 0; i < modulesIds.length; i++) {
          const module_id = modulesIds[i];

          let rights_for_current = [];

          /* get rights for selected module */
          let rights = myRights.filter(right => right.module_id == module_id);
          if (rights.length > 0) {
              rights.forEach(right => {
                  rights_for_current.push({key: right.id, value: 0});
              })
          } else
              continue;

          /* format the matrix */
          module_rights.push({ module_id, rights: rights_for_current})
      }

      this.module_rights = module_rights;


      this.logger.trace('module_rights_matrix', module_rights);
    });

    /* ------------------------------------------------------------------------------- */

      this.adjustAccessRightSectin();

  }

  adjustAccessRightSectin() {
    this._usersService.getUserRights(this.user_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((rights: Right[]) => {
      this.userRights = rights;

      this.module_rights.forEach((module_rights: ModuleRights) => {

        this.userRights.forEach((ur: Right) => {


          if(module_rights.module_id == ur.module_id) {
            module_rights.rights.forEach((r: { key: number, value: number}) => {
              if(r.key == ur.id) {
                r.value = 1;
              }
            })
          }
        })

      })
    })
  }

  createForms() {

    return {
      fg1: this._formBuilder.group({
        first_name: [""],
        cin: ["", Validators.required],
        last_name: ["", Validators.required],
        birth_day: [""],
        gender: ["", Validators.required],
      }),

      fg2: this._formBuilder.group({
        email: [""],
        phone_number: ["", Validators.required],
        address: ["", Validators.required],
      }),

      fg3: this._formBuilder.group({
        job_id: [""],
        departement_id: ["", Validators.required],
        superior_id: [""],
        role_id: [""],
      }),

      fg4: this._formBuilder.group({
        login: ["", Validators.required],
        email: ["", Validators.required],
      }),

      fg5: this._formBuilder.group({
        rights_depending: ["", Validators.required],
      })
    }



  }


  fillForms() {
    this.fg1.patchValue({
      first_name: this.selectedUser.first_name,
      cin: this.selectedUser.cin,
      last_name: this.selectedUser.last_name,
      birth_day: this.selectedUser.birth_day,
      gender: this.selectedUser.gender,
    });

    this.fg2.patchValue({
      email: this.selectedUser.email,
      phone_number: this.selectedUser.phone_number,
      address: this.selectedUser.address,
    });

    this.fg3.patchValue({
      job_id: this.selectedUser.job_id,
      departement_id: this.selectedUser.departement_id,
      superior_id: this.selectedUser.superior_id,
      role_id: this.selectedUser.role_id
    });

    this.fg4.patchValue({
      login: this.selectedUser.login,
      email: this.selectedUser.email,
    });

    this.fg5.patchValue({
      rights_depending: this.selectedUser.rights_depending,
    });
  }

  edit_rights_depending(value: any) {
    var formInformation: any = this._util.filterFormGroupValues(this.fg5);

    // inject user_id to
    formInformation["id"] = this.selectedUser.id;





    this._usersService.updateUser(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
      this._usersService.refresh();
      if(_.ok) {
        this.snackBar.open(_.feedback, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });

        this.getSelectedUser();
        this.showEditRightsDependingOnInput  = true;

      } else {
        this.snackBar.open(_.error, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });
      }
    });
  }

  onSubmitfg1() {


    var formInformation = Object.assign({}, this.fg1.value);

    // fixing format for post
    if(moment.isMoment(this.fg1.controls["birth_day"].value)) {
      // let birth_day: any = this.fg1.controls["birth_day"].value.toISOString().slice(0,10);
      let birth_day = this.fg1.controls["birth_day"].value.format('YYYY-MM-DD');
      formInformation["birth_day"] = birth_day;
    }


        // inject user_id to
        formInformation["id"] = this.user_id;

        for(let prop in formInformation) {
            if(formInformation[prop] === "") {
                delete formInformation[prop];
                continue;
            }
        }

        this._usersService.updateUser(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
          this._usersService.refresh();
            if(_.ok) {
              this.snackBar.open(_.feedback, "ok", {
                duration: 2500,
                horizontalPosition: "end",
                verticalPosition: "bottom",
              });

              this._authService.getAuthenticatedUser().subscribe();

              this.getSelectedUser();
              this.enableEditfg1 = false;

            } else {
              this.snackBar.open(_.error, "ok", {
                duration: 2500,
                horizontalPosition: "end",
                verticalPosition: "bottom",
              });
            }
        });
  }

  onSubmitfg2() {

    var formInformation = Object.assign({}, this.fg2.value);

    // inject user_id to
    formInformation["id"] = this.user_id;

    for(let prop in formInformation) {
        if(formInformation[prop] === "") {
            delete formInformation[prop];
            continue;
        }
    }

    this._usersService.updateUser(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
      this._usersService.refresh();
      if(_.ok) {
        this.snackBar.open(_.feedback, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });

        this._authService.getAuthenticatedUser();
        this.getSelectedUser();
        this.enableEditfg2 = false;

      } else {
        this.snackBar.open(_.error, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });
      }
    });
  }

  onSubmitfg3() {

    var formInformation = Object.assign({}, this.fg3.value);

    // inject user_id to
    formInformation["id"] = this.user_id;

    for(let prop in formInformation) {
        if(formInformation[prop] === "") {
            delete formInformation[prop];
            continue;
        }
    }

    this._usersService.updateUser(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
      this._usersService.refresh();
      if(_.ok) {
        this.snackBar.open(_.feedback, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });

        this._authService.getAuthenticatedUser();
        this.getSelectedUser();
        this.enableEditfg3 = false;

      } else {
        this.snackBar.open(_.error, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });
      }
    });
  }

  onSubmitfg4() {

    var formInformation = Object.assign({}, this.fg4.value);

    // inject user_id to
    formInformation["id"] = this.user_id;

    for(let prop in formInformation) {
        if(formInformation[prop] === "") {
            delete formInformation[prop];
            continue;
        }
    }

    this._usersService.updateUser(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
      this._usersService.refresh();
      if(_.ok) {
        this.snackBar.open(_.feedback, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });

        this._authService.getAuthenticatedUser();
        this.getSelectedUser();
        this.enableEditfg4 = false;

      } else {
        this.snackBar.open(_.error, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });
      }
    });
  }

  ondisableAccount(status: 0 | 1) {
    var formInformation: any = {};

    // inject user_id to
    formInformation["id"] = this.user_id;
    formInformation["disabled"] = status;


    this._usersService.updateUser(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
      this._usersService.refresh();
      if(_.ok) {
        this.snackBar.open(_.feedback, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });

        this.getSelectedUser();

      } else {
        this.snackBar.open(_.error, "ok", {
          duration: 2500,
          horizontalPosition: "end",
          verticalPosition: "bottom",
        });
      }
    });

  }

  onactiveModeEdition() {

    this.editMode = !this.editMode;

      this.enableEditfg1 = !this.enableEditfg1;
      this.enableEditfg2 = !this.enableEditfg2;
      this.enableEditfg3 = !this.enableEditfg3;
      this.enableEditfg4 = !this.enableEditfg4;


  }

  getSelectedUser() {

      this._usersService.getUsers().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: User[]) => {
        this.users = data || [];
        this.selectedUser = Object.assign({}, this.users.filter((user: User) => user.id == this.user_id)[0]);

        // from here we want load the page
        this.readyComponent = true;

        this._rolesService.getRoleRights(this.selectedUser.role_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((rights: Right[]) => {
          this.allUserRoleRights = rights;
        })

        this._groupsService.getDepartementRights(this.selectedUser.departement_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((rights: Right[]) => {
          this.allUserGroupRights = rights;
        })

        // building the forms controls
        this.fg1 = this.createForms().fg1;
        this.fg2 = this.createForms().fg2;
        this.fg3 = this.createForms().fg3;
        this.fg4 = this.createForms().fg4;
        this.fg5 = this.createForms().fg5;

        this.fillForms();
    });


    this._usersService.getUserAvatar(this.user_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((data: any) => {



      if(data.ok) {
        this.photo = data.imageData;
        this.noAvatar = false;
      } else {
        this.noAvatar = true;
      }

    })

  }

  deleteAvatar() {


        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '30rem',
            data: {content: "Êtes-vous sûr de vouloir supprimer l'avatar?", title: "Alerte de confirmation"}
        });

        dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
            if(result) {
                this._usersService.deleteAvatar(this.user_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
                    if(res.ok){
                        this.snackBar.open(res.feedback, "ok", {
                            duration: 2500,
                            horizontalPosition: "end",
                            verticalPosition: "bottom",
                        });

                        /* update the photo don't forget comparing id if it's the connected */
                        this._authService.getAuthenticatedUser().subscribe();


                        this.getSelectedUser();

                    }
                })
            }
        });
  }

  triggerFinder() {
    $("input#photo").trigger("click");
  }

  handleFileInput(fileList: FileList) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
          width: '30rem',
          data: {content: "êtes-vous sûr de vouloir remplacer cette photo de profil?", title: "Alerte de confirmation"}
      });

      dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
          if(result) {

            // prepare formdata
            let formData: any = new FormData();
            let photo: any = document.querySelector('#photo');

            let file: File;

            if (fileList.length > 0) {
              file = fileList[0];
              formData.append('photo', file, file.name);
            }

            // inject user_id to
            formData.append('id', this.user_id);

            // do put request action
            this._usersService.updateUser(formData).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
              if(_.ok) {
                this.snackBar.open(_.feedback, "ok", {
                  duration: 2500,
                  horizontalPosition: "end",
                  verticalPosition: "bottom",
                });

                this.getSelectedUser();

                /* update the photo don't forget comparing id if it's the connected */
                this._authService.authenticatedUser.subscribe((authenticatedUser: User) => {
                  if (authenticatedUser.id = this.selectedUser.id) {

                    this._util.getBase64(file).then((imageData: any) => {
                      authenticatedUser.photo = imageData;
                      this._authService.setUser(authenticatedUser);
                    })

                  }
                })

              } else {
                this.snackBar.open(_.error, "ok", {
                  duration: 2500,
                  horizontalPosition: "end",
                  verticalPosition: "bottom",
                });
              }
            });

          }
      });
    }

  aboutUser() {
    this.getSelectedUser();


    // search for use rights



    this._usersService.getUserDeptRights(this.user_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((rights: Right[]) => {
      this.userDeptRights = rights;
    })

    this._usersService.getUserRoleRights(this.user_id).pipe(takeUntil(this._unsubscribeAll)).subscribe((rights: Right[]) => {
      this.userRoleRights = rights;
    })


  }

  onUpdateRules() {

    var formInformation: any = {};

    // fixing format for post
    formInformation["id"] = this.user_id;

    let rights: number[] = [];

    this.module_rights.forEach((el: ModuleRights) => {
        el.rights.forEach(right => {
            if(right.value) {
                rights.push(right.key);
            }
        })
    })

    formInformation["rights"] = rights;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '30rem',
      data: {content: "êtes-vous sûr de vouloir redéfinir les droit d'accès", title: "Définition les droits", noThanks: false}
    });

    dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
      if(result) {
          this._usersService.setUserRights(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
            if(_.ok) {

                this.snackBar.open(_.feedback, "ok", {
                  duration: 2500,
                  horizontalPosition: "end",
                  verticalPosition: "bottom",
                });

            }

            this.resetModuleRightsArray();
        });
      }
  });




  }

  getRightsByModule(module_id): Right[]{
      return this.rights.filter((right: Right) => { return right.module_id == module_id });
  }

  getModuleByRight(right_id): Module {
      let right: Right = this.rights.filter((right: Right) => { return right.id == right_id })[0];
      return this.modules.filter((module: Module) => { return module.id == right.module_id })[0];
  }

  /**
   * SET TO RULES METHODS
   */

  setRightTo(module_id, e: Event) {
      e.stopPropagation();
      let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
          return el.module_id === module_id;
      })[0];


      matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
          el.value = 1;
      });
  }

  unsetRightTo(module_id, e: Event) {
      e.stopPropagation();

      let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
          return el.module_id == module_id;
      })[0];


      matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
          el.value = 0;
      })

  }

  /**
   * GET CLASS PROPERTIES VALUES MOTHODS
   */

  getUserProperty(user_id, property) {
    if(!user_id || !property)
      return null;
    else {
      let user = this.users.filter((user: User) => { return user.id == user_id })[0]
      if(!user)
        return null;
      return user[`${property}`] || null;
    }

  }
  getRoleProperty(role_id, property) {

    console.log("son role: " + role_id);
    if(!role_id || !property)
      return null;
    else {
      let role = this.roles.filter((role: Role) => { return role.id == role_id })[0]
      if(!role)
        return null;
      return role[`${property}`] || null;
    }

  }

  getDepartementProperty(departement_id, property) {
    if(!departement_id || !property)
      return null;
    else {
      let departement = this.departements.filter((departement: Departement) => { return departement.id == departement_id })[0]
      if(!departement)
        return null;
      return departement[`${property}`] || null;
    }

  }

  getModuleProperty(module_id, property) {
    if(!module_id || !property)
      return null;
    else {
      let module = this.modules.filter((module: Module) => { return module.id == module_id })[0]
      if(!module)
        return null;
      return module[`${property}`] || null;
    }

  }

  getRightProperty(right_id, property) {
    if(!right_id || !property)
      return null;
    else {
      let right = this.rights.filter((right: Right) => { return right.id == right_id })[0]
      if(!right)
        return null;
      return right[`${property}`] || null;
    }

  }

  getJobProperty(job_id, property) {
    if(!job_id || !property)
      return null;
    else {
      let job = this.jobs.filter((job: Job) => { return job.id == job_id })[0]
      if(!job)
        return null;
      return job[`${property}`] || null;
    }

  }

  /**
   * GET CLASS PROPERTIES VALUES MOTHODS
   */

   rightBelongsTo(right_id: number) {


    return {
      "0": this.userRights.filter((right: Right) => right.id === right_id).length > 0 ? 1 : 0,
      "1": this.allUserGroupRights.filter((right: Right) => right.id === right_id).length > 0 ? 1 : 0,
      "2": this.allUserGroupRights.filter((right: Right) => right.id === right_id).length > 0 ? 1 : 0,
    };

   }


   oneditPassword() {
     console.log("oneditPassword");


     this.dialogRef = this._matDialog.open(EditPasswordFormComponent, {
      panelClass: 'dialog-dialog-container',
      data      : {
          action: 'EditPassword'
      }
    });

    this.dialogRef.afterClosed().subscribe((response: any) => {
      if ( !response ) { return; }

          let data: any

          let action  = response[0];
          let user_id = this._authService.getAuthenticatedUserId();

          switch (action) {
            case 'save':
              data = this._util.filterFormGroupValues(response[1]);
              data["user_id"] = user_id;
              break;
          }

          // do put request action
          this._usersService.editPassword(data)
            .subscribe((_: any) => {
            if(_.ok) {
              this.openSnackBar(_.feedback);
            } else {
              this.openSnackBar(_.error);
            }
          });

      });
   }

   openSnackBar(message) {
    this.snackBar.open(message, "ok", {
      duration: 2500,
      horizontalPosition: "end",
      verticalPosition: "bottom",
    });
   }

   isMySuperior(child_id: number, parent_id: number) {
     return false;
   }

}
