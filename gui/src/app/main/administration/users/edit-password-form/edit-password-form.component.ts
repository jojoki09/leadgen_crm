import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-password-form',
  templateUrl: './edit-password-form.component.html',
  styleUrls: ['./edit-password-form.component.scss']
})
export class EditPasswordFormComponent implements OnInit {

  fg1: FormGroup;
  dialogTitle: string;
  action: string;

  hide_current = true;
  hide_new = true;

  constructor(
    public matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _formBuilder: FormBuilder,
  ) { 

    this.action = _data.action;

    switch (this.action) {
      case 'EditPassword':
        this.dialogTitle = 'Modifier le mot de passe';
        this.fg1 = this.createEditPasswordForm();
        break;
    }

  }

  ngOnInit() {
  }

  createEditPasswordForm(): FormGroup {
    return this._formBuilder.group({
      "current_password"           : ["", Validators.required],
      "new_password"      : ["", Validators.required],
    });
  }


  requestEditPassword() {
    this.matDialogRef.close(['save',this.fg1]);
  }
}
