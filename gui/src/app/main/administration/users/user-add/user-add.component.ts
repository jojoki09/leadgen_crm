import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs/Rx";
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import {User} from "../user.model";
import {Role} from "../../roles/role.model";
import {Right} from "../../rights/right.model";
import {Module} from "../../modules/module.model";
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import {UsersService} from "../users.service";
import {RolesService} from "../../roles/roles.service";
import {RightsService} from "../../rights/rights.service";
import {ModulesService} from "../../modules/modules.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Departement } from '../../departements/departement.model';
import { DepartementsService } from '../../departements/departements.service';
import { Job } from '../job.model';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'app/services/auth.service';
import { NGXLogger } from 'ngx-logger';
import {DatePipe} from "@angular/common";
import * as moment from "moment";
import {isMoment} from "moment";

export interface ModuleRights {
    module_id?: number,
    rights?: {key: number, value: 0 |1 }[]
}

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit, OnDestroy {

    dialogRef: any;
    confirmDialogRef: MatDialogRef<ConfirmDialogComponent>;
    
    module_rights: ModuleRights[] = [];
    modules: Module[] = [];
    rights: Right[] = [];
    roles: Role[] = [];
    users: User[] = [];
    departements: Departement[] = [];
    jobs: Job[] = [];

    rights_depending: number = null;
    hide = true;

    form: FormGroup;
    // Vertical Stepper
    fg1: FormGroup;
    fg2: FormGroup;
    fg3: FormGroup;
    fg4: FormGroup;
    fg5: FormGroup;
    
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(private _formBuilder: FormBuilder, private  _usersService: UsersService,
                private  _departementsService: DepartementsService,private  _rolesService: RolesService,
                private _rightsService: RightsService,
                private _moduleService: ModulesService,
                public dialog: MatDialog,
                public snackBar: MatSnackBar,
                private router: Router,
                private activatedRoute:ActivatedRoute,
                public _authService: AuthService,
                private logger: NGXLogger,
    ) {        
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        
        
    }

    ngOnInit() {
        
        if(!this._authService.hasRight('ajouter et modifier un utilisateur', 1))
            this.router.navigateByUrl("management_tools/users/list");

        this._usersService.breadcrumb.next({
            steps: [
                "Utilisateur",
                "Ajout d'un utilisateur"
            ],
            big_header: "Ajout d'un utilisateur."
        });

        this.fg1 = this._formBuilder.group({
            first_name: [''],
            last_name: ['', Validators.required],
            cin: ['' , Validators.required],
            phone_number: [''],
            address: [''],
            photo: [''],
            birth_day: [new Date()],
            gender: ['', Validators.required],
        });


        this.fg2 = this._formBuilder.group({
            "login": ['', Validators.required],
            "email": ['', [Validators.required, Validators.email]],
            "password": ['', Validators.required],
        });

        this.fg3 = this._formBuilder.group({
            "job_id": ['', Validators.required],
            "superior_id": ['', Validators.required],
            "departement_id": ['',  Validators.required],
        });

        this.fg4 = this._formBuilder.group({
            "role_id": [''],
            "rights_depending": ['', Validators.required]
        });


        this._usersService.users.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: User[]) => { this.users = data || []; });
        this._departementsService.departements.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Departement[]) => { this.departements = data || []; });
        this._rolesService.roles.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Role[]) => { 
            if(data && data.length > 0)
                this.roles =  data.filter(role => role.id != 1 || role.role_name != 'super admin' );
        });
        this._usersService.jobs.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Job[]) => { this.jobs = data || []; });
        this._rightsService.rights.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Right[]) => { this.rights = data || []; });



        /* this._moduleService.modules.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
            this.modules = data || [];

            this.modules.forEach((module: Module, i) => {
                let rights = [];
                this.getRightsByModule(module.id).forEach((right: Right, i) => {
                    rights.push({ key: right.id, value: 0});
                })

                this.module_rights.push({ module_id: module.id, rights: rights});
            });
        }); */

        /* ------------------------------------------------------------------------------- */

        this._moduleService.modules.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Module[]) => {
            this.modules = data || [];

            let module_rights  = [];
    
            /* get modules */
            const myRights: Right[] = this._authService.myRights();
            let modulesIds  = [];
            myRights.forEach(right => { if(modulesIds.indexOf(right.module_id) < 0) { modulesIds.push(right.module_id); } });
    
            
            for (let i = 0; i < modulesIds.length; i++) {
                const module_id = modulesIds[i];
    
                let rights_for_current = [];
    
                /* get rights for selected module */
                let rights = myRights.filter(right => right.module_id == module_id);
                if (rights.length > 0) {
                    rights.forEach(right => {
                        rights_for_current.push({key: right.id, value: 0});
                    })
                } else 
                    continue;
                
                /* format the matrix */
                module_rights.push({ module_id, rights: rights_for_current})
            }
    
            this.module_rights = module_rights;
    
    
            this.logger.trace('module_rights_matrix', module_rights);
            
        });
        
        /* ------------------------------------------------------------------------------- */

        
        

    }

    onRDChanged() {
        this.rights_depending = parseInt(this.fg4.controls["rights_depending"].value);

    }

    getRightsByModule(module_id): Right[]{
        return this.rights.filter((right: Right) => { return right.module_id == module_id });
    }

    getModuleByRight(right_id): Module {
        let right: Right = this.rights.filter((right: Right) => { return right.id == right_id })[0];
        return this.modules.filter((module: Module) => { return module.id == right.module_id })[0];
    }

    setRightTo(module_id, e: Event) {
        e.stopPropagation();
        let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
            return el.module_id === module_id;
        })[0];


        matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
            el.value = 1;
        });
    }

    unsetRightTo(module_id, e: Event) {
        e.stopPropagation();

        let matrix_obj: ModuleRights = this.module_rights.filter((el: ModuleRights) => {
            return el.module_id == module_id;
        })[0];


        matrix_obj.rights.forEach((el: { key: number, value: 0 | 1}) =>{
            el.value = 0;
        })

    }

    getModuleProperty(module_id, property) {
        return this.modules.filter((module: Module) => { return module.id == module_id })[0][`${property}`] || undefined;
    }

    getRightProperty(right_id, property) {
        return this.rights.filter((right: Right) => { return right.id == right_id })[0][`${property}`] || undefined;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Finish the vertical stepper
     */
    finishVerticalStepper(): void
    {

        let formData: FormData = new FormData();
        let photo: any = document.querySelector('#photo');
        
        var formInformation = Object.assign({}, 
            this.fg1.value || {},
            this.fg2.value || {},
            this.fg3.value || {},
            this.fg4.value || {},
            );


        /*var datePipe = new DatePipe("en-US");
        this.f.date.du = datePipe.transform(this.f.date.du, 'yyyy-MM-dd');
        this.f.date.a= datePipe.transform(this.f.date.a, 'yyyy-MM-dd');*/


        const $moment = this.fg1.controls["birth_day"].value;
        // console.log("STREAM", $moment);

        if(isMoment($moment)) {
            // console.log("moment().format()", $moment.format("YYYY-MM-DD"));
            formInformation["birth_day"] = $moment.format("YYYY-MM-DD");
        } else
            delete formInformation["birth_day"];


        /*if(this.fg1.controls["birth_day"].value.toDateString() != new Date().toDateString()) {
            // fixing format for post
            formInformation["birth_day"] = this.fg1.controls["birth_day"].value.format('YYYY-MM-DD');
        } else
            delete formInformation["birth_day"];*/


        for(let prop in formInformation) {
            if(formInformation[prop] === "") {
                delete formInformation[prop];
                continue;
            } 

            // prepare form data object
            formData.append(prop, formInformation[prop]);

        }

        var files = photo.files;
        let fileList: FileList = files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            formData.append('photo', file, file.name);
        }

        let rights: number[] = [];

        this.module_rights.forEach((el: ModuleRights) => {
            el.rights.forEach(right => {
                if(right.value) {
                    rights.push(right.key);
                }
            })
        })

        formData.append("rights", JSON.stringify(rights));

        
        
        // doing the request
        
        let messages: string[] = [];

        

        this._usersService.addUser(formData).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: any) => {
            if(_.ok) {
                messages.push(_.feedback);

                this._usersService.refresh();

                const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                    width: '30rem',
                    data: {content: _.feedback, title: "L'ajout d'un utilisateur", noThanks: false}
                });
        
                dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                    if(result) {
                        this.router.navigate(["../list"], { relativeTo: this.activatedRoute });
                    }
                });

                

            } else {
                const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                    width: '30rem',
                    data: {content: _.error, title: "L'ajout d'un utilisateur", noThanks: false}
                });
        
                dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe(result => {
                    if(result) {
                        
                    }
                });
            }
        });

        
        
        

        
        
        
    }
}
