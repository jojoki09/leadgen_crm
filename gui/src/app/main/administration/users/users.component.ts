import { Component, OnInit, OnDestroy } from '@angular/core';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ActivatedRoute, NavigationEnd, Router, RoutesRecognized} from "@angular/router";
import {PrintService} from "../services/print.service";
import { filter, map, takeUntil } from 'rxjs/operators';
import {BreadCrumb, UsersService} from "./users.service";
import { Subject } from 'rxjs';
// import { map } from "rxjs/add/operator/map";



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
    private _unsubscribeAll: Subject<any>;    
    breadcrumb: BreadCrumb;


    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _usersService: UsersService
    ) { 
        this._unsubscribeAll = new Subject();                
    }

    ngOnInit() {

        this._usersService.breadcrumb.pipe(takeUntil(this._unsubscribeAll)).subscribe((breadcrumb: BreadCrumb) => {
            this.breadcrumb = breadcrumb;
        })


    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    goBack() {
        history.back();
    }
}
