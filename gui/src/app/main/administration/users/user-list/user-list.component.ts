import {Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';

import {
    MatDialog,
    MatPaginator,
    MatSort,
    MatTableDataSource,
    Sort,
    MatSnackBar, MatInput,
} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import {UsersService} from "../users.service";
import {User} from "../user.model";

import { ConfirmDialogComponent } from "../../confirm-dialog/confirm-dialog.component";
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription  } from 'rxjs';
import { Job } from '../job.model';
import { AuthService } from 'app/services/auth.service';
import {DatePipe} from "@angular/common";



@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit, OnDestroy {


    @ViewChild('fromInput', {
        read: MatInput
    }) fromInput: MatInput;

    @ViewChild('toInput', {
        read: MatInput
    }) toInput: MatInput;

    f = {
        "login": null,
        "email": null,
        "last_login": {
            "du": "",
            "a": "",
        },
        "phone_number": null,
        "job": null,
        "full_name": null,
        "disabled": null,
    };


    hideRemoveUserAction: boolean = false;
    // --------------------------------------------------------

    onRefreshing: boolean = false;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filter') filter: ElementRef;
    users: any = [];

    pageSize: number = 25;
    totalSize = 0;
    pageSizeOptions: number[] = [25, 75, 100, 500];

    displayedColumns: string[] =
        [
            'login',
            'email',
            'last_name',
            'phone_number',
            'function',
            'disabled',
            'last_login',
            'buttons'
        ];


    dataSource: any;
    enablePaginator: boolean = false;

    sortData(sort: Sort) {
        const data = this.users.slice();
        if (!sort.active || sort.direction === '') {
            this.users = data;
            return;
        }

        this.users = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'login': return compare(a.login, b.login, isAsc);
                case 'email': return compare(a.email, b.email, isAsc);
                case 'full_name': return compare(a.full_name, b.full_name, isAsc);
                case 'phone_number': return compare(a.phone_number, b.phone_number, isAsc);
                case 'function': return compare(a.function, b.function, isAsc);
                case 'disabled': return compare(a.disabled, b.disabled, isAsc);
                case 'last_login': return compare(a.last_login, b.last_login, isAsc);
                default: return 0;
            }
        });
    }

    // --------------------------------------------------------

    private subscribtions: Subscription[] = [];

    jobs: Job[] = [];

    constructor(
        private _usersService: UsersService,
        public dialog: MatDialog,
        public snackBar: MatSnackBar,
        private router: Router,
        private activatedRoute:ActivatedRoute,
        public _authService: AuthService,
    ) 
    {

        this.subscribtions.push(this._usersService.users.subscribe((data: any[]) => {
            this.users = data;

            this.subscribtions.push(this.activatedRoute.queryParams.subscribe((params: any) => {

                const action = params.do;
                let d: any = [];

                if ( data && data.length > 0)
                {
                    if(action == "filter") {
                        const by = params.by;
                        if(by ==  "departement") {
                            const dept_id = params.departement_id;
                            this._usersService.breadcrumb.next({ steps: [ "Utilisateur", "List" ], big_header: "Résultat des utilisateurs travaillant sous le département sélectionné", goBackBtn: true});
                            this.users = data.filter((user: User) => user.departement_id == dept_id) || [];
                        }

                        this.hideRemoveUserAction = true;
                    } else {

                        this.users = data || [];
                        this._usersService.breadcrumb.next({ steps: [ "Utilisateur", "List" ], big_header: "LISTE DES UTILISATEURS" , goBackBtn: false});
                        this.hideRemoveUserAction = false;
                    }


                    this.dataSource = new MatTableDataSource<Element>(this.users);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                    this.totalSize = this.users.length;
                }

            }));

        }));

    }

    ngOnInit() {

    }

    
    ngOnDestroy() {
        this.subscribtions.forEach(subscribtions => subscribtions.unsubscribe());
    }
    

    onDeleteUser(row: any) {
        /*
        * throw confirmation message
        * remove with success alert
        * */

        let id = row.id;

        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '30rem',
            data: {content: "Êtes-vous sûr de vouloir supprimer cet élément?", title: "Alerte de confirmation"}
        });

        dialogRef.afterClosed().subscribe(result => {
            if(result) {
                this._usersService.deleteUser(id).subscribe((res: any) => {
                    if(res.ok){
                        this.snackBar.open(res.feedback, "ok", {
                            duration: 2500,
                            horizontalPosition: "end",
                            verticalPosition: "bottom",
                        });

                        this.refresh();
                    }
                })
            }
        });

    }

    onShowProfile(user_id: number) {
        this.router.navigate([`../${user_id}`], {relativeTo: this.activatedRoute});
    }

    getJobProperty(job_id, property) {
        if(!job_id || !property)
          return null;
        else {
          let job = this.jobs.filter((job: Job) => { return job.id == job_id })[0]
          if(!job)
            return null;
          return job[`${property}`] || null;
        }
    
      }

      refresh() {
        this._usersService.refresh();
      }

    clear()
    {
        this.fromInput.value = "";
        this.toInput.value = "";
        this.f.last_login.a = "";
        this.f.last_login.du = "";
        this.super_filter();
    }

    super_filter() {

        let d = true;
        let filteredData = [];

        /*f = {
            "login": null,
            "email": null,
            "last_login": {       *********************
                "du": "",
                "a": "",
            },
            "phone_number": null,
            "job": null,
            "full_name": null,
            "disabled": null,        ****
        };*/



        if(this.f.disabled != null && this.f.disabled  != "") {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.disabled == this.f.disabled);
            d = false;
        }

        if(this.f.login != null && this.f.login  != "") {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.login.toLowerCase().indexOf(this.f.login.toLowerCase()) > -1);
            d = false;
        }

        if(this.f.email != null && this.f.email  != "") {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.email.toLowerCase().indexOf(this.f.email.toLowerCase()) > -1);
            d = false;
        }

        if(this.f.phone_number != null && this.f.phone_number  != "") {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.phone_number.toLowerCase().indexOf(this.f.phone_number.toLowerCase()) > -1);
            d = false;
        }

        if(this.f.job != null && this.f.job  != "") {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.job.toLowerCase().indexOf(this.f.job.toLowerCase()) > -1);
            d = false;
        }

        if(this.f.full_name != null && this.f.full_name  != "") {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.full_name.toLowerCase().indexOf(this.f.full_name.toLowerCase()) > -1);
            d = false;
        }

        if(this.f.last_login.du != null && this.f.last_login.a != null)
        {
            var last_loginPipe = new DatePipe("en-US");
            this.f.last_login.du = last_loginPipe.transform(this.f.last_login.du, 'yyyy-MM-dd');
            this.f.last_login.a= last_loginPipe.transform(this.f.last_login.a, 'yyyy-MM-dd');
        }

        if(this.f.last_login.du != null && this.f.last_login.a != null)
        {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.last_login >= this.f.last_login.du && user.last_login <= this.f.last_login.a );
            d = false;
        }

        if(this.f.last_login.du !=null && this.f.last_login.a == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.last_login >= this.f.last_login.du);
            d = false;
        }

        if(this.f.last_login.a !=null && this.f.last_login.du == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.users;
            filteredData = data.filter(user => user.last_login <= this.f.last_login.a);
            d = false;
        }


        this.dataSource = new MatTableDataSource<Element>(d ? this.users : filteredData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.totalSize = d ? this.users.length : filteredData.length;

        return d ? this.users : filteredData;
    }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
