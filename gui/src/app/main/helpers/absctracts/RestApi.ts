import { environment } from "../../../../environments/environment";
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export abstract class RestApi {

    protected baseUrl = environment["API_URL"];
  
    constructor(private http: HttpClient) { }
  
    protected get(relativeUrl: string) {
      return this.http.get(this.baseUrl + relativeUrl);
    }
    
    protected post(relativeUrl: string, data: any) {
      return this.http.post(this.baseUrl + relativeUrl, data);
    }
  
  }