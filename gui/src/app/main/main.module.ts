import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import {FuseSharedModule} from "../../@fuse/shared.module";

const routes = [
    {
        path        : 'gestion-project',
        loadChildren:  './gestion-project/gestion-projects.module#GestionProjectsModule'  /*() => GestionProduitsModule */,
    },
    {
        path        : 'partners',
        loadChildren:  './partner/partner.module#PartnerModule'

    },
    {
        path     : 'prospects',
        loadChildren: './prospect/prospect.module#ProspectModule',
    },
    {
        path        : 'dashboards/analytics',
        loadChildren: './dashboards/analytics/analytics.module#AnalyticsDashboardModule'
    },
    {
        path        : 'dashboards/project',
        loadChildren: './dashboards/project/project.module#ProjectDashboardModule'
    },
    {
        path        : 'home',
        loadChildren:  './accueil/accueil.module#AccueilModule'  /*() => AccueilModule */,
    },
    {
        path        : 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    }

  ];
@NgModule({
  declarations: [],
  imports: [
      RouterModule.forChild(routes),
      FuseSharedModule
  ]
})
export class MainModule { }
