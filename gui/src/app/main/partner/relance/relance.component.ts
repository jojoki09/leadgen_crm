import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from "@angular/material";
import {PartnerService} from "../partner.service";
import {Subject} from "rxjs";
import {fuseAnimations} from "../../../../@fuse/animations";

@Component({
  selector: 'app-relance',
  templateUrl: './relance.component.html',
  styleUrls: ['./relance.component.scss'],
  animations   : fuseAnimations

})
export class RelanceComponent implements OnInit {
    private _unsubscribeAll: Subject<any>;
    dataSource: any;
    partners: any ;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    totalSize = 0;
    pagesize: number = 50;
    displayedColumns = ["object", "nom", "prenom", "buttons"];

    f = {
        "nom": null,
        "prenom": null,
        "object": null,
    };

    constructor(
      private partnerService: PartnerService,

      private _matSnackBar: MatSnackBar,
) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {

      this.partnerService.getRelaunch().subscribe((data: any) => {
          if ( data )
          {
              console.log(data);
              this.dataSource = new MatTableDataSource<Element>(data);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.partners = data;
              this.totalSize = this.partners.length;
          }

      });
  }

}
