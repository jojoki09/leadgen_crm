import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs/index";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {catchError, tap} from "rxjs/operators";
import {EMPTY} from "rxjs/internal/observable/empty";
import {User} from "../administration/users/user.model";

@Injectable({
  providedIn: 'root'
})
export class PartnerService {
    apiUrl = environment["API_URL"];
    private _partners: BehaviorSubject<any[]> = new BehaviorSubject([]);
    public readonly partners: Observable<any[]> = this._partners.asObservable();

    private _partnersdetail: BehaviorSubject<any[]> = new BehaviorSubject([]);
    public readonly partnersdetail: Observable<any[]> = this._partnersdetail.asObservable();

    devis_id: string;
    partnerDetail: any;
    routeParams: any;
    partners_list: any[] = [];
    test : number = 0;
    partner: any;
    onPartnertChanged: BehaviorSubject<any>;
    constructor(
        private http: HttpClient,
    ) {

        this.partners.subscribe((partners: any[]) => { this.partners_list = partners; });
        this.onPartnertChanged = new BehaviorSubject([]);

    }

    resolve(route: ActivatedRouteSnapshot,activatedRoute: ActivatedRoute, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        // this.routeParams = route.params;
        this.routeParams = route.params;

        this.devis_id = activatedRoute.root.queryParams['devis_id'];
        // this.devis_id = activatedRoute.snapshot.queryParams['devis_id'];

        // activatedRoute.queryParams.subscribe(params => {
        //     this.devis_id = params['devis_id'];
        // });
        console.log(this.routeParams);
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPartner()
            ]).then(
                () => {
                    this.getPartnerDetail();
                        resolve();
                },
                reject
            );
        });
    }

    getDocument(partner_id): Observable<any> {

        return  this.http.get(`${this.apiUrl}getDocument/${partner_id}`,{});

    }

    getPartnerTest(): Observable<any> {

        return  this.http.get(`${this.apiUrl}ProcepectByPartId/${this.routeParams.id}`,{});

    }
    getPartner(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onPartnertChanged.next(false);
                resolve(false);
            }
            else
            {

                this.http.get<any>(`${this.apiUrl}ProcepectByPartId/${this.routeParams.id}`)
                    .subscribe((response: any) => {
                        console.log(response);
                        this.partner = response;
                        this.onPartnertChanged.next(this.partner);
                        resolve(response);
                    }, reject);
            }
        });
    }

    getRelaunch(): Observable<any> {

        return  this.http.get(this.apiUrl+'getRelaunch',{});

    }

    getCities(country_id): Observable<any> {

        return  this.http.get(`${this.apiUrl}cities/${country_id}`,{});

    }

    getCountries(): Observable<any> {

        return  this.http.get(this.apiUrl+'countries',{});

    }
    LoadStatus(type): Observable<any> {

        return  this.http.get(this.apiUrl+'ListesStatus/',{params:type});

    }

    getPartnerDetail(): Observable<any> {
        let devis_id = {
            devis_id: this.devis_id,
        };
        return this.http.get(`${this.apiUrl}detailProspect/`, {params:devis_id});



    }

    getPartners(): Observable<any> {
        return this.http.get(`${this.apiUrl}Procepect`)
                        .pipe(tap((partners: any[]) => this._partners.next(partners) ));
    }

    getPartnerById(partner_id): Observable<any> {
        return this.http.get(`${this.apiUrl}partners/${partner_id}`);
    }

    addPartner(Partner: any): Observable<any> {
        return this.http.post<any>(`${this.apiUrl}partners`, Partner)
    }

    updatePartner(Partner: any): Observable<any> {

        return this.http.put(`${this.apiUrl}partners/${this.routeParams.id}`, Partner);
    }

    addPartnerDetail(Payload: any): Observable<any> {
        return this.http.post<any>(`${this.apiUrl}detailProspect`, Payload)
    }

    addNextCalendar(Payload: any): Observable<any> {
        return this.http.post<any>(`${this.apiUrl}calendars`, Payload)
    }

    deletePartner(id): Observable<{}> {
        return this.http.delete(`${this.apiUrl}partners/${id}/delete`)
    }

    refresh() {
      this.getPartners().subscribe();
    }

}
