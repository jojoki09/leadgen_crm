import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE, MatDialog,
    MatInput,
    MatPaginator, MatSnackBar,
    MatSort,
    MatTableDataSource
} from "@angular/material";
import {fuseAnimations} from "../../../../@fuse/animations";
import {MomentDateAdapter} from "@angular/material-moment-adapter";
import {Subject} from "rxjs/index";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {DatePipe} from "@angular/common";
import * as moment from "moment";
import {environment} from "../../../../environments/environment";
import {PartnerService} from "../partner.service";
import {ProjectService} from "../project.service";
import {ConfirmDialogComponent} from "../../administration/confirm-dialog/confirm-dialog.component";


export const MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: environment["format"].date,
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'YYYY',
    },
};
@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss'],
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class PartnerListComponent implements OnInit {

    @ViewChild('fromInputC', {read: MatInput}) fromInputC: MatInput;
    @ViewChild('toInputC', {read: MatInput}) toInputC: MatInput;
    @ViewChild('fromInputU', {read: MatInput}) fromInputU: MatInput;
    @ViewChild('toInputU', {read: MatInput}) toInputU: MatInput;


    f = {
        "status": null,
        "nom": null,
        "prenom": null,
        "project": null,
        "cree_par": null,
        "date_creation": {"du": "","a": "",},
        "mis_a_jour_par": null,
        "mis_a_jour_a": {"du": "","a": "",},
    };

    /*
      id,
      status,
      nom,
      prenom,
      project,
      cree_par,
      date_creation,
      mis_a_jour_par,
      mis_a_jour_a,
    */

    dataSource: any;
    displayedColumns = ["status", "nom", "prenom", "project", "cree_par", "date_creation", "mis_a_jour_par", "mis_a_jour_a", "buttons"];
    totalSize = 0;
    pagesize: number = 50;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @ViewChild('filter') filter: ElementRef;
    private _unsubscribeAll: Subject<any>;

    partners: any ;
    filteredPartners: any = [];
    projects: any = [];

    constructor(
        private partnerService: PartnerService,
        private projectService: ProjectService,
        private _matSnackBar: MatSnackBar,
        public _matDialog: MatDialog,
        private  _authService: AuthService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {
        this._unsubscribeAll = new Subject();
    }

  ngOnInit() {
    this.projectService.getProjects().subscribe();
    this.projectService.projects.subscribe(projects => this.projects = projects);

    this.partnerService.getPartners().subscribe((data: any) => {
        if ( data )
        {
            this.dataSource = new MatTableDataSource<Element>(data);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.partners = data;
            this.filteredPartners = data;
            this.totalSize = this.partners.length;
        }

    });
  }

  refresh() {
      this.partnerService.refresh();
  }

  clear1() {
      this.fromInputC.value = "";
      this.toInputC.value = "";
      this.f.date_creation.a = "";
      this.f.date_creation.du = "";
      this.super_filter();
  }

  clear2() {
      this.fromInputU.value = "";
      this.toInputU.value = "";
      this.f.mis_a_jour_a.a = "";
      this.f.mis_a_jour_a.du = "";
      this.super_filter();
  }

  super_filter() {
        let d = true;
        let filteredData = [];

        /*f = {
            *****************************"status": null,
            "nom": null,
            "prenom": null,
            "project": null,
            "cree_par": null,
            *****************************"date_creation": {"du": "","a": "",},
            "mis_a_jour_par": null,
            *****************************"mis_a_jour_par": {"du": "","a": "",},
        };*/

          if(this.f.nom != null && this.f.nom  != "") {
             let data = filteredData.length > 0 ? filteredData : this.partners;
             filteredData = data.filter(partner =>{ return (partner.nom + "").toLowerCase().indexOf(this.f.nom.toLowerCase().trim()) > -1 }) ;
             console.log(filteredData);
            d = false;
         }
        if(this.f.prenom != null && this.f.prenom != "") {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            filteredData = data.filter(partner => (partner.prenom + "").toLowerCase().indexOf(this.f.prenom.toLowerCase().trim()) > -1);
            d = false;
        }
        if(this.f.project != null && this.f.project != "") {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            filteredData = data.filter(partner => (partner.project + "").toLowerCase().indexOf(this.f.project.toLowerCase().trim()) > -1);
            d = false;
        }
        if(this.f.cree_par != null && this.f.cree_par != "") {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            filteredData = data.filter(partner => (partner.cree_par + "").toLowerCase().indexOf(this.f.cree_par.toLowerCase().trim()) > -1);
            d = false;
        }
        if(this.f.mis_a_jour_par != null && this.f.mis_a_jour_par != "") {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            filteredData = data.filter(partner => (partner.mis_a_jour_par + "").toLowerCase().indexOf(this.f.mis_a_jour_par.toLowerCase().trim()) > -1);
            d = false;
        }

        if(this.f.status != null && this.f.status  != "") {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            console.log(data);
            filteredData = data.filter(partner =>   partner.statut == this.f.status);
         console.log(filteredData) ;
            d = false;
        }

        // date_creation
        if(this.f.date_creation.du != null && this.f.date_creation.a != null)
        {
            var datePipe = new DatePipe("en-US");
            this.f.date_creation.du = datePipe.transform(this.f.date_creation.du, 'yyyy-MM-dd');
            this.f.date_creation.a= datePipe.transform(this.f.date_creation.a, 'yyyy-MM-dd');
        }

        if(this.f.date_creation.du != null && this.f.date_creation.a != null)
        {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            filteredData = data.filter(partner => partner.date_creation != null ? partner.date_creation >= this.f.date_creation.du && partner.date_creation <= this.f.date_creation.a  : false);

            d = false;
        }

        if(this.f.date_creation.du !=null && this.f.date_creation.a == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            filteredData = data.filter(partner => partner.date_creation != null ? partner.date_creation >= this.f.date_creation.du : false);
            d = false;
        }

        if(this.f.date_creation.a !=null && this.f.date_creation.du == null)
        {
            let data = filteredData.length > 0 ? filteredData : this.partners;
            filteredData = data.filter(partner => partner.date_creation != null ? partner.date_creation <= this.f.date_creation.a : false);
            d = false;
        }

      // mis_a_jour_a
      if(this.f.mis_a_jour_a.du != null && this.f.mis_a_jour_a.a != null)
      {
          var datePipe = new DatePipe("en-US");
          this.f.mis_a_jour_a.du = datePipe.transform(this.f.mis_a_jour_a.du, 'yyyy-MM-dd');
          this.f.mis_a_jour_a.a= datePipe.transform(this.f.mis_a_jour_a.a, 'yyyy-MM-dd');
      }

      if(this.f.mis_a_jour_a.du != null && this.f.mis_a_jour_a.a != null)
      {
          let data = filteredData.length > 0 ? filteredData : this.partners;
          filteredData = data.filter(partner => partner.mis_a_jour_a != null ? partner.mis_a_jour_a >= this.f.mis_a_jour_a.du && partner.mis_a_jour_a <= this.f.mis_a_jour_a.a  : false);
          d = false;
      }

      if(this.f.mis_a_jour_a.du !=null && this.f.mis_a_jour_a.a == null)
      {
          let data = filteredData.length > 0 ? filteredData : this.partners;
          filteredData = data.filter(partner => partner.mis_a_jour_a != null ? partner.mis_a_jour_a >= this.f.mis_a_jour_a.du : false);
          d = false;
      }

      if(this.f.mis_a_jour_a.a !=null && this.f.mis_a_jour_a.du == null)
      {
          let data = filteredData.length > 0 ? filteredData : this.partners;
          filteredData = data.filter(partner => partner.mis_a_jour_a != null ? partner.mis_a_jour_a <= this.f.mis_a_jour_a.a : false);
          d = false;
      }

        this.dataSource = new MatTableDataSource<Element>(d ? this.partners : filteredData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.totalSize = d ? this.partners.length : filteredData.length;


        return d ? this.partners : filteredData;
    }

    ondeletePartner(partner_id) {
        const dialogRef = this._matDialog.open(ConfirmDialogComponent, {width: '30rem',data: {content: "Êtes-vous sûr de vouloir supprimer cet élément?", title: "Alerte de confirmation"}});

        dialogRef.afterClosed().subscribe(result => {
            if(result) {
                this.partnerService.deletePartner(partner_id).subscribe((_: any) => {
                    if(_.ok){
                        this._matSnackBar.open(_.message, "ok", { duration: 5000, horizontalPosition: "end", verticalPosition: "bottom", });
                    } else
                        this._matSnackBar.open(_.error, "ok", { duration: 5000, horizontalPosition: "end", verticalPosition: "bottom", });

                    this.refresh();
                })
            }
        });
    }
}
