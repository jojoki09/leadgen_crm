import { Component, OnInit } from '@angular/core';
import {MatDialog, MatSnackBar, MatTableDataSource} from "@angular/material";
import {ProjectService} from "../project.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PartnerService} from "../partner.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Location} from "@angular/common";
import {fuseAnimations} from "../../../../@fuse/animations";

import * as moment from "moment";
import {FuseSidebarService} from "../../../../@fuse/components/sidebar/sidebar.service";
import {Subject} from "rxjs";

@Component({
  selector: 'app-partner-action-form',
  templateUrl: './partner-action-form.component.html',
  styleUrls: ['./partner-action-form.component.scss'],
    animations   : fuseAnimations,
})
export class PartnerActionFormComponent implements OnInit {
  showing:boolean = false;
  partner: any = { nom: null};
  pageType = null;
  rangeOfYears = [];
  // todo construct the form
  myform: FormGroup;
  projects = [];
  sources = [];
  villes = [];
  countries = [];

  filterdProjects = [];
  filterdSources = [];
  filterdVilles = [];
  filterdCountries = [];
  private _unsubscribeAll: Subject<any>;



    constructor(
      private partnerService: PartnerService,
      private projectService: ProjectService,
      private _fuseSidebarService: FuseSidebarService,
      private _matSnackBar: MatSnackBar,
      public _matDialog: MatDialog,
      private  _authService: AuthService,
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private _formBuilder : FormBuilder,
      private _location: Location
  ) {
        this._unsubscribeAll = new Subject();

        this.activatedRoute.queryParams.subscribe((params: any) => {

          const todo = params.todo || undefined;
          if(!todo) return;

          {
              if(todo == "ADD") {
                  // ADD
                  this.pageType = 'new';
                  this.myform = this.createPartnerAddForm();

              } else if(todo == "EDIT") {
                  console.log(params.pid);
                  const partner_id = params.pid || undefined;
                  if(!partner_id) return;
                  // EDIT
                  this.pageType = 'edit';
                  let $p = this.partnerService.partners_list.filter(p => p.id === parseInt(partner_id));
                  if($p.length) {
                      this.partner = $p[0];
                  } else
                    this.backClicked();

                  this.myform = this.createPartnerEditForm();
              }
          }

      });
  }

    range(start, end, step = 1){

        if (step <= 0) {
            throw new Error('step must be a number greater than 0.');
        }
        if (start > end) {
            step = -step;
        }

        const length = Math.floor(Math.abs((end - start) / step)) + 1;

        // Fill up a new array with the range numbers
        // using Array.from() with a mapping function.
        // Finally, return the new array.
        return Array.from(Array(length), (x, index) => start + index * step);
    }

  ngOnInit() {
      this.rangeOfYears = this.range(2010,2019);

      var d = new Date();
      var m3 =d.getMonth();
      $("#interventionType option[value="+m3+"]").prop('selected', true);
      $('#interventionType').change();

      console.log(this.rangeOfYears);
      this.partnerService.refresh();
      this.projectService.getProjects().subscribe();
      this.projectService.getSources().subscribe((data: any) => {
          if ( data )
          {
              this.sources = data.data; this.filterdSources = data.data;
          }

      });
      this.getCountries();



      this.projectService.projects.subscribe(projects => { this.projects = projects; this.filterdProjects = projects;})
      // this.projectService.sources.subscribe(sources => { this.sources = sources; this.filterdSources = sources; })


  }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    backClicked()
    {
        this._location.back();
    }

    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    onaddPartner() {

        let payload = {};

        let nextVisiteFormated = moment(this.myform.get('next_visite').value).format('YYYY-MM-DD');


        payload["type"] = this.myform.get('type').value;
        payload["nom"] = this.myform.get('nom').value;
        payload["prenom"] = this.myform.get('prenom').value;
        payload["mobile"] = this.myform.get('telephone').value;
        payload["email"] = this.myform.get('email').value;
        payload["city_id"] = this.myform.get('ville').value;
        payload["pays"] = this.myform.get('pays').value;
        payload["user_id"] = parseInt(localStorage.getItem("user_id"));
        payload["projet_id"] = this.myform.get('project_id').value;
        payload["source"] = this.myform.get('source').value;
        // payload["visite_prochaine_prevu"] = this.myform.get('visite_prochaine_prevu').value;
        // payload["date_planif"] = nextVisiteFormated;

        console.log(payload);


        this.partnerService.addPartner(payload).subscribe((response: any) => {
            this.partnerService.refresh();
            console.log(response);

            if(response.ok){
                this._matSnackBar.open('Prospect créer avec succés', "ok", {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    horizontalPosition: "end",
                    verticalPosition: "bottom",
                });
                // partner.part_id"
                // [routerLink]="['/partners/', partner.part_id]" [queryParams]="{devis_id: partner.devis_id}" >
                this.router.navigate(['/partners/' ,response.data.partenaire_id], { queryParams: {devis_id:response.data.devis_id }});

            } else
                this._matSnackBar.open(response.error, '', {
                    duration: 10000,
                    horizontalPosition: "end",
                    verticalPosition: "bottom",
                });
        });
    }

    onCountryChanged(country_id: number) {
        console.log(country_id);
        this.getCities(country_id);
        // this.filteredModels = this._modelService.getModelsByBrandId(country_id);
    }

    getCities(country_id) {

        this.partnerService.getCities(country_id).subscribe( Cities =>{
            console.log(Cities);
            this.filterdVilles = Cities;
            this.villes = Cities;
        });
    }


    //todo finish the getcountries select on change load cities correspanding to the selected city
    getCountries() {
        this.partnerService.getCountries().subscribe( Countries =>{
            console.log(Countries);
            this.filterdCountries = Countries;
            this.countries = Countries;
        });
    }

    toggleChange(event)
    {

        let checked = event.checked;
        if (checked) {
            $("#date_selected").show();
            console.log("show element");

        }
        else
        {
            console.log('hide');
            $("#date_selected").hide();
            this.myform.get('next_visite').reset('');
        }

    }
    // getDisplay(setter = null) {
    //  console.log(this.myform.get('next_visite').value);
    //   if( setter != null)
    //   this.showing = setter;
    //
    //     console.log(this.showing);
    //
    //
    //
    //   return this.showing ? 'block' : 'none';
    //
    // }

    oneditPartner() {
        // todo oneditPartner
        console.log("oneditPartner");
        console.error(this.myform.getRawValue());

    }


    createPartnerAddForm(): FormGroup
    {
        return this._formBuilder.group({
            type                                     : ['', Validators.required],
            nom                                    : ['', Validators.required],
            prenom                                     : [''],
            telephone                                    : ['', Validators.required, Validators.maxLength(10)],
            email                                   : ['', Validators.required, Validators.email],
            ville                                   : [''],
            pays                                   : [''],
            project_id                                     : ['', Validators.required],
            source                                     : ['', Validators.required],
            next_visite: [''],
            visite_prochaine_prevu                                     : ['', Validators.required],
        });
    }

    createPartnerEditForm(): FormGroup
    {
        return this._formBuilder.group({
            type                                     : [""+this.partner.type , Validators.required],
            nom                                    : [this.partner.nom , Validators.required],
            prenom                                     : [this.partner.prenom , Validators.required],
            telephone                                    : [this.partner.telephone , Validators.required],
            email                                   : [this.partner.email , Validators.required],
            ville                                   : [this.partner.ville , Validators.required],
            project_id                                     : [this.partner.project_id , Validators.required],
            source                                     : [this.partner.source , Validators.required],
            visite_prochaine_prevu                                     : [parseInt(this.partner.visite_prochaine_prevu) , Validators.required],
        });
    }

  searchProject(nom) {
        if(nom != "")
            this.filterdProjects = this.projects.length > 0 ? this.projects.filter(project =>   ("" + project.nom).toLocaleLowerCase().indexOf(nom.toLocaleLowerCase().trim()) > -1) : [];
        else
            this.filterdProjects = this.projects;
  }

    searchSource(nom) {
        if(nom != "")
            this.filterdSources = this.sources.length > 0 ? this.sources.filter(source =>   ("" + source.nom).toLocaleLowerCase().indexOf(nom.toLocaleLowerCase().trim()) > -1) : [];
        else
            this.filterdSources = this.sources;
    }

    searchVille(nom) {
        if(nom != "")
            this.filterdVilles = this.villes.length > 0 ? this.villes.filter(ville =>   ("" + ville.nom).toLocaleLowerCase().indexOf(nom.toLocaleLowerCase().trim()) > -1) : [];
        else
            this.filterdVilles = this.villes;
    }
    searchPays(nom) {
        console.log(nom);
        if(nom != "")
            this.filterdCountries = this.countries.length > 0 ? this.countries.filter(country =>   ("" + country.name).toLocaleLowerCase().indexOf(nom.toLocaleLowerCase().trim()) > -1) : [];
        else
            this.filterdCountries = this.countries;
    }

}
