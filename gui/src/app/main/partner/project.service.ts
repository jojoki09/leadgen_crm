import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs/index";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {catchError, tap} from "rxjs/operators";
import {EMPTY} from "rxjs/internal/observable/empty";

@Injectable({
    providedIn: 'root'
})
export class ProjectService {
    apiUrl = environment["API_URL"];
    private _projects: BehaviorSubject<any[]> = new BehaviorSubject([]);
    public readonly projects: Observable<any[]> = this._projects.asObservable();
    private _sources: BehaviorSubject<any[]> = new BehaviorSubject([]);
    public readonly sources: Observable<any[]> = this._sources.asObservable();

    projects_list: any[] = []


    constructor(
        private http: HttpClient,
    ) {
        this.projects.subscribe((projects: any[]) => { this.projects_list = projects; });
    }

    getProjects(): Observable<any> {
        return this.http.get(`${this.apiUrl}projects`)
            .pipe(tap((projects: any[]) => this._projects.next(projects) ));
    }



    getSources(): Observable<any> {
        return  this.http.get(this.apiUrl+'sources')
            .pipe(tap((sources: any[]) => this._sources.next(sources) ));

    }
}
