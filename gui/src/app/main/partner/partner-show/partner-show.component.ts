import {AfterViewInit, Component, ElementRef, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {PartnerService} from "../partner.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../services/auth.service";
import * as moment from "moment";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from "@angular/router";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from "@angular/material";
import {fuseAnimations} from "../../../../@fuse/animations";
// import * as $ from "jquery";
import {ConfirmDialogComponent} from "../../administration/confirm-dialog/confirm-dialog.component";
import {User} from "../../administration/users/user.model";


declare let $: any;

@Component({
    selector: 'dialog-client',
    templateUrl: 'dialog-client.html',
    styleUrls: ['./partner-show.component.scss']
})
export class DialogClient {
    contactClient :FormGroup;
    partId : any;
    private _unsubscribeAll: Subject<any>;

    @Input() partner_show: PartnerShowComponent;


    constructor( private _formBuilder: FormBuilder,
                 public dialogRef: MatDialogRef<DialogPerdu>,
                 private _matSnackBar: MatSnackBar,
                 private router:Router,
                 @Inject(MAT_DIALOG_DATA) public data: any,private partnerService: PartnerService) {
        this.partId= data.paramPartId;
        this._unsubscribeAll = new Subject();
        this.contactClient = this.createContactForm();

    }
    createContactForm(): FormGroup
    {
        return this._formBuilder.group({
            article_id :['',Validators.required],
            date :['',Validators.required],
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSubmitClient() {

        let payload = {};

        let nextVisiteFormated = moment(this.contactClient.get('date').value).format('YYYY-MM-DD');
        payload["article_id"]= this.contactClient.get('article_id').value;
        payload["date_planif"]  = nextVisiteFormated;
        payload["vente_devis_id"]  =this.partId;
        payload["type"]= -1;
        payload["statut"]= 2;


        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((clientPerdu: any) =>
        {
            try
            {
                this._matSnackBar.open('Le prospect est devenu un client ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                // setTimeout(() => {this.partner_show.ngOnInit();}, 1000);
                this.router.navigate(['partners',this.partId]);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de modification','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        },(error: any) =>{
            console.log(error);
                this._matSnackBar.open(""+ error.error.message,'',{
                    verticalPosition : 'top',
                    duration : 4000,
                    panelClass :['error-snackbar'],
                })
            }

            );

    }

    ngOnDestroy(): void
    {

        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }


}


@Component({
  selector: 'app-partner-show',
  templateUrl: './partner-show.component.html',
  styleUrls: ['./partner-show.component.scss'],
  animations   : fuseAnimations

})
export class PartnerShowComponent implements OnInit {
    fg1: FormGroup;
    enableEditfg1: boolean = false;
    partner: any;
    partnerDetail: any;
    pageType: string;
    partnerForm: FormGroup;
    attachments : any;
    routeParams: any;
    status :  any;
    devis_id: number;
    appelForm : FormGroup;
    visiteForm: FormGroup;
    emailForm: FormGroup;
    smsForm: FormGroup;
    dateForm: FormGroup;
    documentForm: FormGroup;
    private _unsubscribeAll: Subject<any>;
    animal: string;
    name: string;
    @ViewChild("img01") divView: ElementRef;


    constructor(private partnerService: PartnerService, private _formBuilder: FormBuilder,public dialog: MatDialog, public _authService: AuthService, private router:Router,activatedRoute: ActivatedRoute, private _matSnackBar: MatSnackBar
    ) {
        this._unsubscribeAll = new Subject();
        this.routeParams = activatedRoute.params;

        activatedRoute.queryParams.subscribe(params => {
            this.devis_id = params['devis_id'];
        });

        this.fg1 = this.createForms();


        }

    triggerFinder() {
        $("input#doc").trigger("click");
    }

    onSubmitDocument() {

        console.log("in download process");

        let photo: any = document.querySelector('#doc');

        let files = photo.files;

        let fileList: FileList = files;

        // prepare formdata
        let formData: any = new FormData();

        let file: File;

        if (fileList.length > 0) {
            file = fileList[0];
            formData.append('photo', file, file.name);
        }

        // inject user_id to
        formData.append('vente_devis_id', this.devis_id);
        formData.append('type', 5);

        // do put request action
        this.partnerService.addPartnerDetail(formData).pipe(takeUntil(this._unsubscribeAll)).subscribe((document: any) => {
                try {
                    this._matSnackBar.open('Le document est ajouté avec succé ', '', {
                        verticalPosition: 'top',
                        duration: 2000,
                        panelClass: ['success-snackbar']
                    });
                    setTimeout(() => {
                        this.ngOnInit();
                    }, 1000);
                    // this.router.navigate(['/gestion-project/projects']);
                }
                catch (e) {
                    this._matSnackBar.open('Erreur lors de modification', '', {
                        verticalPosition: 'top',
                        duration: 2000,
                        panelClass: ['error-snackbar']
                    });
                }
            },
            (error: any) => {
                this._matSnackBar.open("" + error.error.message, '', {
                    verticalPosition: 'top',
                    duration: 4000,
                    panelClass: ['error-snackbar'],
                })
            }
        );
    }


    onSubmitfg1() {


        var formInformation = Object.assign({}, this.fg1.value);

        // fixing format for post
        if(moment.isMoment(this.fg1.controls["birth_day"].value)) {
            // let birth_day: any = this.fg1.controls["birth_day"].value.toISOString().slice(0,10);
            let birth_day = this.fg1.controls["birth_day"].value.format('YYYY-MM-DD');
            formInformation["birth_day"] = birth_day;
        }

        // type
        //     nom
        // prenom
        // mobile
        // projet_id
        // source


        for(let prop in formInformation) {
            if(formInformation[prop] === "") {
                delete formInformation[prop];
                continue;
            }
        }

        console.log(formInformation);

        this.partnerService.updatePartner(formInformation).pipe(takeUntil(this._unsubscribeAll)).subscribe((response: any) => {

            try {
                this._matSnackBar.open(response.message, "ok", {
                    duration: 5000,
                    horizontalPosition: "end",
                    verticalPosition: "bottom",
                });
                this.enableEditfg1 = false;
                setTimeout(() => this.ngOnInit(), 1000);
                this.router.navigate(['partners', this.routeParams._value.id]);

                // this.dialog.close();
                //
                // this.dialogRef.close();
            }
            catch (e) {
                this._matSnackBar.open(response.error, '', {
                    duration: 10000,
                    horizontalPosition: "end",
                    verticalPosition: "bottom",
                });
            }
    });
    }

    createForms(): FormGroup
    {
        return this._formBuilder.group({
            email: ["", Validators.required],
            mobile: ["", Validators.required],
            nom: ["", Validators.required],
            prenom: ["", Validators.required],
            emploi: [""],
            adresse: [""],
            birth_day: [""],
        });
    }


    openDialog(dialog): void {
        switch (dialog){
            case '2':
                const dialogRef = this.dialog.open(DialogClient, {
                    panelClass: 'event-form-dialog',
                    data      : {
                        paramPartId : this.devis_id,
                    }                });

                dialogRef.afterClosed().subscribe(result => {
                    console.log('The dialog was closed');
                    this.animal = result;
                });
                break;
            case '-1':
                const dialogRef2 = this.dialog.open(DialogPerdu, {
                    panelClass: 'event-form-dialog',
                    data      : {
                        paramPartId : this.devis_id,
                    }
                });

                dialogRef2.afterClosed().subscribe(result => {
                    console.log('The dialog was closed');
                    this.animal = result;
                });
                break;
            case '4':

           this.onsubmitNegociation();
                // const dialogRef3 = this.dialog.open(DialogEnNegociation, {
                //     panelClass: 'event-form-dialog',
                //     data      : {
                //         paramPartId : this.routeParams._value.id,
                //     }
                // });
                //
                // dialogRef3.afterClosed().subscribe(result => {
                //     console.log('The dialog was closed');
                //     this.animal = result;
                // });
                break;
            case '1':
                this.onsubmitProspect();
                break;
        }


    }

    onsubmitProspect() {

        let payload = {};

        payload["vente_devis_id"]  = this.devis_id;
        payload["type"]= -1;
        payload["statut"]= 1;


        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((clientPerdu: any) =>
        {
            try
            {
                this._matSnackBar.open('Le client est devenu un Prospec ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                setTimeout(() => {this.ngOnInit();}, 1000);
                // this.router.navigate(['/gestion-project/projects']);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de modification','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        },
            (error: any) =>{
                this._matSnackBar.open(""+ error.error.message,'',{
                    verticalPosition : 'top',
                    duration : 4000,
                    panelClass :['error-snackbar'],
                })
            }

        );

    }



    onsubmitNegociation() {

        let payload = {};

        payload["vente_devis_id"]  = this.devis_id;
        payload["type"]= -1;
        payload["statut"]= 4;


        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((clientPerdu: any) =>
        {
            try
            {
                this._matSnackBar.open(' on est on négociation avec le client ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                setTimeout(() => {this.ngOnInit();}, 1000);
                // this.router.navigate(['/gestion-project/projects']);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de modification','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        });

    }

    touchDate(event){

        console.log(event);

            $('.date-picker').datepicker( {
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });

    }
    ngOnInit() : void
    {


        this.partnerService.onPartnertChanged
            .subscribe(partner => {
                if ( partner )
                {
                    console.log(partner);
                    this.partner = partner;
                    this.pageType = 'edit';
                }
                else
                {
                    this.pageType = 'new';

                }
                this.appelForm = this.createAppelForm();
                this.visiteForm = this.createVisiteForm();
                this.emailForm = this.creatEmailForm();
                this.smsForm = this.createSmsForm();
                this.dateForm = this.createDateForm();
                this.documentForm = this.createDocumentForm();

            });

        this.partnerService.getPartnerTest().subscribe(partner => {
            this.partner = partner
        });

        this.getPartner();
        // todo
        this.partnerService.getDocument(this.routeParams._value.id).subscribe( documents =>{
            console.log(documents);
            this.attachments= [];
            for (var i = 0; i < documents.length; i++){

                if (documents[i].doc != null )
                {
                    this.attachments.push(documents[i]);
                }
            }

            console.log(this.attachments);
        });
        let params = {
            type: 1,
        };
        this.partnerService.LoadStatus(params).subscribe( status =>{
            console.log(status.data);
            this.status = status.data;
        });

    }

    donwload(attachement){

        window.open(attachement,"_self")
    }

    getPartner() :void{
        this.partnerService.getPartnerDetail().subscribe( partners =>{
            console.log(partners.data);
            let objectParser = partners.data;
            for (let i = 0; i < objectParser.length; i++){
                console.log(objectParser[i]);
                if (objectParser[i].action_type == null) {
                    objectParser[i].action_type = false;
                }
            }
            this.partnerDetail = objectParser;
        });
    }


    toggleChange(event)
    {

        let checked = event.checked;
        if (checked) {
            $("#date_selected").show();
            console.log("show element");

        }
        else
        {
            console.log('hide');
            $("#date_selected").hide();
            this.partnerForm.get('next_visite').reset('');
        }

    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }



    onSubmitAppel(){

        let payload = {};

        // let nextVisiteFormated = moment(this.myform.get('next_visite').value).format('YYYY-MM-DD');


        payload["object"]  = this.appelForm.get('object').value;
        payload["description"]  = this.appelForm.get('observation').value;
        payload["vente_devis_id"]  =this.devis_id;
        payload["type"]= 1;
        payload["statut"]= 1;

        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((projectPost: any) =>

        {
            try
            {
                this._matSnackBar.open('Appel créer avec succés ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                setTimeout(() => {this.ngOnInit();}, 1000);
                // this.router.navigate(['/gestion-project/projects']);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de l\'ajout de l\'appelant','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        });
    }

    close()
    {
        let element:HTMLElement;
        element = document.getElementById("myModal");
        element.style.display = "none";

    }
    openModal(event)
    {

        console.log(event.target);
        let element:HTMLElement;
        element = document.getElementById("myModal");
// Get the image and insert it inside the modal - use its "alt" text as a caption

        this.divView.nativeElement.src = (event.target as HTMLImageElement).src;
        element.style.display = "block";

    }

    onSubmitVisite()
    {

        let payload = {};

        // let nextVisiteFormated = moment(this.myform.get('next_visite').value).format('YYYY-MM-DD');

        payload["vente_devis_id"]  =this.devis_id;
        payload["type"]= 2;
        payload["statut"]= 1;
        payload["article_id"]  = this.visiteForm.get('appart_viste').value;
        payload["price"]  = this.visiteForm.get('budget').value;
        payload["area"]  = this.visiteForm.get('surface').value;
        payload["unite_id"]  = this.visiteForm.get('type').value;
        payload["description"]  = this.visiteForm.get('observation').value;

        // payload["visite_prochaine_prevu"]  = this.myform.get('visite_prochaine_prevu').value;;
        // payload["date_planif"]  = nextVisiteFormated;
        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((projectPost: any) =>

        {
            try
            {
                this._matSnackBar.open('Visite créer avec succés ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                setTimeout(() => {this.ngOnInit();}, 1000);
                // this.router.navigate(['/gestion-project/projects']);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de l\'ajout de la visite','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        });
    }
    onSubmitEmail(){
        let payload = {};

        // let nextVisiteFormated = moment(this.myform.get('next_visite').value).format('YYYY-MM-DD');
        payload["statut"]= 1;
        payload["type"]= 3;
        payload["vente_devis_id"]  =this.devis_id;

        payload["object"]  = this.emailForm.get('object').value;
        payload["description"]  = this.emailForm.get('message').value;




        // payload["visite_prochaine_prevu"]  = this.myform.get('visite_prochaine_prevu').value;;
        // payload["date_planif"]  = nextVisiteFormated;
        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((projectPost: any) =>

        {
            try
            {
                this._matSnackBar.open('Email Créer avec succés ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                setTimeout(() => {this.ngOnInit();}, 1000);
                // this.router.navigate(['/gestion-project/projects']);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de l\'ajout de email','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        });

    }

    onSubmitDate(){
        let payload = {};
        console.log(this.partner);
           if(this.partner.statut =='1')
           {
               payload["color"]= '#cddc39';
           }
           else if (this.partner.statut == '2')
           {
               payload["color"]= '#0DFF17';
           }
           else if (this.partner.statut =='3')
           {
               payload["color"]= '#ff6c16';
           }
           else if (this.partner.statut =='4')
           {
               payload["color"]= '#00bcd4';
           }
           else
           {
               payload["color"]= '#d40013';
           }
        payload["project_id"]= this.partner.projet_id;
        payload["object"]  = this.dateForm.get('object').value;
        let nextVisiteFormated = moment(this.dateForm.get('next_visite').value).format('YYYY-MM-DD');
        payload["date_planif"]  = nextVisiteFormated;
        payload["part_id"]  =this.routeParams._value.id;

        console.log(payload);
        // payload["statut"]= 1;
        // payload["type"]= 3;
        // payload["vente_devis_id"]  =this.routeParams._value.id;
        //
        // payload["description"]  = this.emailForm.get('message').value;




        // payload["visite_prochaine_prevu"]  = this.myform.get('visite_prochaine_prevu').value;;

        this.partnerService.addNextCalendar(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((projectPost: any) =>
        {
            try
            {
                this._matSnackBar.open('date de visite prochaine Créer avec succés ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                this.dateForm.reset();
                //setTimeout(() => {this.ngOnInit();}, 1000);
                this.router.navigate(['/calendar']);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de l\'ajout de la viste prochaine','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        });

    }
    onSubmitSms(){
        let payload = {};


        // let nextVisiteFormated = moment(this.myform.get('next_visite').value).format('YYYY-MM-DD');
        payload["vente_devis_id"]  =this.devis_id;
        payload["statut"]= 1;
        payload["type"]  = 4;
        payload["description"]  = this.smsForm.get('message').value;


        // payload["visite_prochaine_prevu"]  = this.myform.get('visite_prochaine_prevu').value;;
        // payload["date_planif"]  = nextVisiteFormated;
        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((projectPost: any) =>

        {
            try
            {
                this._matSnackBar.open('Sms Créer avec succés ', '', {
                    verticalPosition: 'top',
                    duration        : 2000,
                    panelClass: ['success-snackbar']
                });
                setTimeout(() => {this.ngOnInit();}, 1000);
                // this.router.navigate(['/gestion-project/projects']);
            }
            catch (e)
            {
                this._matSnackBar.open('Erreur lors de l\'ajout de sms','',{
                    verticalPosition : 'top',
                    duration: 2000,
                    panelClass :['error-snackbar']
                });
            }
        });

    }

    createAppelForm(): FormGroup
    {
        return this._formBuilder.group({
            object :[''],
            observation :['',Validators.required],
        });
    }
    createVisiteForm(): FormGroup
    {
        return this._formBuilder.group({
            appart_viste              : ['',Validators.required],
            budget            : [''],
            surface            : [''],
            type            : [''],
            observation : [''],
        });
    }
    creatEmailForm(): FormGroup
    {

        return this._formBuilder.group({
            object            : [''],
            message            : [''],
        });
    }
    createDateForm(): FormGroup
    {

        return this._formBuilder.group({
            object            : [''],
            next_visite            : ['',Validators.required],
        });
    }
    createDocumentForm(): FormGroup
    {
        return this._formBuilder.group({
            doc            : ['',Validators.required],
        });
    }
    createSmsForm(): FormGroup
    {
        return this._formBuilder.group({
            message            : ['',Validators.required],
        });
    }


}

@Component({
    selector: 'dialog-perdu',
    templateUrl: 'dialog-perdu.html',
    styleUrls: ['./partner-show.component.scss']
})
export class DialogPerdu implements AfterViewInit {
    contactPerdu :FormGroup;
    partId : any;
    private _unsubscribeAll: Subject<any>;
    @ViewChild(PartnerShowComponent)
    private partner_show: PartnerShowComponent;

    test : any;


    constructor( private _formBuilder: FormBuilder,
                 public dialogRef: MatDialogRef<DialogPerdu>,
                 private _matSnackBar: MatSnackBar,
                 private router:Router,
                 @Inject(MAT_DIALOG_DATA) public data: any,private partnerService: PartnerService) {
        this.partId= data.paramPartId;
        this._unsubscribeAll = new Subject();
        this.contactPerdu = this.createContactForm();

    }
    createContactForm(): FormGroup
    {
        return this._formBuilder.group({
            object :['',Validators.required],
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }


    onSubmitPerdu() {

        let payload = {};

        payload["vente_devis_id"]  =this.partId;
        payload["type"]= -1;
        payload["object"]= this.contactPerdu.get('object').value;
        payload["statut"]= -1;


        this.partnerService.addPartnerDetail(payload).pipe(takeUntil(this._unsubscribeAll)).subscribe((clientPerdu: any) =>
            {
                try
                {
                    this._matSnackBar.open('Le client est Perdu Bye Bye ', '', {
                        verticalPosition: 'top',
                        duration        : 2000,
                        panelClass: ['success-snackbar']
                    });

                    //this.partner_show.ngOnInit();
                    setTimeout(() => {this.partner_show.getPartner();}, 1000);

                    // setTimeout(() => {this.ngOnInit();}, 1000);
                    this.router.navigate(['partners',this.partId]);
                }
                catch (e)
                {
                    this._matSnackBar.open('Erreur lors de modification','',{
                        verticalPosition : 'top',
                        duration: 2000,
                        panelClass :['error-snackbar']
                    });
                }
            },
            (error: any) =>{
                console.log(error);
                this._matSnackBar.open(""+ error.error.message,'',{
                    verticalPosition : 'top',
                    duration : 4000,
                    panelClass :['error-snackbar'],
                })
            }
        );

    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    ngAfterViewInit(): void {

        this.test = this.partner_show.getPartner();
        console.log("test allah allah");
    }

}
