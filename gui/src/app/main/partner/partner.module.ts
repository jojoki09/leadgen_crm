import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PartnerActionFormComponent} from "./partner-action-form/partner-action-form.component";
import {PartnerListComponent} from "./partner-list/partner-list.component";
import {RouterModule, Routes} from "@angular/router";
import {DialogClient, DialogPerdu, PartnerShowComponent} from './partner-show/partner-show.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialUiModule} from "../material-ui/material-ui.module";
import {ConfirmDialogModule} from "../administration/confirm-dialog/confirm-dialog.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {GestionProjectsModule} from "../gestion-project/gestion-projects.module";
import {PartnerService} from "./partner.service";
import {  FuseSidebarModule} from '@fuse/components';
import { RelanceComponent } from './relance/relance.component';
const routes: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: "full"
    },
    {
        path        : 'list',
        component:  PartnerListComponent,
    },
    {
        path        : 'action-form',
        component:  PartnerActionFormComponent,
    },
    {
        path        : 'relance',
        component:  RelanceComponent,
    },
    {
        path        : ':id',
        component:  PartnerShowComponent,
        resolve  : {
            data: PartnerService,
        }

    }
];
@NgModule({
  declarations: [PartnerActionFormComponent, PartnerListComponent, PartnerShowComponent,DialogPerdu,DialogClient, RelanceComponent],
  imports: [
      GestionProjectsModule,
      CommonModule,
      RouterModule.forChild(routes),
      FuseSidebarModule,
      ReactiveFormsModule,
      MaterialUiModule,
      FlexLayoutModule,
      FormsModule,
      ConfirmDialogModule,
  ],
    entryComponents: [DialogPerdu,DialogClient],

})
export class PartnerModule { }
