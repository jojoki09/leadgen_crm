import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ProductsComponent} from "./projects/products.component";
import {ProductsService} from "./projects/products.service";
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';
import { FlexLayoutModule } from "@angular/flex-layout";
import {
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatMenuModule,
    MatCheckboxModule,
    MatDialogModule, MatRadioModule, MatDividerModule, MatToolbarModule, MatTooltipModule
} from "@angular/material";

import {NgxChartsModule} from "@swimlane/ngx-charts";
import { ProjectComponent } from './project/project.component';
import {ProjectService} from "./project/project.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CategoriesComponent } from './categories/categories.component';
const routes: Routes = [
    {
        path     : 'projects',
        component: ProductsComponent,
    },
    {
        path     : 'projects/:id',
        component: ProjectComponent,
        resolve  : {
            data: ProjectService,
        }
    },
    {
        path     : 'projects/:id/:label',
        component: ProjectComponent,
        resolve  : {
            data: ProjectService,
        }
    }

  ];
@NgModule({
  declarations: [
      ProductsComponent,
      ProjectComponent,
      CategoriesComponent,
  ],
  imports: [
      RouterModule.forChild(routes),
      ReactiveFormsModule,
      MatButtonModule,
      MatChipsModule,
      MatExpansionModule,
      MatFormFieldModule,
      MatIconModule,
      MatInputModule,
      MatPaginatorModule,
      MatRippleModule,
      MatSelectModule,
      MatSortModule,
      MatSnackBarModule,
      MatTableModule,
      MatTabsModule,
      NgxChartsModule,
      MatMenuModule,
      MatCheckboxModule,
      MatDialogModule,
      MatRadioModule,
      FuseConfirmDialogModule,
      FuseSidebarModule,
      FormsModule,
      MatDividerModule,
      MatToolbarModule,
      FlexLayoutModule,
      MatTooltipModule,

  ],
    providers   : [
        ProductsService,
        ProjectService,
        ProductsComponent,
        ProjectComponent,
    ],
    entryComponents: [
        CategoriesComponent
    ]
})
export class GestionProjectsModule { }
