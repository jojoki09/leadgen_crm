import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CategoriesComponent {

    action: string;
    contact: any;
    contactForm: FormGroup;
    dialogTitle: string;

    constructor(private router: Router,public matDialogRef: MatDialogRef<CategoriesComponent>, @Inject(MAT_DIALOG_DATA) private _data: any, private _formBuilder: FormBuilder)
    {
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'MODIFIER CATEGORIE';
            this.contact = _data.contact;
        }
        else
        {
            this.dialogTitle = 'AJOUTER CATEGORIE';
        }

        this.contactForm = this.createContactForm();
    }

    createContactForm(): FormGroup
    {
        return this._formBuilder.group({
            id               : [''],
            nom              : [''],
            description      : [''],
        });
    }

}
