import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import {Projects} from "./projects";
import { environment } from 'environments/environment';
import {catchError, tap} from "rxjs/operators";

@Injectable()
export class ProductsService  {

    private ApiUrl =  `${environment["API_URL"]}`;
    products: any[];
    onProductsChanged: BehaviorSubject<any>;
  constructor(  private http:HttpClient )
  {
      this.onProductsChanged = new BehaviorSubject({});

  }

    // resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    // {
    //     return new Promise((resolve, reject) => {
    //
    //         Promise.all([
    //             this.getProducts()
    //         ]).then(
    //             () => {
    //                 resolve();
    //             },
    //             reject
    //         );
    //     });
    // }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    // getUsers(): Observable<any> {
    //     return this.http.get(`${this.apiUrl}users`)
    //         .pipe(
    //             catchError(this.handleError('getUsers', [])),
    //             tap((users: any[]) => this._users.next(users))
    //         );
    // }

  getProjects(): Observable<any>
  {
      return this.http.get<any>(this.ApiUrl+'projects',{});
  }

  deleteProject(id) : Observable<Projects>
  {
    return this.http.delete<Projects>(this.ApiUrl+'projects/'+id,{});
  }

}
