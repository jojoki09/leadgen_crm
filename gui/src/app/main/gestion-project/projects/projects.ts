export class Projects {
    'id':number;
    'code' : string;
    'nom':string;
    'part_ref':string;
    'part_id' : number;
    'societe_id' : number;
    'adresse_id': number;
    'contact_id':number;
    'employe_id':number;
    'date_ouverture':string;
    'date_cloture':string;
    'categorie_id': number;
    'facturation':number;
    'typ' : number;
    'etat':number;
    'files_id':number;
    'created_by':string;
    'update_by':string;
    'created_at':string;
    'updated_at':string;

}
