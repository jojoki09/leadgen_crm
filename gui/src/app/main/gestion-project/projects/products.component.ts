import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import { Subject } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {ProductsService} from "./products.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  animations   : fuseAnimations,
})
export class ProductsComponent implements OnInit {
    @ViewChild("img01") divView: ElementRef;

    dataSource: any;
    product:any;
    displayedColumns = ['nom','photo','categories','villes', 'created_by', 'date_ouverture', 'etat', 'buttons'];
    dialogRef: any;
    totalSize = 0;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @ViewChild('filter')
    filter: ElementRef;

    private _unsubscribeAll: Subject<any>;

    constructor(private productsService : ProductsService, private http: HttpClient,private router:Router,public _matDialog: MatDialog,private _matSnackBar: MatSnackBar)
    {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit( ) {
        this.getProduit();
    }

    getProduit(): void {
        this.productsService.getProjects().subscribe(projects => {

            console.log(projects);
            this.dataSource = new MatTableDataSource<Element>(projects);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.product = projects;
            this.totalSize = this.product.length;
        });
    }

    close()
    {
        let element:HTMLElement;
        element = document.getElementById("myModal");
        element.style.display = "none";

    }
    openModal(event)
    {

        console.log(event.target);
        let element:HTMLElement;
        element = document.getElementById("myModal");
// Get the image and insert it inside the modal - use its "alt" text as a caption

        this.divView.nativeElement.src = (event.target as HTMLImageElement).src;
        element.style.display = "block";

    }


    open(id,label)
    {
        this.router.navigate(['/main/gestion-produits/products/'+id+'/'+label]);
    }

    actualiser()
    {
        this.ngOnInit();
    }

    delete(id)
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûr de vouloir supprimer ce projet?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.productsService.deleteProject(id).subscribe(product=>{
                    try {
                        this._matSnackBar.open('projet supprimer avec succès','',{
                            verticalPosition : 'top',
                            duration:2000,
                            panelClass: ['success-snackbar']
                        });
                        setTimeout(() => {this.ngOnInit();}, 1000);
                        this.ngOnInit();

                    }
                    catch (e) {
                        this._matSnackBar.open('Erreur lors de suppression de produit','',{
                            verticalPosition : 'top',
                            duration:2000,
                            panelClass: ['error-snackbar']
                        })
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

    searchReference(event : any)
    {
        let reference = event.target.value;
        const regext = new RegExp(`(\w+)?${reference}(\w+)?`,'i');
        if(reference!="")
        {
            this.dataSource=this.product.filter(product=>regext.test(product.reference));
        }
        else
        {
            this.ngOnInit();
        }
    }

    searchLibelle(event:any)
    {
        let label = event.target.value;
        const regext = new RegExp(`(\w+)?${label}(\w+)?`,'i');
        if(label!="")
        {
            this.dataSource=this.product.filter(product=>regext.test(product.label));
        }
        else
        {
            this.ngOnInit();
        }
    }

    searchSellPrice(event:any)
    {
        let sell_price=event.target.value;
        const regext = new RegExp(`(\w+)?${sell_price}(\w+)?`,'i');
        if(sell_price!="")
        {
            this.dataSource = new MatTableDataSource<Element>(this.product.filter(product=>regext.test(product.sell_price)));
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.totalSize = this.product.length;
        }
        else
        {
            this.ngOnInit();
        }
    }

    searchTva(event:any)
    {
        let tva=event.target.value;
        const regext = new RegExp(`(\w+)?${tva}(\w+)?`,'i');
        if(tva!="")
        {
            this.dataSource = new MatTableDataSource<Element>(this.product.filter(product=>regext.test(product.tva)));
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.totalSize = this.product.length;
        }
        else
        {
            this.ngOnInit();
        }
    }

    searchProvider(event:any)
    {
        let nom=event.target.value;
        const regext = new RegExp(`(\w+)?${nom}(\w+)?`,'i');
        if(nom!="")
        {
            this.dataSource = new MatTableDataSource<Element>(this.product.filter(product=>regext.test(product.nom)));
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.totalSize = this.product.length;
        }
        else
        {
            this.ngOnInit();
        }
    }

    searchEtatVente(event:any)
    {
        let etatV=event.value;
        const regext = new RegExp(`(\d+)?${etatV}(\d+)?`,'i');
        if(etatV!="")
        {
            this.dataSource = new MatTableDataSource<Element>(this.product.filter(product => product.tobuy == (etatV)));
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.totalSize = this.product.length;
        }
        else
        {
            this.ngOnInit();
        }
    }

    searchEtatAchat(event:any)
    {
        let etatA=event.value;
        const regext = new RegExp(`(\d+)?${etatA}(\d+)?`,'i');
        if(etatA!="")
        {
            this.dataSource = new MatTableDataSource<Element>(this.product.filter(product => product.tobuy == (etatA)));
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.totalSize = this.product.length;
        }
        else
        {
            this.ngOnInit();
        }
    }
}


