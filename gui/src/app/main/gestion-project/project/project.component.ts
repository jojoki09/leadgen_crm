import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Project} from "./projects.model";
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {ProjectService} from "./project.service";
import { fuseAnimations } from '@fuse/animations';
import {MatDialog, MatDialogRef, MatSnackBar} from "@angular/material";
import {Router} from "@angular/router";
import {ProductsComponent} from "../projects/products.component";
import * as $ from 'jquery';
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {CategoriesComponent} from "../categories/categories.component";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  animations   : fuseAnimations,
})
export class ProjectComponent implements OnInit {

    project: any;
    produitPost:any;
    pageType: string;
    categorieData:any;
    selectedCity: any;
    cities: any;
    providerData:any;
    idProduit:any;
    myform: FormGroup;
    selectedProvider:any;
    selectedCategory : any;
    dialogRef: any;
    idProducts : any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    private _unsubscribeAll: Subject<any>;


    constructor(private projectService: ProjectService, private _formBuilder: FormBuilder, private _matSnackBar: MatSnackBar, private router:Router, private pc:ProductsComponent, public _matDialog: MatDialog) {

        this._unsubscribeAll = new Subject();
        this.project = new Project();

    }

    ngOnInit() : void
    {

      // this.getProviders();
      // this.getIdProducts();

      this.getCities();
      this.getCategories();

      this.projectService.onProjectChanged
          .subscribe(project => {
              if ( project )
              {
                  this.project = project;
                  this.pageType = 'edit';
              }
              else
              {
                  this.pageType = 'new';
                  this.project = new Project();
              }
              this.myform = this.createProjectForm();

          });
    }

    createProjectForm(): FormGroup
    {

        return this._formBuilder.group({
            nom: ['',Validators.required],
            ville_id: ['',Validators.required],
            categorie_id: ['',Validators.required],
        });
    }


    public hasError = (controlName: string, errorName: string) =>{
        return this.myform.controls[controlName].hasError(errorName);
    }

    getCategories() {
        this.projectService.getCategories().subscribe( Categoriedata =>{

            console.log(Categoriedata.data);
            this.categorieData = Categoriedata.data;
            this.selectedCategory = this.categorieData;
        });
    }
    getCities() {

        this.projectService.getCities().subscribe( Cities =>{
            console.log(Cities);
            this.cities = Cities;
            this.selectedCity = Cities;
        });
    }



    getIdProduct() {
        this.projectService.getIdProduct().subscribe( idProduit=> {
            this.idProduit = idProduit + 1;
        });
        if (this.idProduit == null) {
            this.idProduit = 0;
            this.idProduit = this.idProduit + 1;
        }
        else
            this.idProduit = this.idProduit + 1;
    }

    searchCity(event:any)
    {
        let provider=event.target.value;
        const regext=new RegExp(`(\w)?${provider}(\w+)?`,'i');
        if(provider!="")
        {
            this.selectedCity=this.selectedCity.filter(cityData=>regext.test(cityData.nom));
        }
        else
            this.selectedCity=this.cities;
    }

    searchCategory(event:any)
    {
        let category =event.target.value;
        console.log(category);
        const regext=new RegExp(`(\w)?${category}(\w+)?`,'i');
        if(category!="")
        {
            console.log(this.categorieData);
            this.selectedCategory = this.categorieData.filter(categorieData=>regext.test(categorieData.nom));
        }
        else
            this.selectedCategory=this.categorieData;
    }

    open()
    {
        this.router.navigate(['./main/gestion-tiers/fournisseurs/new']);
    }

    getIdProducts()
    {
        this.projectService.getIdProduct().subscribe(response=> {
            this.idProducts = response + 1;
        });
        if (this.idProducts == null) {
            this.idProducts = 0;
            this.idProducts = this.idProducts + 1;
        }
        else
            this.idProducts = this.idProducts + 1;
    }

    onSubmit(nom,ville_id,category_id)
    {
        var formInformation = Object.assign({},
            this.myform.value || {}
        );

      console.log(formInformation) ;
        let formData: FormData = new FormData();


        for(let prop in formInformation) {
            if(formInformation[prop] === "") {
                delete formInformation[prop];
                continue;
            }

            // prepare form data object
            formData.append(prop, formInformation[prop]);

        }



        let photo: any = document.querySelector('#photo');

        var files = photo.files;

        let fileList: FileList = files;


        if (fileList.length > 0) {
            let file: File = fileList[0];
            formData.append('photo', file, file.name);
        }
         console.log(formData);






        if(nom !='' )
        {
            this.projectService.storeProject(formData).pipe(takeUntil(this._unsubscribeAll)).subscribe((projectPost: any) =>

            {
                try
                {
                    this._matSnackBar.open('Projet Ajouter avec succés ', '', {
                        verticalPosition: 'top',
                        duration        : 2000,
                        panelClass: ['success-snackbar']
                    });
                    setTimeout(() => {this.ngOnInit();}, 1000);
                    this.pc.ngOnInit();
                    this.router.navigate(['/gestion-project/projects']);
                }
                catch (e)
                {
                    this._matSnackBar.open('Erreur lors de l\'ajout de produit','',{
                        verticalPosition : 'top',
                        duration: 2000,
                        panelClass :['error-snackbar']
                    });
                }
            },
                (error: any) =>{
                    this._matSnackBar.open(""+ error.error.message,'',{
                        verticalPosition : 'top',
                        duration : 4000,
                        panelClass :['error-snackbar'],
                    })
                } );
        }
        else
        {
            this._matSnackBar.open('Veuillez remplire tout les champs obligatoire','',{
                verticalPosition : 'top',
                duration : 2000,
                panelClass :['error-snackbar'],
            })
        }
       }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onUpdate(id,code,reference,label,toBuy,toSell,description,description_facture,marque,provider_id,category_id,weight,weight_unit,width,width_unit,height,height_unit,surface,surface_unit,volume,volume_unit,seuil_stock_alert)
    {
        this.produitPost = new Project();
        this.produitPost.id =id;
        this.produitPost.provider_id = provider_id;
        this.produitPost.category_id = category_id;
        this.produitPost.code_product = code;


        if(label !='')
        {
            this.projectService.updateProduct(this.produitPost).subscribe(produitpost=>
            {
                try
                {
                    this._matSnackBar.open('Produit modifier avec succés ', '', {
                        verticalPosition: 'top',
                        duration        : 2000,
                        panelClass: ['success-snackbar']
                    });
                    setTimeout(() => {this.ngOnInit();}, 1000);
                    this.pc.ngOnInit();
                    this.router.navigate(['/gestion-produits/products']);
                }
                catch (e)
                {
                    this._matSnackBar.open('Erreur lors de modification de produit','',{
                        verticalPosition : 'top',
                        duration: 2000,
                        panelClass :['error-snackbar']
                    });
                }
            });
        }
        else
        {
            this._matSnackBar.open('Veuillez remplire tout les champs obligatoire','',{
                verticalPosition : 'top',
                duration : 2000,
                panelClass :['error-snackbar'],
            })
        }
    }


    editCategorie(): void
    {
        this.dialogRef = this._matDialog.open(CategoriesComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                action : 'Add'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                console.log(response);


                console.log("============================================================");

                console.log(response.getRawValue());

                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];

                this.projectService.storeCategorie(response.getRawValue()).subscribe(productCategories=>
                {
                    try
                    {
                        this._matSnackBar.open('Categorie ajouter avec succès','',{
                            verticalPosition : 'top',
                            duration: 2000,
                            panelClass :['success-snackbar']
                        });
                       this.getCategories();


                    }
                    catch (e)
                    {
                        this._matSnackBar.open('Erreur lors de l\'ajout de categorie','',{
                            verticalPosition : 'top',
                            duration: 2000,
                            panelClass :['error-snackbar']
                        });
                    }
                })
            });
    }

    deleteCategorie(id)
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });


        this.confirmDialogRef.componentInstance.confirmMessage = 'Etes-vous sûr de vouloir supprimer cette categorie?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.projectService.deleteCategorie(id).subscribe(categorie=>{
                    try {
                        this._matSnackBar.open('Categorie supprimer avec succès','',{
                            verticalPosition : 'top',
                            duration:2000,
                            panelClass: ['success-snackbar']
                        });
                        setTimeout(() => {this.ngOnInit();}, 1000);
                        this.getCategories();

                    }
                    catch (e) {
                        this._matSnackBar.open('Erreur lors de suppression de categorie','',{
                            verticalPosition : 'top',
                            duration:2000,
                            panelClass: ['error-snackbar']
                        })
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }

}
