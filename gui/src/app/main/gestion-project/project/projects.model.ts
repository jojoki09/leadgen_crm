
export class Project {
    id: number;
    provider_id : number;
    category_id:number;
    label: string;
    description: string;
    description_facture:string;
    brand:string;
    tva:number;
    seuil_stock_alert: number;
    in_bulk : number;
    buy_price:number;
    buy_price_ttc:number;
    buy_price_min:number;
    buy_price_min_ttc:number;
    sell_price:number;
    sell_price_ttc:number;
    sell_price_min:number;
    sell_price_min_ttc:number;
    price_base_type:string;
    weight:number;
    weight_unit:string;
    width:number;
    width_unit:string;
    height:number;
    height_unit:string;
    surface:number;
    surface_unit:string;
    volume:number;
    volume_unit:string;
}
