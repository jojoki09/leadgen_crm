import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import {Projects} from "../projects/projects";
import {Categories} from "../categories/product-categories";
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

    routeParams: any;
    project: any;
    onProjectChanged: BehaviorSubject<any>;

    private ApiUrl =  `${environment["API_URL"]}`;

    constructor( private http:HttpClient ) {
        this.onProjectChanged = new BehaviorSubject([]);

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProject(),
                this.getCategories(),
                this.getCities()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getCities(): Observable<any> {

      return  this.http.get(this.ApiUrl+'cities',{});

    }
    getSources(): Observable<any> {

      return  this.http.get(this.ApiUrl+'sources',{});

    }

    getCategories(): Observable<any> {

      return  this.http.get(this.ApiUrl+'categorie',{});

    }

    getProject(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {

                this.onProjectChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get<any>(this.ApiUrl+'projects'+'/getProductById/'+ this.routeParams.id)
                    .subscribe((response: any) => {
                        this.project = response;
                        this.onProjectChanged.next(this.project);
                        resolve(response);
                    }, reject);
            }
        });
    }


    getProviders(): Observable<any>
    {
        return this.http.get<any>(this.ApiUrl+'products'+'/getProviders',{});
    }

    getIdProduct(): Observable<any>
    {
        return this.http.get<any>(this.ApiUrl+'products'+'/getIdProduit',{});
    }

    storeProject(project: any): Observable<any>
    {
        return this.http.post<any>(this.ApiUrl+'projects',project);
    }

    updateProduct (produit: Projects): Observable<Projects>
    {
        return this.http.post<Projects>(this.ApiUrl+'products'+'/updateProduct', produit);
    }

    storeCategorie(Categorie: Categories): Observable<Categories>
    {
        return this.http.post<Categories>(this.ApiUrl+'categorie',Categorie);
    }

    deleteCategorie(id) : Observable<Categories>
    {
        return this.http.delete<Categories>(this.ApiUrl+'products'+'/deleteCategorie/'+id,{});
    }

    getIdProjects()
    {
        return this.http.get<any>(this.ApiUrl+'products/getIdProjects',{});
    }


    getInstalledProjectsByClientId(client_id: number) {
        return this.http.get(`${this.ApiUrl}getInstalledProjectsByClientId/${client_id}`);
    }


}
