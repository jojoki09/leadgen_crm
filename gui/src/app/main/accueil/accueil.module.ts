import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatButtonModule,
    MatDividerModule, MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatTableModule, MatTabsModule
} from "@angular/material";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {FuseSharedModule} from "../../../@fuse/shared.module";
import {FuseSidebarModule, FuseWidgetModule} from "../../../@fuse/components";
import {RouterModule, Routes} from "@angular/router";
import {AccueilComponent} from "./accueil.component";

const routes: Routes = [
    {
        path     : '**',
        component: AccueilComponent,
    }
];


@NgModule({
  declarations: [AccueilComponent],
  imports: [
      RouterModule.forChild(routes),
      CommonModule,
      MatButtonModule,
      MatDividerModule,
      MatFormFieldModule,
      MatIconModule,
      MatMenuModule,
      MatSelectModule,
      MatTableModule,
      MatTabsModule,
      NgxChartsModule,
      FuseSharedModule,
      FuseSidebarModule,
      FuseWidgetModule
  ]
})
export class AccueilModule { }
