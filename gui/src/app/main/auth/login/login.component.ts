import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/services/auth.service';
import { LogService } from 'app/services/log.service';
import { TokenService } from 'app/services/token.service';
import { Router } from '@angular/router';
import { User } from 'app/main/administration/users/user.model';
import { UsersService } from 'app/main/administration/users/users.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class LoginComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;
  navigateAfterLogin = "/home";
  loginForm: FormGroup;



  alert: { success?: 0 | 1, message?: string, hasError: boolean} = {
    hasError: false
  };

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _authService: AuthService,
        private Token: TokenService,
        private _usersService: UsersService,
        private router: Router,
    )
    {
      this._unsubscribeAll = new Subject();
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };


    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.loginForm = this._formBuilder.group({
            email   : ['', [Validators.required, /* Validators.email */]],
            password: ['', Validators.required]
        });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onLogin() {

      if(!this.loginForm.valid)
        return;


      // authenticate set the token on the local storage update authStat

      this._authService.authenticate(this.loginForm.getRawValue()).pipe(takeUntil(this._unsubscribeAll)).subscribe((_: {ok: number, token?: string,user_id: string, error?:string}) => {
        if(_.ok) {
                                                                                              // every thing is ok keep the token and redirect
                                                                                              /* console.log({token: _.token}); */
          console.log(_);
          const $token = _.token;
          const $user_id= _.user_id;

          this.Token.set($token);
          localStorage.setItem("user_id", $user_id);

          this._authService.changeAuthStatus(true);
          this.router.navigateByUrl(this.navigateAfterLogin);

          this._authService.getAuthenticatedUser().subscribe();


        } else {
          this.alert = {
            hasError: true,
            message: _.error,
            success: 0
          }

          setTimeout(() => {
            this.alert = {
              hasError: false,
            }
          }, 3500);
        }
      });


    }

}
