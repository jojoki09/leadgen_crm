import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'app/main/administration/confirm-dialog/confirm-dialog.module';
import { MaterialUiModule } from '../material-ui/material-ui.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FuseSharedModule } from '@fuse/shared.module';
import { VerifyEmailGuard } from 'app/services/route-guards/verify-email.guard';
import { AfterLoginGuard } from 'app/services/route-guards/after-login.guard';

const routes: Routes = [
  { 
    
    path: '',  
    redirectTo: 'login', 
    pathMatch: 'full',
  },
  { 
    path: 'login', 
    canActivate: [AfterLoginGuard],
    component: LoginComponent
  },
  { 
    path: 'request/verify-email',  
    canActivate: [VerifyEmailGuard]
  },
  
];


@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    
    // load material design style
    MaterialUiModule,
    // flexlayout for repensive design 
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDialogModule,

    FuseSharedModule,
  ]
})
export class AuthModule { }
