///<reference path="../../node_modules/@angular/platform-browser/animations/src/module.d.ts"/>
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';
import { FakeDbService } from 'app/fake-db/fake-db.service';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { AuthGuard } from './services/route-guards/auth.guard';
import { JwtInterceptor } from './main/interceptors/jwt-interceptor';
import { AuthService } from './services/auth.service';
import { environment } from 'environments/environment.prod';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import {LoggerModule, NgxLoggerLevel} from "ngx-logger";


const appRoutes: Routes = [

    // {
    //     path: '',
    //     redirectTo: 'auth',
    //     pathMatch: 'full'
    // },
    {
        path: 'management_tools',
        canActivate: [AuthGuard],
        loadChildren:  './main/administration/administration.module#AdministrationModule'  /* () =>  AdministrationModule */ ,
    },
    {
        path: "auth",
        /* canActivate: [AfterLoginGuard], */
        loadChildren:  './main/auth/auth.module#AuthModule'  /* () => AuthModule */,
    },
    {
        path        : '',
        canActivate: [AuthGuard],
        loadChildren:  './main/main.module#MainModule'  /* () => MainModule */,
    }
];

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports     : [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,

        LoggerModule.forRoot({serverLoggingUrl: `${environment["API_URL"]}logs`, level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR}),
    ],
    bootstrap   : [
        AppComponent
    ],
    providers: [
        AuthService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
    ]
})
export class AppModule
{

}
