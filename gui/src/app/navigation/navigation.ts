import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'dashboards',
        title    : 'Dashboards',
        translate: 'NAV.DASHBOARDS',
        type     : 'collapsable',
        icon     : 'dashboard',
        children : [
            {
                id   : 'analytics',
                title: 'Analytics',
                type : 'item',
                url  : '/dashboards/analytics'
            },
            {
                id   : 'project',
                title: 'Project',
                type : 'item',
                url  : '/dashboards/project'
            }
        ]
    },

    {
        id       : 'admin',
        title    : 'Administration',
        type     : 'group',
        children : [
            {
                id       : 'administration',
                title    : 'Administration',
                type     : 'collapsable',
                icon     : 'apps',

                children: [
                    {
                        id       : 't_users',
                        title    : 'Utilisateurs',
                        icon     : 'group',
                        type     : 'item',
                        url      : '/management_tools/users'
                    },
                    /*{
                        id       : 't_departements',
                        title    : 'Departements',
                        icon     : 'work-outline',
                        type     : 'item',
                        url      : '/management_tools/departements'
                    },
                    {
                        id       : 't_roles',
                        title    : "Rôles",
                        icon     : 'assignment_turned_in',
                        type     : 'item',
                        url      : '/management_tools/roles'
                    }*/
                ]
            },
        ]
    },

    // {
    //     id       : 'prospect',
    //     title    : 'prospects',
    //     type     : 'item',
    //     icon     : 'domain',
    //     url      : '/prospects'
    // },
    {
        id       : 'partner',
        title    : 'Gestion de prospect',
        type     : 'group',
        children : [
            {
                id       : 'partner',
                title    : 'Prospect',
                type     : 'collapsable',
                icon     : 'apps',

                children: [
                    {
                        id       : 't_partner',
                        title    : 'Prospect',
                        icon     : 'group',
                        type     : 'item',
                        url      : '/partners'
                    },
                   {
                        id       : 't_relance',
                        title    : 'Relance',
                        icon     : 'launch',
                        type     : 'item',
                        url      : '/partners/relance'
                    }
                    // {
                    //     id       : 't_roles',
                    //     title    : "Rôles",
                    //     icon     : 'assignment_turned_in',
                    //     type     : 'item',
                    //     url      : '/management_tools/roles'
                    // }*/
                ]
            }
        ],
    },
    {
        id       : 'calendar',
        title    : 'Calendrier',
        type     : 'item',
        icon     : 'today',
        url      : '/calendar'
    },
    {
        id: 'gestion-tiers',
        title: 'Gestions project ',
        type: 'collapsable',
        icon: 'domain',
        children: [
            {
                id: 'projets',
                title: 'projets',
                type: 'item',
                url: '/gestion-project/projects'
            },
        ]
    },
    {
        id: 'gestion-tiers',
        title: 'Gestions du Stock ',
        type: 'collapsable',
        icon: 'domain',
        children: [
            {
                id: 'stock',
                title: 'Stock',
                type: 'item',
                url: '/gestion-project/projects'
            },
        ]
    },
];
