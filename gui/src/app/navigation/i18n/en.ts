export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'DASHBOARDS'  : 'Dashboards',
            'GESTION DES PRODUITS': 'GESTION DES PRODUITS',
            'SAMPLE'        : {
                'TITLE': 'Produits',
                'BADGE': '25'
            }
        }
    }
};
