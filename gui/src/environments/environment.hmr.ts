export const environment = {
    production: false,
    hmr       : true,
    format     : {
        date: "DD/MM/YYYY",
        time: "HH:mm:ss",
    },
    API_URL: 'http://localhost:8000/api/',
};
