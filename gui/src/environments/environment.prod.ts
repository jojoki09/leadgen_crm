export const environment = {
    production: true,
    hmr       : false,
    API_URL: 'http://160.153.245.134/api/',
    format     : {
        date: "DD/MM/YYYY",
        time: "HH:mm:ss",
    },
};
